//
//  NewAddViewController.h
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"
#import <MapKit/MapKit.h>
#import "DataPlan.h"

@class ViewController;
@interface NewAddViewController : UIViewController<UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
@property double lat;
@property double lon;
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthScroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthViewInner;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelServicio;
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conMapHeight;
@property (weak, nonatomic) IBOutlet UILabel *labPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;

@property BOOL isComboShow;
@property DataPlan* catSel;
@property (strong, nonatomic) NSMutableArray *categories;
@property (weak, nonatomic) IBOutlet UIButton *btnPublish;
- (IBAction)actPublich:(id)sender;
- (IBAction)actCancel:(id)sender;
- (IBAction)actDone:(id)sender;
- (IBAction)actCancelBox:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerGuardar;
- (IBAction)actCategory:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancelar;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end
