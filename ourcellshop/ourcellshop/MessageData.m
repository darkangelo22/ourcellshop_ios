//
//  MessageData.m
//  
//
//  Created by René Abilio on 14/1/16.
//
//

#import "MessageData.h"

@implementation MessageData
-(id)initWithData:(NSString*)fromp to:(NSString*)top subject:(NSString*)subjectp body: (NSString*)bodyp date:(NSString*) datep imgUrl:(NSString*) imgUrlp;
{
    self.from = fromp;
    self.to = top;
    self.subject=subjectp;
    self.body=bodyp;
    self.date = datep;
    self.imgUrl=imgUrlp;
    
    return self;
}
-(id)initWithMessage:(MessageData *)msg  {
    
    self.from = msg.from;
    self.subject=msg.subject;
    self.body=msg.body;
    self.date = msg.date;
    self.imgUrl=msg.imgUrl;
    
    return self;
}
@end
