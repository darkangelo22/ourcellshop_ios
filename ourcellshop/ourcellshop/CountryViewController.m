//
//  CountryViewController.m
//
//
//  Created by René Abilio on 4/2/16.
//
//

#import "CountryViewController.h"
#import "ViewController.h"
@interface CountryViewController ()

@end

@implementation CountryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableCountries setDelegate:self];
    [self.tableCountries setDataSource:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark Tabla Proveedores
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    keyValue* arr =[_countries objectAtIndex:section];
    return arr.value.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"CountryTableViewCell";
    
    CountryTableViewCell *cell = (CountryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CountryTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    //    Configure the cell...
    keyValue* arr =[_countries objectAtIndex:indexPath.section];
    
    NSString *sectionTitle = arr.key;
    NSArray *sectionCountry = arr.value;
    DataCountry *country = [sectionCountry objectAtIndex:indexPath.row];
    NSString* imgName =[NSString stringWithFormat:@"%@.png",country.isO2];
    cell.labCountry.text = country.descriptionCountry;
    cell.imgCountryFlag.image = [UIImage imageNamed:[imgName lowercaseString]];
    return cell;
    
    
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self CountryAllKey];
}
-(NSArray*)CountryAllKey{
    NSMutableArray* array = [[NSMutableArray alloc]init];
    for (keyValue* val in _countries) {
        [array addObject:val.key];
    }
    return array;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    return index ;
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    keyValue* arr =[_countries objectAtIndex:section];
    
    return arr.key;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [ _countries count];
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    keyValue* arr =[_countries objectAtIndex:indexPath.section];
    DataCountry *selectedCountry = [arr.value objectAtIndex:index];
    self.HomeViewController.txtCountry.text = selectedCountry.name;
    [self.HomeViewController filterProviders:selectedCountry.name];
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}

- (IBAction)actBack:(id)sender {
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}
@end
