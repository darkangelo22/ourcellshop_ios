//
//  ViewController.m
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "ViewController.h"
#define DEVICE_WIDTH self.view.bounds.size.width
#define DEVICE_HEIGTH self.view.bounds.size.height

@interface ViewController ()

@property HomeViewController* homeVC;
@property MapViewController* mapVC;
@property MenuViewController* menuVC;
@property ServiceViewController* serviceVC;
@property BuyViewController* buyVC;
@property SellViewController* sellVC;
@property FilterViewController* filterVC;
@property FilterServiceViewController* servicefilterVC;
@property ReadMsgViewController* readMsgVC;
@property NewMsgViewController* nuevoMsgVC;
@property NewAdViewController* nuevoAdVC;
@property MessageSendViewController* msgSendVC;
@property MessageViewController* msgVC;
@property NewAddViewController* nuevoAddVC;
@property PinViewController* loginVC;
@property CountryViewController* countryVC;
@property int cantProcess;//Provisional (Hasta conectar con el Servicio Rest)
@property NSTimer* timerBuy;//Timer para simular el tiempo en el popus "Procesando"
@property int cantProcessBuyed;//Provisional (Hasta conectar con el Servicio Rest)
@property NSTimer* timerTicketBuyed;
@property dispatch_queue_t myQueue;


@property UIView* scrollInMenu;
@property UIView* blackScreen;

@property BOOL isOpenMenu;
@property BOOL isOpenRigthMenu;
@property (nonatomic) BOOL showModalTermCond;

@property UIView* screenContainerPopup;

//Properties de la compra actual
@property NSString *appTransactionId;
@property BOOL isCompleteBuy;
@property BOOL isTerminatedBuyComplety;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    FilterServices* filterdata = [[FilterServices alloc] initWithZipCode:-1 Distance:5 Category:1 MinPrice:-1 MaxPrice:-1 Published:3];
    self.FilterServiceData = filterdata;
    self.FilterProductData = filterdata;
    self.txtBuscar.delegate = self;
    
    //Ajuste iPhone 6 or 5 Scroll
    if(IS_IPHONE_5 || IS_IPHONE_6){
        self.heigthScroll.constant = DEVICE_HEIGTH;
    }
    
    if(IS_IPHONE_6){
        self.conAlignTickets.constant = -120;
        self.conAlignInicio.constant = 120;
        self.consWith.constant = 350;
    }
    
    if(IS_IPHONE_6_PLUS){
        self.heigthScroll.constant = DEVICE_HEIGTH;
        
        self.conAlignTickets.constant = -130;
        self.conAlignInicio.constant = 130;
    }
    self.viewSearch.layer.cornerRadius = 3;
    
    
    //Init Subviews
    self.homeVC = [self loadController:[HomeViewController class]];
    self.homeVC.viewController = self;
    self.serviceVC = [self loadController:[ServiceViewController class]];
    self.serviceVC.viewController = self;
    self.buyVC = [self loadController:[BuyViewController class]];
    self.buyVC.viewController = self;
    self.sellVC = [self loadController:[SellViewController class]];
    self.sellVC.viewController = self;
    self.filterVC = [self loadController:[FilterViewController class]];
    self.filterVC.viewController = self;
    self.servicefilterVC = [self loadController:[FilterServiceViewController class]];
    self.servicefilterVC.viewController = self;
    self.mapVC = [self loadController:[MapViewController class]];
    self.mapVC.viewController = self;
    self.msgSendVC = [self loadController:[MessageSendViewController class]];
    self.msgSendVC.viewController = self;
    self.msgVC = [self loadController:[MessageViewController class]];
    self.msgVC.viewController = self;
    self.readMsgVC = [self loadController:[ReadMsgViewController class]];
    self.readMsgVC.viewController = self;
    
    self.nuevoMsgVC = [self loadController:[NewMsgViewController class]];
    self.nuevoMsgVC.viewController = self;
    
    self.loginVC = [self loadController:[PinViewController class]];
    self.loginVC.viewController = self;
    
    self.countryVC = [self loadController:[CountryViewController class]];
    self.countryVC.viewController = self;
    
    
    [self fillScroll];
    self.scroll.delegate = self;
    
    
    //Menus -------------------------------------------------------
    //-------------------------------------------------------------
    self.isOpenMenu = NO;
    CGFloat height = DEVICE_HEIGTH;
    if(self.menuVC == nil){
        self.menuVC = [self loadController:[MenuViewController class]];
        self.menuVC.view.frame = CGRectMake(0, 0, self.menuVC.view.frame.size.width, height);
        self.menuVC.viewController = self;
    }
    
    //    if(self.rigthMenuVC == nil){
    //        self.rigthMenuVC = [self loadController:[RigthMenuViewController class]];
    //        self.rigthMenuVC.view.frame = CGRectMake(0, 0, self.rigthMenuVC.view.frame.size.width, height);
    //        self.rigthMenuVC.viewController = self;
    //    }
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    
    
    
    //Fonts -------------------------------------------------------
    [self.titleApp setFont:[UIFont fontWithName:@"Roboto-Regular" size:16.0]];
    [self.titleApp2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.txtBuscar setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labHome setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labServices setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labSell setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labBuy setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labPay setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    
    if(IS_IPHONE_6){
        [self.btnHome setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
        [self.btnManual setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
        [self.btnTickets setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    }
    
    
    if(IS_IPHONE_6_PLUS){
        [self.btnHome setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
        [self.btnManual setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
        [self.btnTickets setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    }
    //---------------------------------------
    
    self.isShowMap = NO;
    self.isShowMsgSend = NO;
    
}

-(void)cleanColors{
    
    self.imgHome.image = [UIImage imageNamed:@"home.png"];
    self.imgService.image = [UIImage imageNamed:@"iphone.png"];
    self.imgSell.image = [UIImage imageNamed:@"money"];
    self.imgBuy.image = [UIImage imageNamed:@"cash.png"];
    self.imgPay.image = [UIImage imageNamed:@"msg.png"];
    
    UIColor *myColor = [UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0];
    
    //Set Gray Color
    [self.labHome setTextColor:myColor];
    [self.labServices setTextColor:myColor];
    [self.labSell setTextColor:myColor];
    [self.labBuy setTextColor:myColor];
    [self.labPay setTextColor:myColor];
    
}

-(void)colorToHome{
    self.imgHome.image = [UIImage imageNamed:@"home-white.png"];
    
    [self.labHome setTextColor:[UIColor whiteColor]];
}

-(void)colorToService{
    self.imgService.image = [UIImage imageNamed:@"iphone-white.png"];
    
    [self.labServices setTextColor:[UIColor whiteColor]];
}

-(void)colorToSell{
    self.imgSell.image = [UIImage imageNamed:@"money-white.png"];
    
    [self.labSell setTextColor:[UIColor whiteColor]];
}

-(void)colorToBuy{
    self.imgBuy.image = [UIImage imageNamed:@"cash-white.png"];
    
    [self.labBuy setTextColor:[UIColor whiteColor]];
}

-(void)colorToPzy{
    self.imgPay.image = [UIImage imageNamed:@"msg-white.png"];
    
    [self.labPay setTextColor:[UIColor whiteColor]];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    //self.linePin.backgroundColor = [UIColor lightGrayColor];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    //    int posMarkX = self.btnHome.frame.origin.x - 10;
    //    int widthPosMarkAux = self.btnHome.frame.size.width + 20;
    //    self.mark.frame = CGRectMake(posMarkX, self.mark.frame.origin.y, widthPosMarkAux, self.mark.frame.size.height);
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actDelete:(id)sender {
    self.txtBuscar.text = @"";
}
-(void)CenterWithLat:(double)lat lon:(double)lon title:(NSString*)title
{
    [self.mapVC CenterWithLat:lat lon:lon title:title];
}
-(void)backHome{
    [self cleanColors];
    [self colorToHome];
    
    [self.scroll setContentOffset:CGPointMake(0, 0) animated:false];
    [self closeMenu];
}

- (IBAction)actHome:(id)sender {
    
    [self goHome];
    
}

- (IBAction)actService:(id)sender{
    //    [self cleanColors];
    //    [self colorToService];
    //
    //    self.screenToFilter = 1;
    //    self.screenToCancelLogin = 2;
    //
    //    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH, 0) animated:false];
    //    [self closeMenu];
    
    [self goService];
    
}

- (IBAction)actSell:(id)sender {
    
    BOOL isUserLog = [AdminNSUserDefaults isUserLogued];
    if (!isUserLog) {
        [self loadLogin];
    }else{
        [self closeMenu];
        [self cleanColors];
        [self colorToSell];
        self.screenToFilter = 2;
        
        [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*2, 0) animated:false];
        [self closeMenu];
        [self.sellVC onLoad];
    }
    
}

- (IBAction)actBuy:(id)sender {
    
    //    [self closeMenu];
    //    [self cleanColors];
    //    [self colorToBuy];
    //
    //    if(self.isShowMap){
    //
    //        self.screenToFilter = 4;
    //        self.screenToCancelLogin = 4;
    //        [self goMap];
    //    }else{
    //
    //        self.screenToFilter = 3;
    //        self.screenToCancelLogin = 3;
    //        [self goGallery];
    //    }
    [self.buyVC onLoad];
    [self goBuy];
    
}

- (IBAction)actManual:(id)sender {
    
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH, 0) animated:false];
    
    [self closeMenu];
    
}

- (void)actSucive {
    
    
}

- (IBAction)actCleanTxtSearch:(id)sender {
}

-(void)showFilters{
    [self presentViewController:self.filterVC animated:YES completion:Nil];
}
-(void)showServiceFilters{
    [self presentViewController:self.servicefilterVC animated:YES completion:Nil];
}

#pragma mark - Metodos Menu

-(void)showTerminoCondiciones{
    
    
    
}

-(void)showConfiguration{
    
    
}

- (void)showHelp{
    
}

- (void)showPagoServicio{
    
}


#pragma mark - Metodos Auxiliares

-(void)openLeftMenu{
    if(!self.isOpenMenu)
        [self showMenu];
    else
        [self closeMenu];
}

- (IBAction)actMenu:(id)sender {
    
    if(!self.isOpenMenu)
        [self showMenu];
    else
        [self closeMenu];
}

- (IBAction)actConfig:(id)sender {
    
    
    [self showConfiguration];
    
}

- (IBAction)actPzy:(id)sender {
    
    BOOL isUserLog = [AdminNSUserDefaults isUserLogued];
    if (!isUserLog) {
        [self loadLogin];
    }else{
        [self closeMenu];
        [self cleanColors];
        [self colorToPzy];
        
        if(self.isShowMsgSend){
            
            [self goMsgSend];
        }else{
            [self goMsg];
        }
    }
    
}

- (IBAction)hideMenuHeader:(id)sender {
    
    [self closeMenu];
}

-(void)btnHide{
    
    [self closeMenu];
    
}

- (id)loadController:(Class)classType {
    NSString *className = NSStringFromClass(classType);
    UIViewController *controller = [[classType alloc] initWithNibName:className bundle:nil];
    return controller;
}

-(void)showMenu{
    
    self.isOpenMenu = YES;
    
    //    int xScroll = self.scroll.frame.origin.x;
    //    int yScroll = self.scroll.frame.origin.y+8;
    //CGFloat height = self.menuVC.view.frame.size.height+100;
    //    CGFloat height = DEVICE_HEIGTH - 102;//102 tamanno de la view naranja
    
    CGFloat height = DEVICE_HEIGTH;
    //Pantalla negra de la derecha
    UIView* overlayView = [[UIView alloc] initWithFrame:self.view.frame];
    overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    UIButton* buttonHide = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, overlayView.frame.size.width, height)];
    [buttonHide addTarget:self action:@selector(btnHide) forControlEvents:UIControlEventTouchUpInside];
    [overlayView addSubview:buttonHide];
    self.blackScreen = overlayView;
    [self.view addSubview:overlayView];
    
    
    //Init view donde carga el menu
    UIView* scrollIn = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.menuVC.view.frame.size.width, DEVICE_HEIGTH)];
    self.scrollInMenu = scrollIn;
    [self.view addSubview:scrollIn];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.2;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    [scrollIn.layer addAnimation:transition forKey:nil];
    [scrollIn addSubview:self.menuVC.view];
}


-(void)closeMenu{
    
    self.isOpenMenu = NO;
    
    if(self.menuVC == nil){
        self.menuVC = [self loadController:[MenuViewController class]];
    }
    
    
    [self.blackScreen removeFromSuperview];
    [self.scrollInMenu removeFromSuperview];
    [self.menuVC.view removeFromSuperview];
}


-(void) fillScroll{
    for(UIView *subview in [ self.scroll subviews]) {
        [subview removeFromSuperview];
    }
    
    self.scroll.scrollEnabled=NO;
    CGFloat height = self.scroll.frame.size.height;
    CGFloat totalWith = 0;
    
    
    [self.scroll setContentSize:CGSizeMake(DEVICE_WIDTH*6, height)];
    
    
    //Add Home
    self.homeVC.view.frame=CGRectMake(totalWith, 0, self.homeVC.view.frame.size.width, height);
    [self.scroll addSubview:self.homeVC.view];
    totalWith += self.homeVC.view.frame.size.width;
    
    
    //Add Service
    int adjustTotal = totalWith+8;
    int widthService = self.serviceVC.view.frame.size.width;
    if (IS_IPHONE_5 ) {
        adjustTotal=totalWith;
    }
    else{
        adjustTotal=totalWith;
    }
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 55;
    }
    
    self.serviceVC.view.frame=CGRectMake(adjustTotal, 0, widthService, height);
    [self.scroll addSubview:self.serviceVC.view];
    totalWith += widthService;
    
    
    //Add Sell
    adjustTotal = totalWith+8;
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        adjustTotal=totalWith;
    }
    else{
        adjustTotal=totalWith;
    }
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 110;
    }
    self.sellVC.view.frame=CGRectMake(adjustTotal, 0, self.sellVC.view.frame.size.width, height);
    [self.scroll addSubview:self.sellVC.view];
    totalWith += self.sellVC.view.frame.size.width;
    
    
    //Add Buy
    adjustTotal = totalWith+8;
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        adjustTotal=totalWith;
    }
    else{
        adjustTotal=totalWith;
    }
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 165;
    }
    
    self.buyVC.view.frame=CGRectMake(adjustTotal, 0, self.buyVC.view.frame.size.width, height);
    [self.scroll addSubview:self.buyVC.view];
    totalWith += self.buyVC.view.frame.size.width;
    
    
    //Map After All Principal Tabs
    //Add Map
    adjustTotal = totalWith+8;
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        adjustTotal=totalWith;
    }else{
        adjustTotal=totalWith;
    }
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 212;
    }
    
    self.mapVC.view.frame=CGRectMake(adjustTotal, 0, self.mapVC.view.frame.size.width, height);
    [self.scroll addSubview:self.mapVC.view];
    totalWith += self.buyVC.view.frame.size.width;
    
    
    //Msg
    
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        adjustTotal=totalWith;
    }
    else{
        adjustTotal=totalWith;
    }
    adjustTotal = totalWith + 16;
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 274;
    }
    
    self.msgVC.view.frame=CGRectMake(adjustTotal, 0, self.msgVC.view.frame.size.width, height);
    [self.scroll addSubview:self.msgVC.view];
    totalWith += self.msgVC.view.frame.size.width;
    
    
    //Msg Send
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        adjustTotal=totalWith;
    }
    else{
        adjustTotal=totalWith;
    }
    
    if ( IS_IPHONE_6)
    {
        adjustTotal=totalWith + 337;
    }
    
    self.msgSendVC.view.frame=CGRectMake(adjustTotal, 0, self.msgSendVC.view.frame.size.width, height);
    [self.scroll addSubview:self.msgSendVC.view];
    totalWith += self.msgSendVC.view.frame.size.width;
    
    
    
    [self.scroll setContentOffset:CGPointMake(0,0)];
}

-(void)goHome{
    
    [self cleanColors];
    [self colorToHome];
    
    self.screenToCancelLogin = 1;
    [self.scroll setContentOffset:CGPointMake(0, 0) animated:false];
    [self closeMenu];
    
}

-(void)goService{
    
    [self cleanColors];
    [self colorToService];
    
    self.screenToFilter = 1;
    self.screenToCancelLogin = 2;
    
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH, 0) animated:false];
    [self.serviceVC onLoad];
    [self closeMenu];
    
    
}

-(void)goBuy{
    
    [self closeMenu];
    [self cleanColors];
    [self colorToBuy];
    
    if(self.isShowMap){
        
        self.screenToFilter = 4;
        self.screenToCancelLogin = 4;
        [self goMap];
    }else{
        
        self.screenToFilter = 3;
        self.screenToCancelLogin = 3;
        [self goGallery];
        
    }
    
}
-(void)goMapWithData:(NSMutableArray*)data
{
    self.mapVC.data = data;
    self.mapVC.showData = YES;
    [self.mapVC loadData];
    [self goMap];
}
-(void)goMap{
    self.isShowMap = YES;
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*4, 0) animated:false];
    [self closeMenu];
}

-(void)goGallery{
    self.isShowMap = NO;
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*3, 0) animated:false];
    [self closeMenu];
    
}

-(void)goMsg{
    self.isShowMsg = NO;
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*5, 0) animated:false];
    [self closeMenu];
    [self.msgVC onLoad];
}
-(void)goMsgSend{
    self.isShowMsgSend = NO;
    [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*6, 0) animated:false];
    [self closeMenu];
    [self.msgSendVC onLoad];
}
-(void)goMsgRead:(MessageData*)msg{
    
    self.readMsgVC.message = msg;
    [self presentViewController:self.readMsgVC animated:YES completion:^{[self closeMenu];}];
    
}
-(void)goMsgNew:(NSString*)owner{
    self.nuevoMsgVC.own = owner;
    [self presentViewController:self.nuevoMsgVC animated:YES completion:^{[self closeMenu];}];
    
}

-(void)goCancelLogin{
    
    switch (self.screenToCancelLogin) {
        case 1:
        {
            [self goHome];
        }break;
        case 2:
        {
            [self goService];
        }break;
        case 3:
        {
            [self goGallery];
        }break;
        case 4:
        {
            [self goMap];
        }break;
    }
    
}

-(void)goNewAd{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if(self.nuevoAdVC == nil){
        self.nuevoAdVC = [sb instantiateViewControllerWithIdentifier:@"nuevoAdViewController"];
        self.nuevoAdVC.viewController = self;
    }
    [self.nuevoAdVC clean];
    
    [self presentViewController:self.nuevoAdVC animated:YES completion:^{[self closeMenu];}];
    
}


-(void)goNewAdd{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if(self.nuevoAddVC == nil){
        self.nuevoAddVC = [sb instantiateViewControllerWithIdentifier:@"nuevoAddViewController"];
        //self.nuevoAdVC.viewController = self;
    }
    
    [self presentViewController:self.nuevoAddVC animated:YES completion:^{[self closeMenu]; }];
    
}

-(void)goScreenFilterFrom{
    switch (self.screenToFilter) {
        case 1://Services
        {
            [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH, 0) animated:false];
            [self closeMenu];
        }break;
        case 2://Sell
        {
            [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*2, 0) animated:false];
            [self closeMenu];
        }break;
        case 3://Gallery
        {
            [self goGallery];
        }break;
        case 4://Map
        {
            [self goMap];
        }break;
    }
}
-(void)loadLogin:(int)value{
    self.loginVC.gate = value;
    
    [self presentViewController:self.loginVC animated:YES completion:^{[self closeMenu];}];
    
    
}
-(void)loadLogin{
    
    [self presentViewController:self.loginVC animated:YES completion:^{[self closeMenu];}];
    
}

-(void)loadCountry:(NSMutableDictionary*)countries arrayc:(NSMutableArray*)arrayc{
    self.countryVC.countries = countries;
    self.countryVC.arrayc = arrayc;
    self.countryVC.HomeViewController = self.homeVC;
    [self presentViewController:self.countryVC animated:YES completion:^{[self closeMenu];}];
    
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sscrollView{
    
    CGPoint offset = self.scroll.contentOffset;
    int pos =  round(offset.x/self.scroll.frame.size.width);
    
    switch (pos) {
        case 0:{
            
            [self.scroll setContentOffset:CGPointMake(0, 0) animated:true];
            
        }  break;
        case 1:
            [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH, 0) animated:true];
            break;
        case 2:{
            
            [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*2, 0) animated:true];
        }break;
        case 3:{
            [self.scroll setContentOffset:CGPointMake(DEVICE_WIDTH*3, 0) animated:true];
        }break;
        default:
            break;
    }
    
    _posMark = pos;
    
}

-(void)refreshTableHomeProviders{
    [self.homeVC updateTableRepairDismiss];
}


@end
