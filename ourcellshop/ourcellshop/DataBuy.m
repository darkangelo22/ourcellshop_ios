//
//  DataBuy.m
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import "DataBuy.h"

@implementation DataBuy

-(id)initWithData:(NSString*)idB titleBuyP:(NSString*) titl descriptionBuyP:(NSString*) desc imagesP:(NSMutableArray*) arrayImag coordinatesp:(NSString*) coordinates  ownerp:(NSString*) owner{
    
    if (!(self = [super init]))
        return nil;
    
    self.idBuy = idB;
    self.titleBuy = titl;
    self.descriptionBuy = desc;
    self.arrayImages = arrayImag;
     self.coordinates = coordinates;
    self.owner = owner;
    return self;
}

-(id)initWithBuy:(DataBuy*)dataBuy{
    
    if (!(self = [super init]))
        return nil;
    
    self.idBuy = dataBuy.idBuy;
    self.titleBuy = dataBuy.titleBuy;
    self.descriptionBuy = dataBuy.descriptionBuy;
    self.arrayImages = dataBuy.arrayImages;
    self.coordinates = dataBuy.coordinates;
    self.owner = dataBuy.owner;
    return self;

}

@end
