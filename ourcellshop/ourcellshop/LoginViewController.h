//
//  LoginViewController.h
//  
//
//  Created by René Abilio on 22/12/15.
//
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "AdminNSUserDefaults.h"
#import "UserLogued.h"

@class ViewController;

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conLogoTop;

@property (weak, nonatomic) IBOutlet UIButton *labForgot;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSingIn;
@property int gate;
- (IBAction)actSignIn:(id)sender;
- (IBAction)actSignInFacebook:(id)sender;
- (IBAction)actSignInTwitter:(id)sender;
- (IBAction)actCancel:(id)sender;

@end
