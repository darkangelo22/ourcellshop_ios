//
//  ServicesProviderViewController.h
//  
//
//  Created by René Abilio on 29/1/16.
//
//

#import <UIKit/UIKit.h>
#import "DataProvider.h"

@class ViewController;

@interface ServicesProviderViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property DataProvider* dataProvider;
@property (weak, nonatomic) IBOutlet UIButton *BtnDone;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property (strong, nonatomic) NSMutableArray *categories;
@property (strong, nonatomic) NSMutableArray *services;
@property (weak, nonatomic) IBOutlet UITableView *tableServices;
@property (weak, nonatomic) IBOutlet UIImageView *imgProvider;
- (IBAction)ActDone:(id)sender;
- (IBAction)actBack:(id)sender;
@property(assign) ViewController* viewController;
@end
