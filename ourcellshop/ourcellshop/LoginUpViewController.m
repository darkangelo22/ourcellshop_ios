//
//  LoginUpViewController.m
//  
//
//  Created by René Abilio on 23/12/15.
//
//

#import "LoginUpViewController.h"

@interface LoginUpViewController ()

@end

@implementation LoginUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnCancel.layer.cornerRadius=5;
    self.btnSignUp.layer.cornerRadius=5;
    
    //Ajuste iPhone 6
    if(IS_IPHONE_6){
        self.conLogoTop.constant = 58;
        
        
    }
    //Ajuste iPhone 5
    if(IS_IPHONE_5){
        self.conLogoTop.constant = 48;
        
    }
    
    //Ajuste iPhone 6+
    if(IS_IPHONE_6_PLUS){
        self.conLogoTop.constant = 63;
    }
    

    [self.txtFullName setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtEmail setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtZipCode setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtPhoneNumber setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtPassword setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.btnSignUp setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    
    //Delegate From TextField
    self.txtFullName.delegate = self;
    self.txtZipCode.delegate = self;
    self.txtPhoneNumber.delegate = self;
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)actCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actSignUp:(id)sender {
    
    //If Login Correct Save NSUserDefault
    [self dismissViewControllerAnimated:YES completion:^{}];
}

-(void)moveToMain{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    ViewController *viewControllerPrincipal = [sb instantiateViewControllerWithIdentifier:@"initialViewController"];
    
    
    [self presentViewController:viewControllerPrincipal animated:YES completion:NULL];
}



#pragma mark Metodos del Scroll y los TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
