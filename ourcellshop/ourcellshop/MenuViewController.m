//
//  MenuViewController.m
//  t2parking
//
//  Created by René Abilio on 8/26/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.titleApp setFont:[UIFont fontWithName:@"Roboto-Regular" size:17.0]];
    [self.titleApp2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    
    [self.descripcion setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    
    [self.labTickets setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    [self.labMatriculas setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    [self.labConfiguracion setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    [self.labTerminoyCondiciones setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    [self.labAyuda setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)actHelp:(id)sender {
    [self.viewController showHelp];
}

- (IBAction)actTerminos:(id)sender {
    [self.viewController showTerminoCondiciones];
}

- (IBAction)actConfiguracion:(id)sender {
    //[self.viewController showConfiguration];
}

- (IBAction)actPagoServicio:(id)sender {
    //[self.viewController showPagoServicio];
}






//Olds --------------------------------------------
//- (IBAction)actTermCond:(id)sender {
//    [self.viewController showTerminoCondiciones];
//}
//
//- (IBAction)actConfiguracion:(id)sender {
//    [self.viewController showConfiguration];
//}
//
//- (IBAction)actMatriculas:(id)sender {
//    [self.viewController showMatriculas];
//}
//
//- (IBAction)actHistoryTickets:(id)sender {
//    [self.viewController showTicketsHistory];
//}

@end
