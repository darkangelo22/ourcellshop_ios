//
//  RouteService.h
//  t2parking
//
//  Created by René Abilio on 9/12/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdminNSUserDefaults.h"
#import <UIKit/UIKit.h>

@interface RouteService : NSObject
//https://microsoft-apiapp33301bd594e440739cfdaf7b784ba6a7.azurewebsites.net/api/provider?fields=id,name,description,countryid,plans
#define URL_BASE [[NSString alloc]initWithFormat:@"http://%@",@"cellshopapi.azurewebsites.net/api/"];
#define URL_BASE_PHOTO_PROVIDER [[NSString alloc]initWithFormat:@"http://%@",@"cellshopapi.azurewebsites.net/"];

//Operaciones
#define GET_PROVIDERS [RouteService getProviders];
#define GET_PROVIDERS_PHOTO [RouteService getProviders_photo];
#define GET_PROVIDERS_VERSION [RouteService getProviders_version];
#define GET_COUNTRIES [RouteService getCountries];
#define GET_USER(userid) [RouteService getUser:userid];
#define GET_SERVICES [RouteService getServices];
#define GET_PRODUCTS [RouteService getProducts];
#define GET_MESSAGES [RouteService getMessages];
#define GET_CATEGORIES [RouteService getCategories];
#define GET_TOKEN(user,pass) [RouteService getToken:user passp:pass];

#define VALIDATE_PIN(pin,phone) [RouteService getValidatePin:pin numberPhone:phone];
#define SOLICITAR_ESTACIONAMIENTO(plate, minu, plac, hou) [RouteService requestParking:plate minutes:minu place:plac hour: hou]
#define CONSULTAR_ESTACIONAMIENTO(appTransaccionId) [RouteService consultationParking:appTransaccionId]
#define CONSULTAR_REGISTRO [RouteService consultationRegister]
#define HISTORICO_TICKETS [RouteService historyTickets]
#define PRICE_OF_30 [RouteService getPriceOf30]
#define CONSULTA_PATENTE_SUCIVE(patente, padron) [RouteService getConsultaPatenteSucive: patente padron:padron]


//Metodos auxiliares
+(NSString*) getProviders;
+(NSString*) getProviders_photo;
+(NSString*) getProviders_version;
+(NSString*) getCountries;
+(NSString*) getUser:(NSString*)userid;
+(NSString*) getServices;
+(NSString*) getProducts;
+(NSString*) getMessages;
+(NSString*) getCategories;
+(NSString*) getToken:(NSString*)user passp:(NSString*)pass;
//+(NSString*) requestParking:(NSString*)plate minutes:(NSString*)min place:(NSString*)place hour:(NSString*)hour;
//+(NSString*) consultationParking:(NSString*)appTransaccionId;
//+(NSString*) consultationRegister;
//+(NSString*) historyTickets;
//+(NSString*) getPriceOf30;
//+(NSString*) getConsultaPatenteSucive:(NSString*)patente padron:(NSString*)padro;
//+(NSString *)uuidString;
//+(BOOL)isAntel:(NSString*)phone;
//+(BOOL)isClaro:(NSString*)phone;
//+(BOOL)isMovistar:(NSString*)phone;

@end
