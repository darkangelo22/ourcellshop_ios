//
//  FilterServices.m
//  ourcellshop
//
//  Created by René Abilio on 9/2/16.
//  Copyright © 2016 kinesys. All rights reserved.
//

#import "FilterServices.h"

@implementation FilterServices


-(id)initWithZipCode:(int)ZipCode Distance: (int)Distance Category:(int) Category MinPrice:(double) MinPrice MaxPrice:(double) MaxPrice Published:(int) Published{
    self.ZipCode = ZipCode;
    self.Distance = Distance;
    self.Category = Category;
    self.MinPrice =MinPrice;
    self.MaxPrice=MaxPrice;
    self.Published = Published;
    return self;
}

-(id)initWithFilter:(FilterServices*)filter{
    self.ZipCode = filter.ZipCode;
    self.Distance = filter.Distance;
    self.Category = filter.Category;
    self.MinPrice =filter.MinPrice;
    self.MaxPrice=filter.MaxPrice;
    self.Published = filter.Published;
    return self;
}
@end
