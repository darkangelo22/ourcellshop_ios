//
//  ProviderTableViewCell.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"

@interface ProviderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgageProvider;
@property (weak, nonatomic) IBOutlet UILabel *descriptionProvider;
@property (weak, nonatomic) IBOutlet UILabel *labSelectPlan;
@property (weak, nonatomic) IBOutlet UILabel *labCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectPlan;





@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigthDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigthCountry;

@end
