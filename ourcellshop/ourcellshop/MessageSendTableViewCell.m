//
//  MessageSendTableViewCell.m
//  
//
//  Created by René Abilio on 14/1/16.
//
//

#import "MessageSendTableViewCell.h"

@implementation MessageSendTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.To setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.Subject setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.body setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.date setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
