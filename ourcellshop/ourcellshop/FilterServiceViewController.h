//
//  FilterServiceViewController.h
//  
//
//  Created by René Abilio on 11/2/16.
//
//

#import <UIKit/UIKit.h>

#import "Util.m"
@class ViewController;

@interface FilterServiceViewController : UIViewController

//Preferences Saved

@property BOOL isActivation;
@property BOOL isRepair;
@property BOOL isUnlock;
@property int timeFromSearch;//1 From Last 24 Hours, 2 From This Week, 3 All Products

@property (weak, nonatomic) IBOutlet UILabel *titleFilters;
@property (weak, nonatomic) IBOutlet UILabel *labDistance;
@property (weak, nonatomic) IBOutlet UILabel *labCategories;
@property (weak, nonatomic) IBOutlet UILabel *labPrice;
@property (weak, nonatomic) IBOutlet UILabel *labPublished;
@property (weak, nonatomic) IBOutlet UILabel *labUnlock;
@property (weak, nonatomic) IBOutlet UILabel *labRepair;
@property (weak, nonatomic) IBOutlet UILabel *labActivation;
@property (weak, nonatomic) IBOutlet UIButton *labResetAll;
@property (weak, nonatomic) IBOutlet UIButton *labCancel;
@property (weak, nonatomic) IBOutlet UILabel *labMin;
@property (weak, nonatomic) IBOutlet UILabel *labMax;
@property (weak, nonatomic) IBOutlet UILabel *labWeek;
@property (weak, nonatomic) IBOutlet UILabel *labAllProducts;
@property (weak, nonatomic) IBOutlet UIButton *labBtnSave;
@property (weak, nonatomic) IBOutlet UIImageView *imgActivation;
@property (weak, nonatomic) IBOutlet UIImageView *imgUnlock;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthScroll;
@property (weak, nonatomic) IBOutlet UILabel *labLocation;
@property (weak, nonatomic) IBOutlet UILabel *labZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;

@property (weak, nonatomic) IBOutlet UIImageView *imgRepair;
@property (weak, nonatomic) IBOutlet UILabel *lab24hours;
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UISlider *HorizontalSlider;

@property (weak, nonatomic) IBOutlet UITextField *minPrice;
@property (weak, nonatomic) IBOutlet UITextField *maxPrice;
@property (weak, nonatomic) IBOutlet UIImageView *img24Hours;
@property (weak, nonatomic) IBOutlet UIImageView *imgAllProducts;
@property (weak, nonatomic) IBOutlet UIImageView *imgThisWeek;
- (IBAction)actUnlock:(id)sender;

- (IBAction)actCancel:(id)sender;
- (IBAction)actResetAll:(id)sender;
- (IBAction)actRepair:(id)sender;
- (IBAction)actActivation:(id)sender;
- (IBAction)act24Horas:(id)sender;
- (IBAction)actAllProducts:(id)sender;
- (IBAction)actThisWeek:(id)sender;
- (IBAction)actSaveFilter:(id)sender;
@end
