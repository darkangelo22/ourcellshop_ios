 //
//  main.m
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
