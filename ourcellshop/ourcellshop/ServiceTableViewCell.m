//
//  ServiceTableViewCell.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "ServiceTableViewCell.h"
#import "ViewController.h"
@implementation ServiceTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.titleService setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.datePublish setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.descriptionService setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    [self.labLike setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.labMap setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.labMore setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    
    
    
    
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conSpaceRigthDate.constant = 25;
//        self.conSpaceRigthDescription.constant = 25;
//        self.conSpaceRigthBlue.constant = 25;
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actMap:(id)sender {
     NSArray *dataElements = [self.coordinates componentsSeparatedByString:@";"];
    double lat = [dataElements[0] doubleValue];
    double lon= [dataElements[1] doubleValue];
    [self.viewController CenterWithLat:lat lon:lon title:_descriptionService.text];
    [self.viewController goMap];
}

- (IBAction)actContact:(id)sender {
    NSString* own =self.owner;
    [self.viewController goMsgNew:own];

}
@end
