//
//  DataSell.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "DataSell.h"

@implementation DataSell

-(id)initWithData:(NSString*)idS imageP: (NSData*)img titleSellP:(NSString*) titl descriptionSellP:(NSString*) desc priceP:(NSNumber*) pric dateP:(NSDate*) datePubli{
    
    if (!(self = [super init]))
        return nil;
    
    self.idSell = idS;
    self.image = img;
    self.descriptionSell = desc;
    self.titleSell = titl;
    self.price = pric;
    self.datePublish = datePubli;
    
    return self;
}

-(id)initWithSell:(DataSell*)sell{
    
    if (!(self = [super init]))
        return nil;
    
    self.idSell = sell.idSell;
    self.image = sell.image;
    self.descriptionSell = sell.descriptionSell;
    self.titleSell = sell.titleSell;
    self.price = sell.price;
    self.datePublish = sell.datePublish;
    
    return self;
    
}

@end
