//
//  ReadMsgViewController.m
//  
//
//  Created by René Abilio on 11/2/16.
//
//

#import "ReadMsgViewController.h"
#import "ViewController.h"
@interface ReadMsgViewController ()

@end

@implementation ReadMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.labFrom setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    [self.labfromvalue setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labTo setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    [self.labToValue setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labTitle setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtMsg setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    _labfromvalue.text = _message.from;
    _txtMsg.text= _message.body == (NSString *)[NSNull null] ?@"":_message.body;
    _labToValue.text= _message.to == (NSString *)[NSNull null] ?@"":_message.to;
    _labFecha.text = _message.date;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actCancel:(id)sender {
    [self.viewController goMsg];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}
@end
