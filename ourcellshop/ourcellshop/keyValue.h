//
//  keyValue.h
//  
//
//  Created by René Abilio on 4/2/16.
//
//

#import <Foundation/Foundation.h>

@interface keyValue : NSObject

@property NSString* key;
@property NSMutableArray* value;

-(keyValue*)initWithKey:(NSString*)keyp value:(NSMutableArray*)valuep;

@end
