//
//  FilterServices.h
//  ourcellshop
//
//  Created by René Abilio on 9/2/16.
//  Copyright © 2016 kinesys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterServices : NSObject
@property int ZipCode;
@property int Distance;
@property int Category;
@property double MinPrice;
@property double MaxPrice;
@property int Published;

-(id)initWithZipCode:(int)ZipCode Distance: (int)Distance Category:(int) Category MinPrice:(double) MinPrice MaxPrice:(double) MaxPrice Published:(int) Published;

-(id)initWithFilter:(FilterServices*)filter;
@end
