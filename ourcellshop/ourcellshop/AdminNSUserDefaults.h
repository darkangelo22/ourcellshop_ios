//
//  AdminNSUserDefaults.h
//  t2parking
//
//  Created by René Abilio on 9/7/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserLogued.h"

@interface AdminNSUserDefaults : NSObject

//+(void)saveMatriculas:(NSMutableArray*)matriculas;
//
//+(NSMutableArray*)getMatriculas;
//
//+(void)savePrincipalMatricula:(NSString*)matricula;
//
//+(NSString*)getPrincipalMatricula;
//
//+(void)savePhoneNumber:(NSString*)phone;
//
//+(NSString*)getPhoneNumber;
//
//+(void)savePhoneNumberValidated;
//
//+(BOOL)getPhoneNumberValidated;

+(BOOL)stringIsNilOrEmpty:(NSString*)aString;

+(void)saveUserLogued:(UserLogued*)user;

+(UserLogued*)getUserLogued;

+(BOOL)isUserLogued;

@end
