//
//  ProviderTableViewCell.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "ProviderTableViewCell.h"

@implementation ProviderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.descriptionProvider setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labSelectPlan setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.labCountry setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conSpaceLeft.constant = 17;
//        self.conSpaceRigth.constant = 15;
//        self.conSpaceRigthDescription.constant = -15;
//        self.conSpaceRigthCountry.constant = 15;
    }
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
