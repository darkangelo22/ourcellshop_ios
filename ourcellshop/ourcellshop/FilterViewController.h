//
//  FilterViewController.h
//  
//
//  Created by René Abilio on 2/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"

@class ViewController;

@interface FilterViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;

@property(assign) ViewController* viewController;

//Preferences Saved
@property BOOL isElectronic;
@property BOOL isSoftware;
@property BOOL isRepair;
@property BOOL isUnlock;
@property int timeFromSearch;//1 From Last 24 Hours, 2 From This Week, 3 All Products


//Header
@property (weak, nonatomic) IBOutlet UILabel *titleFilters;
@property (weak, nonatomic) IBOutlet UIButton *labResetAll;
@property (weak, nonatomic) IBOutlet UIButton *labCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;


//MiniHeaders
@property (weak, nonatomic) IBOutlet UILabel *labDistance;
@property (weak, nonatomic) IBOutlet UILabel *labCategories;
@property (weak, nonatomic) IBOutlet UILabel *labPrice;
@property (weak, nonatomic) IBOutlet UILabel *labPublished;


//Categories
@property (weak, nonatomic) IBOutlet UILabel *labElectronic;
@property (weak, nonatomic) IBOutlet UILabel *labSoftware;
@property (weak, nonatomic) IBOutlet UILabel *labRepair;
@property (weak, nonatomic) IBOutlet UILabel *labUnlock;
@property (weak, nonatomic) IBOutlet UIImageView *imgElectronic;
@property (weak, nonatomic) IBOutlet UIImageView *imgSoftware;
@property (weak, nonatomic) IBOutlet UIImageView *imgRepair;
@property (weak, nonatomic) IBOutlet UIImageView *imgUnlock;

//Price
@property (weak, nonatomic) IBOutlet UILabel *labMin;
@property (weak, nonatomic) IBOutlet UILabel *labMax;
@property (weak, nonatomic) IBOutlet UITextField *txtMin;
@property (weak, nonatomic) IBOutlet UITextField *txtMax;

//Published
@property (weak, nonatomic) IBOutlet UIImageView *img24Hours;
@property (weak, nonatomic) IBOutlet UIImageView *imgThisWeek;
@property (weak, nonatomic) IBOutlet UIImageView *imgAllProd;
@property (weak, nonatomic) IBOutlet UILabel *lab24Hours;
@property (weak, nonatomic) IBOutlet UILabel *labWeek;
@property (weak, nonatomic) IBOutlet UILabel *labAllProducts;
@property (weak, nonatomic) IBOutlet UISlider *HorizontalSlider;


//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *labLocation;
@property (weak, nonatomic) IBOutlet UILabel *labZipCode;

@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;

//Actions
- (IBAction)actResetAll:(id)sender;
- (IBAction)actCancel:(id)sender;
- (IBAction)actSaveAll:(id)sender;
- (IBAction)actElectronic:(id)sender;
- (IBAction)actUnlock:(id)sender;
- (IBAction)actSoftware:(id)sender;
- (IBAction)actRepair:(id)sender;
- (IBAction)act24Hours:(id)sender;
- (IBAction)actThisWeek:(id)sender;
- (IBAction)actAllProducts:(id)sender;


//Auxiliar Methods
-(void)refreshCategories;
-(void)refreshTimePublished;

@end
