//
//  HomeViewController.h
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdminNSUserDefaults.h"
#import "RouteService.h"
#import "DataProvider.h"
#import "DataCountry.h"
#import "ProviderTableViewCell.h"
#import "PlansViewController.h"
#import "ServicesProviderViewController.h"
#import "CountryTableViewCell.h"
#import "keyValue.h"
@class ViewController;

@interface HomeViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeightCombo;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchProvider;
@property (weak, nonatomic) IBOutlet UITableView *tableProviders;
@property (strong, nonatomic) NSMutableArray *providers;
@property (strong, nonatomic) NSMutableArray *allproviders;
@property (strong, nonatomic) NSMutableArray *allcountries;
@property (strong, nonatomic) NSMutableArray *countries;
@property (strong, nonatomic) NSMutableArray *categories;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property BOOL* isComboShow;
@property DataCountry* countrySel;
@property DataPlan* catSel;
@property DataProvider* provSel;
- (IBAction)actCountry:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property DataCountry *selectedCountry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthDescription;
@property (weak, nonatomic) IBOutlet UICollectionView *aCollectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *viewRemainingTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conWidthRenewTicket;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthViewRenew;
@property(assign) ViewController* viewController;
@property (strong, nonatomic) NSMutableArray *items;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleHome;
@property (weak, nonatomic) IBOutlet UIButton *numberAlert;
@property (weak, nonatomic) IBOutlet UILabel *labFindUs;
@property (weak, nonatomic) IBOutlet UILabel *labWelcome;
@property (weak, nonatomic) IBOutlet UILabel *labCountry;
@property (weak, nonatomic) IBOutlet UILabel *labDataCountry;
@property (weak, nonatomic) IBOutlet UILabel *labProviders;
@property (weak, nonatomic) IBOutlet UIButton *btnGuardarPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelarPicker;
@property (weak, nonatomic) IBOutlet UITableView *tableCountries;
- (IBAction)actCancelar:(id)sender;

- (IBAction)actDone:(id)sender;
- (id)loadController:(Class)classType;
- (IBAction)actOpenMenu:(id)sender;
- (IBAction)actLogin:(id)sender;
-(void)filterProviders:(NSString*)filtro;
-(void)selectPlanClicked:(UIButton*)sender;
-(void)updateTableRepairDismiss;



@end
