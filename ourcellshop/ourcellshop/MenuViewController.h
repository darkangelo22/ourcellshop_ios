//
//  MenuViewController.h
//  t2parking
//
//  Created by René Abilio on 8/26/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface MenuViewController : UIViewController
- (IBAction)actTerminos:(id)sender;
- (IBAction)actConfiguracion:(id)sender;
- (IBAction)actHelp:(id)sender;
- (IBAction)actPagoServicio:(id)sender;

@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *labTickets;
@property (weak, nonatomic) IBOutlet UIButton *labMatriculas;
@property (weak, nonatomic) IBOutlet UIButton *labConfiguracion;
@property (weak, nonatomic) IBOutlet UIButton *labTerminoyCondiciones;
@property (weak, nonatomic) IBOutlet UIButton *labAyuda;
@property (weak, nonatomic) IBOutlet UILabel *descripcion;
@property (weak, nonatomic) IBOutlet UILabel *titleApp;
@property (weak, nonatomic) IBOutlet UILabel *titleApp2;



//Old ------------------------
//- (IBAction)actMatriculas:(id)sender;
//- (IBAction)actHistoryTickets:(id)sender;

@end
