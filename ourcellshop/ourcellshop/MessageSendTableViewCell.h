//
//  MessageSendTableViewCell.h
//  
//
//  Created by René Abilio on 14/1/16.
//
//

#import <UIKit/UIKit.h>

@interface MessageSendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnSelectMsg;
@property (weak, nonatomic) IBOutlet UILabel *To;
@property (weak, nonatomic) IBOutlet UILabel *Subject;
@property (weak, nonatomic) IBOutlet UILabel *body;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
