//
//  MapViewController.m
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "MapViewController.h"
#import "ViewController.h"

@interface MapViewController () <MKMapViewDelegate>

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.numberAlert.layer.cornerRadius = 8;
    [self.labBack setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.titleAds setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.txtFilterSearch setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    self.txtFilterSearch.delegate = self;
    
    self.txtFilterSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.data = [[NSMutableArray alloc] init];
    self.showData = NO;
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conSpaceRigth.constant = 25;
    }
    
}
-(void)loadData{
    if (self.showData == YES)
    {
          [self.mapView removeAnnotations:[self.mapView annotations]];
        for(DataBuy* dat in _data)
        {
            CLLocationCoordinate2D tapPoint;
            NSArray *dataElements = [dat.coordinates componentsSeparatedByString:@";"];
            double lat = [dataElements[0] doubleValue];
            double lon= [dataElements[1] doubleValue];
            tapPoint.latitude= lat;
            tapPoint.longitude=lon;
            MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
            
            point1.coordinate = tapPoint;
          
            point1.title=dat.descriptionBuy;
            [self.mapView addAnnotation:point1];
            [self.mapView setCenterCoordinate:point1.coordinate];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)CenterWithLat:(double)lat lon:(double)lon title:(NSString*)title
{
    _Center.latitude = lat;
    _Center.longitude = lon;
    MKCoordinateSpan span;
    span.latitudeDelta=1;
    span.longitudeDelta=1;
    MKCoordinateRegion region = {_Center, span};
    [_mapView setRegion:region];
    
    MKPointAnnotation* point = [[MKPointAnnotation alloc]init];
    point.coordinate = _Center;
    point.title=title;
    
    [_mapView addAnnotation:point];


}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)actBack:(id)sender {
    [self.viewController backHome];
}

- (IBAction)actShowFilters:(id)sender {
    self.viewController.screenToFilter = 4;
    [self.viewController showFilters];
}

- (IBAction)goBuy:(id)sender {
    self.viewController.screenToCancelLogin = 3;
    [self.viewController goGallery];
}

#pragma mark Metodos del Scroll y los TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //self.currSelected = textField;
    [textField setReturnKeyType:UIReturnKeyDone];
    //
    //    CGRect formRect = _scrollView.frame;
    //    float height = [UIScreen mainScreen].bounds.size.height - _scrollView.frame.origin.y - PORTRAIT_KEYBOARD_HEIGHT;
    //    formRect.size.height = height < SCROLL_HEIGHT ? height : SCROLL_HEIGHT;
    //    _scrollView.frame = formRect;
    
    //self.linePin.backgroundColor = [[UIColor blueColor]colorWithAlphaComponent:0.4f];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    //    if ([textField isFirstResponder])
    //    {
    //        CGRect formRect = _scrollView.frame;
    //        formRect.size.height = SCROLL_HEIGHT;
    //        _scrollView.frame = formRect;
    //
    //        [textField resignFirstResponder];
    //
    //    }
    
    
    [textField resignFirstResponder];
    return YES;
}


@end
