//
//  ServicesProviderViewController.m
//  
//
//  Created by René Abilio on 29/1/16.
//
//

#import "ServicesProviderViewController.h"
#import "ViewController.h"
@interface ServicesProviderViewController ()

@end

@implementation ServicesProviderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Load Logo
    if (self.dataProvider != nil) {
        UIImage *image = [[UIImage alloc] initWithData:self.dataProvider.image];
        [self.imgProvider setImage:image];
    }
    [self.BtnDone setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    self.BtnDone.layer.cornerRadius = 5;
    [self.tableServices setDataSource:self];
    [self.tableServices setDelegate:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (id)loadController:(Class)classType {
    NSString *className = NSStringFromClass(classType);
    
    UIViewController *controller = [[classType alloc] initWithNibName:className bundle:nil];
    
    return controller;
}
-(NSNumber*) GetSelectedCategory{
    for(DataPlan* cat in _categories)
    {
        if (cat.isSelectedPlan)
            return cat.idc;
    }
    return 0;
}
- (IBAction)ActDone:(id)sender {
    
    
    
    PlansViewController *servicesDetails = [self loadController:[PlansViewController class]];
    servicesDetails.dataProvider = self.dataProvider;
    servicesDetails.category = [[NSNumber alloc] initWithInt:self.GetSelectedCategory.intValue];
    [self presentViewController:servicesDetails animated:YES completion:Nil];
}
-(void)viewDidAppear:(BOOL)animated{
    //INHABILITANDO CLICK EN PANTALLA CON BOTON GIGANTE
    self.view.userInteractionEnabled = NO;
    //  self.viewController.view.userInteractionEnabled = NO;
    
    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    //INICIALIZANDO SERVICIO DE PROVEDORES
    NSString *urlProviderService = GET_CATEGORIES;
    NSURL *urlprovider = [NSURL URLWithString:urlProviderService];
    self.categories = [[NSMutableArray alloc]init];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlprovider
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:300.0];
  
    dispatch_queue_t myQueue = dispatch_queue_create("my queue", NULL);
    dispatch_async(myQueue, ^{
        
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             
           
                  [self.overlayView removeFromSuperview];
                  self.view.userInteractionEnabled = YES;
                  //  self.viewController.view.userInteractionEnabled = YES;
                  if (data.length > 0 && connectionError == nil)
                  {
                      NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:0
                                                                                 error:NULL];
                      NSString *urlproviderphoto = GET_PROVIDERS_PHOTO;
                      //LLENANDO LISTA DE CATEGORIAS
                      self.dataProvider.Plans = [[NSMutableArray alloc] init];
                      for (id key in greeting) {
                          NSString* providerId = [key objectForKey:@"providerid"];
                          if ([providerId compare: self.dataProvider.idProvider]==NSOrderedSame)
                          {
                              NSString* desc =[key objectForKey:@"description"] ;
                              DataPlan* pl = [[DataPlan alloc] initWithData:desc descriptionp:desc pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:[key objectForKey:@"id"]];
                              [self.categories addObject:pl];
                              for (id plan in [key objectForKey:@"plans"] ) {
                                  NSString* name =[plan objectForKey:@"name"] ;
                                  NSString* desc =[plan objectForKey:@"description"] ;
                                  NSString* price =[plan objectForKey:@"price"] ;
                                   NSNumber* serviceType =[[NSNumber alloc] initWithInt:[[plan objectForKey:@"serviceTypeId"]integerValue ]];
                                  DataPlan* pl = [[DataPlan alloc] initWithData:name descriptionp:desc pricep:price labplanp:@"" isSelPlan:NO typep:serviceType idcp:[plan objectForKey:@"providerServicesId"]];
                                  [self.dataProvider.Plans addObject:pl];
                               }
                              if (self.categories.count > 0)
                                  [[self.categories objectAtIndex:0] setIsSelectedPlan:YES];
                          }
                         
                          
                          
                      }
                      
                      //Refreshing table
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [self.tableServices reloadData];
                      });
                      
                  }else if (connectionError != nil){
                      //Poner PIN INCORRECTO con cartel Error de conexion
                      //                 _labError.text = @"Error de conexión";
                      //                 [_labError setHidden:NO];
                  }
              }];
        
    });
}
#pragma mark Tabla

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"PlansTableViewCell";
    
    PlansTableViewCell *cell = (PlansTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //    if (cell == nil)
    //    {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlansTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    //    }
    
    DataPlan* auxPlaN = [_categories objectAtIndex:indexPath.row];
    
    //Data Fill
    cell.plan = auxPlaN;
    [cell updateViewPlanData];
    cell.btnSelectPlan.tag = indexPath.row;
    [cell.btnSelectPlan addTarget:self action:@selector(selectPlanClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
-(void)selectPlanClicked:(UIButton*)sender
{
    int tagBtnIndex = sender.tag;
    
    DataPlan *planAux = [self.categories objectAtIndex:tagBtnIndex];
    
    //Clean isSelectPlan
    for (int i=0; i<[self.categories count]; i++) {
        DataPlan *planX = [self.categories objectAtIndex:i];
        planX.isSelectedPlan = NO;
    }
    
    //Assign YES to planClicked (planAux)
    planAux.isSelectedPlan = YES;
    
    //Reload Table to RePaint
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableServices reloadData];
    });
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    DataPlan *planEditAux = [self.categories objectAtIndex:index];
}
- (IBAction)actBack:(id)sender {
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}
@end
