//
//  UserLogued.m
//  
//
//  Created by René Abilio on 7/1/16.
//
//

#import "UserLogued.h"

@implementation UserLogued

-(id)initWithData:(NSString*)user typeauthentication: (NSString*)auth  token: (NSString*)token  userid: (NSString*)userid;
{

    if (!(self = [super init]))
        return nil;
    
    self.username = user;
    self.typeauthentication = auth;
    self.token = token;
    self.userid = userid;
    return self;
    
}

-(id)initEmptyUser{
    if (!(self = [super init]))
        return nil;
    
    self.username = @"";
    self.typeauthentication = @"";
    self.token = @"";
    self.userid =  @"";
    return self;
}

//To save In NSUserDefault
- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.typeauthentication forKey:@"typeauthentication"];
    [encoder encodeObject:self.token forKey:@"token"];
    [encoder encodeObject:self.userid forKey:@"userid"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.username = [decoder decodeObjectForKey:@"username"];
        self.typeauthentication = [decoder decodeObjectForKey:@"typeauthentication"];
        self.token = [decoder decodeObjectForKey:@"token"];
        self.userid = [decoder decodeObjectForKey:@"userid"];

    }
    return self;
}

@end
