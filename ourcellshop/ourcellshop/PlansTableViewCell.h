//
//  PlansTableViewCell.h
//  
//
//  Created by René Abilio on 10/1/16.
//
//

#import <UIKit/UIKit.h>
#import "DataPlan.h"

@interface PlansTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnSelectPlan;

@property (weak, nonatomic) IBOutlet UILabel *labPlan;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;

@property (weak, nonatomic) IBOutlet UIButton *actCheckPlan;

@property DataPlan* plan;

-(void)updateViewPlanData;

@end
