//
//  ReadMsgViewController.h
//  
//
//  Created by René Abilio on 11/2/16.
//
//
#import "ViewController.h"
#import <UIKit/UIKit.h>
#import "MessageData.h"
@class ViewController;
@interface ReadMsgViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labToValue;
@property ViewController* viewController;
@property (weak, nonatomic) IBOutlet UILabel *labTo;
@property MessageData* message;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labFrom;
@property (weak, nonatomic) IBOutlet UILabel *labfromvalue;
@property (weak, nonatomic) IBOutlet UITextView *txtMsg;
@property (weak, nonatomic) IBOutlet UILabel *labFecha;
- (IBAction)actCancel:(id)sender;

@end
