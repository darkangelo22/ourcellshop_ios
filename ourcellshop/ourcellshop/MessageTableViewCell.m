//
//  MessageTableViewCell.m
//  
//
//  Created by René Abilio on 14/1/16.
//
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.From setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.Subject setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.Body setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.Date setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
