//
//  BuyTableViewCell.m
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import "BuyTableViewCell.h"
#import "ViewController.h"
@implementation BuyTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.titleBuy setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.descriptionBuy setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    [self.btnMore setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.btnLike setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.btnMap setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.btnContact setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    self.scrollView.delegate = self;
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conRigthBlue.constant = 30;
//        self.conRigthDescription.constant = 30;
//        self.conRigthImage.constant = 30;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Methods From Carousel
-(void)initDataCarouselWithArrayImages:(NSMutableArray*)imagesCarousel{

    self.pageImages = imagesCarousel;
    
    NSInteger pageCount = self.pageImages.count;
    
    // Set up the page control
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    self.widthImagesScroll = [[NSMutableArray alloc]init];
    
    for (NSInteger i=0; i<=imagesCarousel.count; i++) {
        [self loadPage:i];
    }

}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.center = CGPointMake( self.scrollView.bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);

        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        //id widthImag = newPageView.frame.size;
        
        NSNumber *sizeWitdh = [[NSNumber alloc]initWithFloat:newPageView.frame.size.width];
        [self.widthImagesScroll addObject:sizeWitdh];
        
        
        [self setImageToCenter:newPageView];
//        [newPageView setContentMode:UIViewContentModeScaleAspectFill];
//        [newPageView setClipsToBounds:YES];
        
        
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)setImageToCenter:(UIImageView *)imageView
{
    CGSize imageSize = imageView.image.size;
    [imageView sizeThatFits:imageSize];
    CGPoint imageViewCenter = imageView.center;
    imageViewCenter.x = CGRectGetMidX(self.scrollView.frame);
    [imageView setCenter:imageViewCenter];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sscrollView{
    
    CGPoint offset = self.scrollView.contentOffset;
    int pos =  round(offset.x/self.scrollView.frame.size.width);
    self.pageControl.currentPage = pos;
    
    int xPos = [self getInitXToPos:pos];
    
    [self.scrollView setContentOffset:CGPointMake(xPos, 0) animated:true];
    
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    CGPoint offset = self.scrollView.contentOffset;
    int pos =  round(offset.x/self.scrollView.frame.size.width);
    self.pageControl.currentPage = pos;
    
    int xPos = [self getInitXToPos:pos];
    
    [self.scrollView setContentOffset:CGPointMake(xPos, 0) animated:true];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    // Load the pages which are now on screen
//    [self loadVisiblePages];
//}

-(int)getInitXToPos:(int)pos{
    int spaceInX = 0;
    for (int i=0; i<pos; i++) {
        NSNumber *widtPos=[self.widthImagesScroll objectAtIndex:i];
        spaceInX+=widtPos.floatValue;
    }
    
    return spaceInX;
}

- (IBAction)actContact:(id)sender {
    NSString* own =self.owner;
    [self.viewController goMsgNew:own];
}

- (IBAction)actMap:(id)sender {
    NSArray *dataElements = [self.coordinates componentsSeparatedByString:@";"];
    double lat = [dataElements[0] doubleValue];
    double lon= [dataElements[1] doubleValue];
    [self.viewController CenterWithLat:lat lon:lon title:_descriptionBuy.text];
    [self.viewController goMap];
}

@end
