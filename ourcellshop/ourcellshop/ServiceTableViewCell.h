//
//  ServiceTableViewCell.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"
@class ViewController;
@interface ServiceTableViewCell : UITableViewCell
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UILabel *datePublish;

@property (weak, nonatomic) IBOutlet UITextView *descriptionService;
- (IBAction)actMap:(id)sender;
- (IBAction)actContact:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleService;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;

@property (weak, nonatomic) IBOutlet UIButton *labLike;

@property (weak, nonatomic) IBOutlet UIButton *labMore;

@property (weak, nonatomic) IBOutlet UIButton *labMap;
@property NSString* owner;
@property NSString* coordinates;


//Constrain iPhone 4 Space
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigthDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigthDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigthBlue;



@end
