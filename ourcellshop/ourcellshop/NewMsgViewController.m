//
//  NewMsgViewController.m
//  
//
//  Created by René Abilio on 11/2/16.
//
//

#import "NewMsgViewController.h"
#import "ViewController.h"

@interface NewMsgViewController ()

@end

@implementation NewMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
         [self.labTitle setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.labTo setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
         [self.labsubject setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
         [self.labData setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.labMessage setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.txtMsg setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.txtSubject setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.txtTo setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
         [self.btnSend setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    self.txtMsg.text =@"";
    self.txtSubject.text =@"";
    self.txtTo.text =_own;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actSend:(id)sender {
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@message",urlBase];
    NSString* to = _txtTo.text;
    NSString* subject=_txtSubject.text;
    NSString* msg = _txtMsg.text;
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]]; // Convert date to string
   
    NSString* myUser  = @"rmejias";
     NSString* myUserEmail  = @"rmejias@gmail.com";
     NSString* myUserID  = @"0e4879f5-e286-4356-9eaf-cbccce70caf8";
    NSString *post = [NSString stringWithFormat:@"date=%@&from=%@&subject=%@&body=%@&fromUserEmail=%@&toUserName=%@&applicationUserId=%@&messageStatusId=%d",date,myUser,subject,msg,myUserEmail,to,myUserID,1];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&err];
    if (data.length > 0 && err == nil)
    {
        NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                              options:0
                                                                error:NULL];
        
    }
    
    
    
    
    [self.viewController goService];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)actCancel:(id)sender {
    [self.viewController goService];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}
@end
