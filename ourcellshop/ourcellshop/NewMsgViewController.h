//
//  NewMsgViewController.h
//  
//
//  Created by René Abilio on 11/2/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"

@class ViewController;
@interface NewMsgViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labData;
@property NSString* own;
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labTo;
@property (weak, nonatomic) IBOutlet UILabel *labsubject;
@property (weak, nonatomic) IBOutlet UITextView *txtMsg;
@property (weak, nonatomic) IBOutlet UILabel *labMessage;
@property (weak, nonatomic) IBOutlet UITextField *txtTo;
@property (weak, nonatomic) IBOutlet UITextField *txtSubject;
- (IBAction)actSend:(id)sender;

- (IBAction)actCancel:(id)sender;
@end
