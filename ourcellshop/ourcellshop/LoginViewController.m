//
//  LoginViewController.m
//  
//
//  Created by René Abilio on 22/12/15.
//
//

#import "LoginViewController.h"
#import "ViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnSingIn.layer.cornerRadius=5;
    // Do any additional setup after loading the view from its nib.
    
    //Ajuste iPhone 6
    if(IS_IPHONE_6){
        self.conLogoTop.constant = 36;
        
        
    }
    //Ajuste iPhone 5
    if(IS_IPHONE_5){
        self.conLogoTop.constant = 26;
        
    }
    
    //Ajuste iPhone 6+
    if(IS_IPHONE_6_PLUS){
        self.conLogoTop.constant = 41;
    }
    

    [self.labForgot setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Bold" size:10.0]];
    [self.btnSingIn setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    
    [self.txtEmail setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtPassword setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)actSignIn:(id)sender {
    //If Login Correct Save NSUserDefault
    UserLogued *user = [[UserLogued alloc]initWithData:self.txtEmail.text typeauthentication:@"1" token:@"" userid:@""];
    NSString* token = GET_TOKEN(user.username, _txtPassword.text);
    if ([token compare:@"(null) (null)"]== NSOrderedSame)
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Usuario o Contraseña incorrectos."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else{
        user.token=token;
        NSString *urlBase = URL_BASE;
        NSString *userurl = [[NSString alloc]initWithFormat:@"%@Accounts/User/%@",urlBase,user.username];
        NSURL *url = [NSURL URLWithString:userurl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                timeoutInterval:300.0];

        
        [request addValue:[NSString stringWithFormat:@"%@", token] forHTTPHeaderField:@"Authorization"];
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE USUARIOS
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:NULL];
                 user.userid = [greet objectForKey:@"Id"];
                 [AdminNSUserDefaults saveUserLogued:user];
                 
                 [self.viewController goHome];
                 if (self.gate == 0)
                 {
                 //If Login Correct Save NSUserDefault
                 [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{ }];
                 }
                 else
                 {
                     [self.presentingViewController dismissViewControllerAnimated:YES completion:^{ }];
                 }
             }
         }];
        
        
        
    }
}

- (IBAction)actSignInFacebook:(id)sender {
    //LogIn versus Facebook
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:@"Funcionalidad en desarrollo."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    
    [myAlertView show];

//    UserLogued *user = [[UserLogued alloc]initWithData:self.txtEmail.text typeauthentication:@"3" token:@"" userid:@"" ];
//    [AdminNSUserDefaults saveUserLogued:user];
//    
//    [self.viewController goHome];
//    
//    //If Login Correct Save NSUserDefault
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{ }];
}

- (IBAction)actSignInTwitter:(id)sender {
    //LogIn versus Twitter
//    UserLogued *user = [[UserLogued alloc]initWithData:self.txtEmail.text typeauthentication:@"2" token:@"" userid:@""];
//    
//    
//    [AdminNSUserDefaults saveUserLogued:user];
//    
//    [self.viewController goHome];
//    
//    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{ }];

    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:@"Funcionalidad en desarrollo."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    
    [myAlertView show];
}

- (IBAction)actCancel:(id)sender {
    
    [self.viewController goCancelLogin];
    
    [self dismissViewControllerAnimated:YES completion:^{}];
}


#pragma mark Metodos TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
     [textField resignFirstResponder];
    return YES;
}

@end
