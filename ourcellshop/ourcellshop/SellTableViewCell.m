//
//  SellTableViewCell.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "SellTableViewCell.h"

@implementation SellTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.titleSell setFont:[UIFont fontWithName:@"Roboto-Bold" size:16.0]];
    [self.dateSell setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.descriptionSell setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    [self.priceSell setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    [self.btnMore setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    
    [self setImageToCenter:self.imageSell];
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conRigthBlue.constant = 25;
//        self.conRigthDate.constant = 25;
//        self.conRigthDescription.constant = 25;
//        self.conRigthImage.constant = 25;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setImageToCenter:(UIImageView *)imageView
{
    CGSize imageSize = imageView.image.size;
    [imageView sizeThatFits:imageSize];
    CGPoint imageViewCenter = imageView.center;
    imageViewCenter.x = CGRectGetMidX(self.contentView.frame);
    [imageView setCenter:imageViewCenter];
}

@end
