//
//  BuyViewController.h
//  
//
//  Created by René Abilio on 1/1/16.
//
//

#import <UIKit/UIKit.h>
#import "BuyTableViewCell.h"
#import "DataBuy.h"

@class ViewController;

@interface BuyViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *numberAlert;
@property (weak, nonatomic) IBOutlet UILabel *labBack;
@property (weak, nonatomic) IBOutlet UILabel *titleAds;
@property (weak, nonatomic) IBOutlet UITextField *txtFilterSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableBuy;
@property (strong, nonatomic) NSMutableArray *buys;

- (void)onLoad;
- (IBAction)actBack:(id)sender;
- (IBAction)actShowFilters:(id)sender;
- (IBAction)goMap:(id)sender;

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigth;

@end
