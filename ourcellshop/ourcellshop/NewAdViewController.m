//
//  NewAdViewController.m
//  
//
//  Created by René Abilio on 3/1/16.
//
//

#import "NewAdViewController.h"
#import "ViewController.h"

@interface NewAdViewController ()

@end

@implementation NewAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.isPublishOnFacebook = YES;
    
    //Header
    [self.titleNewAd setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.labCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    [self.labPhoto setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.txtWathDoSell setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtPrice setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtDescription setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labDescription setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labDollar setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.labCategory setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labElectronic setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    [self.labShareFacebook setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    [self.btnPublish setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    self.btnPublish.layer.cornerRadius = 5;
    
    //Scroll
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
        //IS iPhone 4 Heigth Scroll reduce
        self.conHeigthScroll.constant = 400;
    }
    _isComboShow = NO;
    [self.picker setDelegate:self];
    [self.picker setDataSource:self];
    self.picker.showsSelectionIndicator=YES;
    [self.btnPickerAceptar setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.btnPickerCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    self.categories=[[NSMutableArray alloc] init];
    [self.txtDescription setDelegate:self];
    [self.txtWathDoSell setDelegate:self];

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    
    tapRecognizer.numberOfTapsRequired = 1;
    
    tapRecognizer.numberOfTouchesRequired = 1;
    
    
    [self.mapView addGestureRecognizer:tapRecognizer];
    self.scrollView.scrollEnabled=YES;
    //CGFloat height = DEVICE_HEIGTH;
    [self.scrollView setContentSize:CGSizeMake(DEVICE_WIDTH, 100)];
}
-(void)clean{
    _img1.image = nil;
    _img2.image = nil;
    _img3.image = nil;
    _txtWathDoSell.text =@"";
    _txtPrice.text =@"";
    _txtDescription.text =@"Description";
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_txtDescription.text compare: @"Description"]==NSOrderedSame)
    {
        _txtDescription.text = @"";
        _txtDescription.textColor = [UIColor blackColor];
    }
    return YES;
}
- (BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    if ([_txtDescription.text compare: @""]==NSOrderedSame)
    {
        _txtDescription.text = @"Description";
        _txtDescription.textColor = [UIColor lightGrayColor];
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(_txtDescription.text.length == 0){
        _txtDescription.textColor = [UIColor lightGrayColor];
        _txtDescription.text = @"Description";
        [_txtDescription resignFirstResponder];
    }
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textViewDidEndEditing:(UITextView *)textField{
    [textField resignFirstResponder];
}
-(bool)textViewShouldReturn:(UITextView *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)actCategory:(id)sender {
    self.categories = [[NSMutableArray alloc]init];
    DataPlan* pl = [[DataPlan alloc] initWithData:@"Unlock" descriptionp:@"Unlock"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"1"];
    [self.categories addObject:pl];
    DataPlan* pl1 = [[DataPlan alloc] initWithData:@"Repair" descriptionp:@"Repair"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"2"];
    [self.categories addObject:pl1];
    
    DataPlan* pl2 = [[DataPlan alloc] initWithData:@"Activation" descriptionp:@"Activation"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"3"];
    [self.categories addObject:pl2];
    [self hideShowPicker];
    [self.picker reloadAllComponents];
}

- (IBAction)actCancel:(id)sender {
    //Dismiss
    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)foundTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.mapView];
    
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    
    point1.coordinate = tapPoint;
    [self.mapView removeAnnotations:[self.mapView annotations]];
    _lat = tapPoint.latitude;
    _lon = tapPoint.longitude;
    point1.title=@"Service Position";
    [self.mapView addAnnotation:point1];
}- (UIImage*)imageWithImage:(UIImage*)image
               scaledToSize:(CGSize)targetSize;

{
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}
-(NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation([self imageWithImage:image scaledToSize:CGSizeMake(100,100) ]);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    return @"iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADrVJREFUeNrsXAl0VNUZ/mfLvkxCyEYSkhCCaFgNkIMCtlTUohSs0rpAJa22VbQeSrWKVjxoUVF7qli1rYpHRGoUD2BVbEtVqoCJCAYCCWQhZCcJk0kymclkZvr9791JQkKS2V4WTv5zvpNk5uW9d7/77/e+p1q/fj2NiueiHqVglMBRAkeyaPv6Qme3kdrhGGVISIdaTTaV2nUCT0XEklmrIxoaElVAoLg/vmsb0A5YhoQ9EBfd2kRRbc0gUdU/gfLXDtqdNpPqw6KI7Dalby8FyBC4BEgCcGEKA/wADWAF2gAjUA2UAceAfPGzUVk71dHCwly6uiyfbKxUrpow2TqUIJDvYC6wGPgecJnQNldlRo+/64BcYA/wCXDS9xqoIo3D7p4PVEDGA7cBtwht85VEi8lgmIG9wBZg12CY/GAQmA78BrgV0Ct8rQDghwJs2i8JMttGYhoTAfwRyAPuHgTyegq7hr8AXwM3jTQCl4kbfwgIHeIMhN1FDvA+kDrcCQwBXgZ2AGnDLJW7EdgvXMmwJHCScOC/Gsb5MAect4HnRHo0bAicD3wGzBohhcUaYSVhw4FAjngfAbEjrDpbAnwIjBlKAq8B3gOCR2iJO0/ki/qhIHC2iG6BNLKFq6J/iLJx0AhMEJoXSheHLBJJ96AQyLXsViCRLi75hUj4FSfwUWDBsBiy3SHDdx23Z4FpShI4B3h4WBDncFBKsD+lhwSQRq0istl9cWb25391p0egdtN0N/sqAfWGvIkg7pMZyXQsK42OZk2g3FmpdPVYuGObT1SRg+O9ShC4CsgcUvLAT7hOQ7umj6drxoRQIDRPp1LRjNAA2jltPE0ND5S103thNxXnSwI52q4betO109KYcLokqHfGwWTekxDpKwK5k/SALwm8g+RWOw21BqYH6vr8Oi3Qz5fV/c9FuuY1gf7A6iENGB12OUgABS3mPg8tMpl9pYFOq7tzoINciTacZKYPCXkgLkMfSCtj9RTjp6UvDCbaXn2ODhvbaHrY+QVQS4eNNpc3QCV82qH7GfA0YPKGwJVDYaqsbdmJkfTnSXEUopFJWRmnp/n6ILrh2zJ6EZ8vjAqVgshho4keLKqhYy0W5Ag+JZDXcX4g6mWPCBwrTjCoJqsBKU9Njqe1Sb0bJUziu3VGWnaojBKC/SQCSy1WkK7yNXlOudkbAufTYK5lQOui/XX0ekYCLR4T0udhmUhb/qnRUIXVLqoQtXNBWwn5Psmd9hZPgsiiwfR3M8OD6DMkxf2Rx9IZSERFkoDoO4V9ojKbKOKB6Z5EYbUo3ZT3dyDvJ/ERtDczhSYHDdxVKm6zUgDMdQlywnemJVE+qpHDcybQk2kxvozC3eUKT0yYO8zKLgyJwa5Pj6XHUsa6/G8bJ8ZQUoAOCbX/eZ9nx+tpfUkdWfm8vjXpTE8IZPKClSRPj7Ls1UvH0fLogZcmrDBVndjYsygy5ILfb6lsJCvSGQWCySRhkXZ3TDhVSbPlmvbjmckDktcMQlYcKaMjxgtvLugQu8deLq+nh05UKxWJ40V555YPVK50AynzI4MpK6z/1YCTrRaas/8kbT3dQHnNXQSaEK131xpoWV4xPVxULedbAX5KkUciE4ly14SjldE+B8UgambHDZwdBYKQWxIi6X2dltZAuwqghVq1inYhDyw2tUsGNVsY1eWhgaTVajo10sfCLbxIdzUwTBECbQ5aNyGalo4d+PQJCBSPpkRT3pw0enf6eCo0W+lPZQ1UbIafA6mE8u4UPmuCRqcE6ijBX6tUFHbWxm4RGKBIygI5eK6FDFZbLz/WZ6RD7LgeZduemSm0bUYyaTUixOJHY3sHnYCpc4CZHOyvJIF+3rSzvO+m4CebnwbB420U/c+dPit9ndtkoox9x2nOV4X0fEktVbS193u6W2LCaGZYt8Ypzr2/Sa71Z+Nzvoa0LVlcU2npzweavdY2u50uxaCWI+Gdpw+iRJRprHfFJgtNDpEVfGPpWSpsRS2rsdHXRTX0BP5eOjaUViCxzooIkfxg7zlxCJIcFAUz12vlVYY1SVF0a6ye6qCVB+Av36ttgra3Qk1U0i5TL6XdXQKN3midv1pNGybF0X1JY8i/x807O8rMMZdgH5xtlgcIss7hwzeqDPRmTRMdm5tOl4Sc70lyagyUx6Tg+FWJkbRhQgyN85eHEaZVA36UjvNfiQnjZsRb1QZag4mpB6kSkZ5Ls7sE1nmqecEgIgcl1nWips2pPkc7MPBCmKcK32+ZmkRTEDV5OI+nRlMaKopVRyvkNSH+UKWm+1CZ9CRPikFAIrQuG9q2XlQveU2tlB4cIGnrjYdKSQXNXxKjpxUJY2gFov3U0ABa/O1pqkTA8ZBEvmyjuz6w3FPt2zw5XiKv1mKla3OLafnh07S9rpm+NVqoAQylgrBm+Ki1JyrJyIlybDhtmBgjmTz///gQf5Bz4SzqpzDRI1ekS+S1IB+8u+AMZSFXzG8xS4GkDeTvPttKdx6roLn7i6TGwzRMxLYpieTnuQYagHp3CSzxpB21EP7rDsw6J7tLD5fRnoYWxC8ddF0jmd39MCvW0I/qjfTcyVq6Pb9c8mkPQKOmhQdJ53hmYiyFa9W9OjDXHDxJexuaKUL4PI7ABzEpNujHN0bZZc+F6UoJNa55qMVC131TSlXQPG7EZvOiU4dH68dVwDl3CTwFtLp1GRB0b6Kcb76A0upAo0nO1wS5ofBVt8fKCfRLZ2ARCCq7a4y0FSbOmQk3S6/ABFyovPv9yRr6FFqcU9flmpOQkOdlpdHTGQlUyU1VCC9xdj4cBKLL26y07lSN9OdqEKjTebSsXXihOnggAmsEiS77Pj0Imgct4rxuC5w3CU1hs5yKYPE+fF8UBvCloZX2IReU/BHwSqU8uQsiguiu+N4l5w5E091MHMo1XtZkebCwijaV1Egu84HxUfRYqmzy8/XBtJCPsXeR+B6CFEfmy3gnQ7CfJ+lNnidpDDN+kFzdKwLS9DCdSBBUbm6n0m4Om5379qmJNFm0n5KhOZ/PmiBtyeCh+IkoHQez2289X+lboblrud6Fb9PBrKWlS8iHja1U0NhCO6GVG9PjaJ7o0IzB9XfgWpfCL1aaO6SgxAtOvF4SHamlKJ5UhzNauSxfekIgy6fAXS5bsPjZhhnucHRpZpBOTckBXYn8OJguo6ew33u5opEi8fNH0eF0DtXKmqIqKjVZJb+m7Ua2VMeA8C/h+67KLaEDMOVZ7EM5ncGxUXAdlTBfKT1y8D3Zz7tHN/3fYU8J/EJEIL0r7LVjZrkvF4uBca+PnTwPoAH+6fnSOnoYZsbEbkTF8TmCgZo1EGQnwbRez0giM34vb7fRbfkVlBRQQ0Zon4H/QQQUM/KcemuHlMbEYwIKm80SsXYc90RJHW1FpA2Fhr1d1UgF3L1xRl0ckyQm0MLa515SzRvnWzwlkOut/wA/diWA1Fo6UFVYKANpwwL4og8kP6iS1mofKa6jnFqDRHBBS3vXIKBl2WFy3zYfg24BQZLz5wUjnhVN12AdICq3qQ2BIpCWINj8F+Yr9UlA0C74uelfFUkmnMvLm85HU0E675nhOpmbDidN7e4SmEMDrHsMJG+63ObDDb5TY5B+X5cSRVrWHGdExCCPoGQrMMkESakGBhKOnO9RkRBz1dAZ63iQqt5avlWcf1VcBE0IC+ja1obzlVhsMnlqsUrnkH0zn5/nYScCUT3X2q7zx7nwv70lkP1gkWtdMw29gvSkHDfJ/blXJ4/DBbrt3RNR17mQFIRBv3VZAvyjjr6D9r0jaWw/t4Tj9yF47Kxrkvzl9oxEisX/coNWOqda1bUzQWwFeQQJ+k1Ii9rw+0a4ETd3LmyhfnYluEogP/G42dUo0giTXHX0DFngz3iR59PLk+lKREhNt0FxRbAIg/osM4VuiAqVWlt34H/aXEkvoJl3F1TSKZOFMqGB+2alSit6wRpV5/mZTE6btk1Pog0ivVl9vJJOSNrpsvpx7fs3b7ox3eUNYK1LbX5oyV5oybJDJfTalCRaGIG8DEQdg2/kLjJb5iSkM+miocCdmZXflaPMM3fljf1OuYqqEGiuzSuhbcgrZ8PXbkfwKEfaxNfgieMVu5mhAZ1p0P3HK+j1SoNr5++SvwMVviKQo9CTwKuuNck09HFDq7Se8bvkKLoZ/oqTWIZTzsDMtyFaPnu6nuqlSOvG4DBJxcjxFiB94conO2GM1Hhg4jqLVwSjD2Hqz5SeRZ0Mv+deBcKZ/SZv+4EX0kLe7pXpKolnEEnvO1FNf0CKMQXkjcMAOaacgbbkQ1uauSvtDChur1KoyYxzbSqrpxfgdzMQjLitz3liLdKnozh/LSfS8Mvkfvn2BMmvFvApgVxsrhZZuWt3JDl1LRnglvY1wUQNbV0Rlp25zsvnvdmd4RzspL+BljkbCl6enx/TfdHVg92dei7tnvZooKxlrG2dKYyPe+s8Wd6fn2fgl0JZFCGQZT3wOV2csra/ss1XBPLs3M5x4CIj7zXy4HEvT1flOLzf1Nc6wQiUf5EHj3l5Q6DT2d5M3q7eDb3wawCWUx+rbkoSyLJHNBpaRyh5/wNuEB0nGgoCWfhp9cWigz2SZJe47wavgr+Pboaj8lUkv4JpJMjzJL/Fw+jtiXy5tYMXXnhD9ivDmDjub/Lrp35Loqk9nAh01sy/Fn7x1DAj7wMgC9jm0/xdoZvlV4rwBvWnqJ92+CDJURFl2WRLfH1yJXdn8VYIfvXT5SS/w8owyMQdE7kdT2SOUhcZjHeocjf7HpKftVgnNEIpaRNZAWscd434NVQmJQfXZ7vCoRJbwlQ+q/pPk/w2t03CF10vgo67L2DsKbwJihe++eWLHyvie/vhoE8CVc49eL7fc8y19D4Blp6vAOUH/Pghub5eAcr5Zil1vQK0wNtcbkDph4NeBMqHqmj58QNk1WgG4yW0pQK7u88fDauX0KoopN1MFo3WdQ0cazKSUg+fuTLnSvsud8WuUgMq1wnsUI++o3u4ROFRAkdllEDF5P8CDACK7P+SuYYe/QAAAABJRU5ErkJggg==";
}
- (IBAction)actPublish:(id)sender {
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@product/register",urlBase];
    NSString* title = [[NSString alloc ]initWithFormat:@"%@ ", _txtWathDoSell.text];
    NSString* description=[[NSString alloc]initWithFormat:@"%@ ", _txtDescription.text];
    NSString* coordinates = [[NSString alloc] initWithFormat:@"%f;%f",_lat,_lon];
    NSString* serviceTypeId= @"1";
    if ([_txtCategory.text compare:@"Unlock"]==NSOrderedSame) {
        serviceTypeId = @"1";
    }
    if ([_txtCategory.text compare:@"Repair"]==NSOrderedSame) {
        serviceTypeId = @"2";
    }
    if ([_txtCategory.text compare:@"Activation"]==NSOrderedSame) {
        serviceTypeId = @"3";
    }
   
    /////////////////////////////
    NSMutableArray* imgs = [[NSMutableArray alloc] init];
   
    NSString* imgname=@"";
    NSString* img64= @"";
    if (!CGSizeEqualToSize(_img1.image.size, CGSizeZero))
    {
         imgname=@"img1.png";
         img64= [self imageToNSString:_img1.image];
         NSArray *imgobjects = [NSArray arrayWithObjects:img64,imgname,nil];
         
         NSArray *imgkeys = [NSArray arrayWithObjects:@"ImageBase64", @"ImageFileName", nil];
         
         NSDictionary *images = [NSDictionary dictionaryWithObjects:imgobjects forKeys:imgkeys];
         [imgs addObject:images];
     }
    if (!CGSizeEqualToSize(_img2.image.size, CGSizeZero))
    {
        imgname=@"img3.png";
        img64= [self imageToNSString:_img2.image];
        NSArray *imgobjects = [NSArray arrayWithObjects:img64,imgname,nil];
        
        NSArray *imgkeys = [NSArray arrayWithObjects:@"ImageBase64", @"ImageFileName", nil];
        
        NSDictionary *images = [NSDictionary dictionaryWithObjects:imgobjects forKeys:imgkeys];
        [imgs addObject:images];
    }
    if (!CGSizeEqualToSize(_img3.image.size, CGSizeZero))
    {
        imgname=@"img2.png";
        img64= [self imageToNSString:_img3.image];
        NSArray *imgobjects = [NSArray arrayWithObjects:img64,imgname,nil];
        
        NSArray *imgkeys = [NSArray arrayWithObjects:@"ImageBase64", @"ImageFileName", nil];
        
        NSDictionary *images = [NSDictionary dictionaryWithObjects:imgobjects forKeys:imgkeys];
        [imgs addObject:images];
    }
//    [self imageToNSString:_img1.image];
    
    NSError* error;

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:imgs
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

     NSString *post = [NSString stringWithFormat:@"Title=%@&Description=%@&ContactByPhone=%@&ContactByText=%@&Price=%@&Coordinates=%@&ApplicationUserId=%@&EmailDisplayId=%d&ServiceTypeId=%d&CategoryId=%d&MakeId=%d&ItemConditionId=%@&Size=%@&ProductImages=%@",title,description,@"true",@"true",_txtPrice.text,coordinates,@"0e4879f5-e286-4356-9eaf-cbccce70caf8",1,7,1,1,serviceTypeId,@"1x2",jsonString];
   ////
      NSString *myJson =@"{";
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'Title'",title];
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'Description'",description];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'ContactByPhone'",@"true"];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'ContactByText'",@"true"];
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'Price'",_txtPrice.text];
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'Coordinates'",coordinates];
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'ApplicationUserId'",@"0e4879f5-e286-4356-9eaf-cbccce70caf8"];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'EmailDisplayId'",@"1"];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'ServiceTypeId'",@"7"];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'CategoryId'",serviceTypeId];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'MakeId'",@"1"];
      myJson = [NSString stringWithFormat:@"%@%@:%@,",myJson,@"'ItemConditionId'",@"1"];
      myJson = [NSString stringWithFormat:@"%@%@:'%@',",myJson,@"'Size'",@"1x2"];
      myJson = [NSString stringWithFormat:@"%@%@:%@",myJson,@"'ProductImages'",jsonString];
      myJson = [NSString stringWithFormat:@"%@%@",myJson,@"}"];
    
    
//    NSArray *objects = [NSArray arrayWithObjects:title,description,@"true",@"true",_txtPrice.text,coordinates,@"0e4879f5-e286-4356-9eaf-cbccce70caf8",@"1",@"7",@"1",@"1",serviceTypeId,@"1x2",jsonString, nil];
//                        
//    NSArray *keys = [NSArray arrayWithObjects:@"Title", @"Description", @"ContactByPhone", @"ContactByText", @"Price", @"Coordinates",  @"ApplicationUserId", @"EmailDisplayId",@"ServiceTypeId",@"CategoryId",@"MakeId", @"ItemConditionId", @"Size",@"ProductImages", nil];
                        
//    NSDictionary *questionDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
//       NSData * jsonData1 = [NSJSONSerialization dataWithJSONObject:myJson options:NSJSONReadingMutableContainers error:&error];

       
    NSLog(@"jsonRequest is %@", myJson);
    /////////////////////////////
    NSData *postData = [myJson dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *json = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    NSLog(@"jsonRequest is %@", json);
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;
    
    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    self.view.userInteractionEnabled = NO;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&err];
    [self.overlayView removeFromSuperview];
    self.view.userInteractionEnabled = YES;

   	 if (data.length > 0 && err == nil)
    {
        NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                              options:0
                                                                 error:NULL];
        
    }
   


    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actPublishFacebook:(id)sender {
    if (self.isPublishOnFacebook) {
        self.imgFacebook.image = [UIImage imageNamed:@"toggle.png"];
    }else{
        self.imgFacebook.image = [UIImage imageNamed:@"toggle-filled.png"];
    }
    
    self.isPublishOnFacebook = !self.isPublishOnFacebook;
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    if (  _choiceimg == 1)
        self.img1.image = chosenImage;
    if (  _choiceimg == 2)
        self.img3.image = chosenImage;
    if (  _choiceimg == 3)
        self.img2.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)takePic1:(id)sender {
    _choiceimg = 1;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
   
    
    [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (IBAction)takePic2:(id)sender {
      _choiceimg = 2;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (IBAction)takePic3:(id)sender {
      _choiceimg = 3;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (IBAction)actPickerDone:(id)sender {
   
    [self hideInmediately];
    [self.txtCategory setText:_catSel.descriptionPlan];
}

- (IBAction)actPickerCancel:(id)sender {
     [self hideInmediately];
}

-(void) hideShowPicker{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if(!_isComboShow){
            
            int y = DEVICE_HEIGTH - _pickerView.frame.size.height;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
            
        }else{
            
            int y = DEVICE_HEIGTH + 10;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
        }
        
        _isComboShow = !_isComboShow;
        
    }];
}

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_categories count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50.0f;
}




- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:@"Roboto-Regular" size:25]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
    }
    
    // Fill the label text here
    DataPlan *platAux = [_categories objectAtIndex:row];
    NSString *formatMatric = [[NSString alloc]initWithFormat:@"%@",platAux.descriptionPlan];
    
    
    tView.text=formatMatric;
    return tView;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    _catSel = [_categories objectAtIndex:row];
    
}

- (void) hideInmediately{
    
    int y = DEVICE_HEIGTH + 10;
    [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
    
    _isComboShow = NO;
}
@end
