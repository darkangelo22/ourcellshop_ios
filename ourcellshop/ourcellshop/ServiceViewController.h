//
//  ServiceViewController.h
//  
//
//  Created by René Abilio on 1/1/16.
//
//

#import <UIKit/UIKit.h>
#import "DataService.h"
#import "ServiceTableViewCell.h"

@class ViewController;

@interface ServiceViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property (weak, nonatomic) IBOutlet UITableView *tableServices;
@property (strong, nonatomic) NSMutableArray *servicesArrayUnlock;
@property (strong, nonatomic) NSMutableArray *servicesArrayRepair;
@property (strong, nonatomic) NSMutableArray *servicesArrayActivation;

@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *numberAlert;
@property (weak, nonatomic) IBOutlet UILabel *titleService;
@property (weak, nonatomic) IBOutlet UILabel *labBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgUnlock;
@property (weak, nonatomic) IBOutlet UIImageView *imgTool;
@property (weak, nonatomic) IBOutlet UIImageView *imgWiFi;
@property (weak, nonatomic) IBOutlet UITextField *txtFilterSearch;
@property (weak, nonatomic) IBOutlet UITextField *txtGeneralSearch;


//Repair interface
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigth;


@property int numberSectionSearch;//1 Unlock, 2 Repair, 3 Activation

- (IBAction)actBack:(id)sender;

//Color Tabs
-(void)onLoad;
-(void)cleanColorsTools;
-(void)colorToUnlock;
-(void)colorToTool;
-(void)colorToWiFi;
- (IBAction)actShowFilter:(id)sender;
- (IBAction)actTool:(id)sender;
- (IBAction)actWifi:(id)sender;
- (IBAction)actUnlock:(id)sender;
-(void)setColorSearchType;
- (IBAction)actNewAd:(id)sender;



@end
