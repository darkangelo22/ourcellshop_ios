//
//  RouteService.m
//  t2parking
//
//  Created by René Abilio on 9/12/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "RouteService.h"

@implementation RouteService

+(NSString*) getProviders{
    
        NSString *urlBase = URL_BASE;
    
        NSString *request = [[NSString alloc]initWithFormat:@"%@Provider?fields=id,name,description,countryid,imageurl",urlBase];
            return request;
}

+(NSString*) getProviders_photo{
    
    NSString *urlBase = URL_BASE_PHOTO_PROVIDER;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@",urlBase];
    return request;
}

+(NSString*) getProviders_version{
    
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@platformVersion",urlBase];
    return request;
}

+(NSString*) getCountries{
    
    NSString *urlBase = URL_BASE;
    
    NSString *validateNumber = [[NSString alloc]initWithFormat:@"%@Country",urlBase];
    
    return validateNumber;
}

+(NSString*) getUser:(NSString*)userid{
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@Accounts/User/%@?fields=email,username,phonenumber,transactions,balance",urlBase,userid];
    
    return request;
    
}

+(NSString*) getServices
{
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@Service?fields=id,Title,Description,ContactByPhone,ContactByText,Price,EmailDisplayId,serviceTypeId,coordinates,username",urlBase];
    return request;
}
+(NSString*) getProducts
{
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@Product?fields=productimages,size,itemConditionId,makeId,categoryid,coordinates,id,title,description,contactByPhone,contactByText,price,emaildisplayid,username",urlBase];
    return request;
}
+(NSString*) getMessages{
    
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@Message?fields=id,date,from,subject,body,fromUserEmail,toUserName,applicationUserId,messageStatusId",urlBase];
    return request;
}

+(NSString*) getCategories
{
    NSString *urlBase = URL_BASE;
    
    NSString *request = [[NSString alloc]initWithFormat:@"%@providerservice?fields=id,description,providerid,plans",urlBase];
    return request;
}

+(NSString*) getToken:(NSString*)user passp:(NSString*)pass{
    NSString *urlBase = URL_BASE_PHOTO_PROVIDER;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@oauth/token",urlBase];
    
    NSString *post = [NSString stringWithFormat:@"username=%@&password=%@&grant_type=%@",user,pass,@"password"];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];

    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&err];
    if (data.length > 0 && err == nil)
    {
        NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                              options:0
                                                                error:NULL];
        
        return [[NSString alloc] initWithFormat:@"%@ %@",[greet objectForKey:@"token_type"],[greet objectForKey:@"access_token"]];
    }
    
    return @"";
}
//
//
//+(NSString*) getValidatePin:(NSString*)pin numberPhone:(NSString*)phone{
//    
//    NSString *validateNumber = VALIDATE_NUMBER(phone);
//    NSString *validatePin = [[NSString alloc]initWithFormat:@"%@&pin=%@",validateNumber,pin];
//    return validatePin;
//    
//}
//
//
//+(NSString*) requestParking:(NSString*)plate minutes:(NSString*)min place:(NSString*)place hour:(NSString*)hour{
//    
//    NSString *urlBase = URL_BASE;
//    NSString *phone = [AdminNSUserDefaults getPhoneNumber];
//    UIDevice *device = [UIDevice currentDevice];
//    NSString *deviceId = device.identifierForVendor.UUIDString;
//    
//    NSString *request = [[NSString alloc]initWithFormat:@"%@solicitarEstacionamiento?phone=%@&deviceId=%@&plate=%@&minutes=%@&plaza=%@",urlBase,phone,deviceId,plate,min,place];
//
//    
//    BOOL isNullHour = [hour isEqualToString:@""];
//    if (!isNullHour) {
//        request = [[NSString alloc]initWithFormat:@"%@&hour=%@", request,hour];
//    }
//    
//    return request;
//    
//}
//
//
//+(NSString*) consultationParking:(NSString*)appTransaccionId{
//    
//    NSString *urlBase = URL_BASE;
//    NSString *phone = [AdminNSUserDefaults getPhoneNumber];
//    
//    UIDevice *device = [UIDevice currentDevice];
//    NSString *deviceId = device.identifierForVendor.UUIDString;
//    
//    NSString *queryEstacionamiento = [[NSString alloc]initWithFormat:@"%@consultarEstacionamiento?phone=%@&appTransactionId=%@&deviceId=%@",urlBase,phone,appTransaccionId,deviceId];
//    return queryEstacionamiento;
//    
//}
//
//
//+(NSString*) consultationRegister{
//    
//    NSString *urlBase = URL_BASE;
//    NSString *phone = [AdminNSUserDefaults getPhoneNumber];
//    
//    NSString *queryParking = [[NSString alloc]initWithFormat:@"%@consultarRegistro?phone=%@",urlBase, phone];
//    return queryParking;
//    
//}
//
//+(NSString*) historyTickets{
//    
//    NSString *urlBase = URL_BASE;
//    NSString *phone = [AdminNSUserDefaults getPhoneNumber];
//    
//    NSString *historyParking = [[NSString alloc]initWithFormat:@"%@historicoTickets?phone=%@&meses_retro=36",urlBase, phone];
//    return historyParking;
//    
//}
//
//+(NSString*) getPriceOf30{
//    
//    NSString *urlBase = URL_BASE;
//    
//    NSString *price30 = [[NSString alloc]initWithFormat:@"%@getPriceOf30",urlBase];
//    return price30;
//    
//}
//
//+(NSString*) getConsultaPatenteSucive:(NSString*)patente padron:(NSString*)padro{
//    NSString *urlBase = @"http://demo.t2voice.com/dondeestaciono/t2parking/sucive?consulta=consultaPatente";
//    
//    NSString *suciveUrl = [[NSString alloc]initWithFormat:@"%@&patente=%@&padron=%@",urlBase, patente,padro];
//
//    return suciveUrl;
//}
//
//+(NSString *)uuidString {
//    // Returns a UUID
//    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
//    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
//    CFRelease(uuid);      return uuidString;
//
//}
//
//+(BOOL)isAntel:(NSString*)phone{
//    BOOL isTelcoAntel = [phone hasPrefix:@"099"] || [phone hasPrefix:@"098"] || [phone hasPrefix:@"093"] || [phone hasPrefix:@"092"] || [phone hasPrefix:@"091"];
//    return isTelcoAntel;
//}
//
//+(BOOL)isClaro:(NSString*)phone{
//    
//    BOOL isTelcoClaro = [phone hasPrefix:@"097"] || [phone hasPrefix:@"096"];
//    return isTelcoClaro;
//    
//}
//
//+(BOOL)isMovistar:(NSString*)phone{
//
//    BOOL isTelcoMovistar = [phone hasPrefix:@"095"] || [phone hasPrefix:@"094"];
//    return isTelcoMovistar;
//    
//}

@end
