//
//  MessageSendViewController.h
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import "ViewController.h"

@class ViewController;

@interface MessageSendViewController : UIViewController< UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property(assign) ViewController* viewController;
@property (strong, nonatomic) NSMutableArray *messages;
@property (weak, nonatomic) IBOutlet UITableView *tableSendMessage;

@property (weak, nonatomic) IBOutlet UIImageView *imgInbox;
@property (weak, nonatomic) IBOutlet UIImageView *imgSend;
@property (weak, nonatomic) IBOutlet UIImageView *imgSearch;
@property (weak, nonatomic) IBOutlet UILabel *labInbox;
@property (weak, nonatomic) IBOutlet UILabel *labSend;
@property (weak, nonatomic) IBOutlet UILabel *labSearch;
- (IBAction)actInbox:(id)sender;
- (IBAction)actSend:(id)sender;
- (IBAction)actSearch:(id)sender;
- (NSString*) getMonth:(NSString *) mon;
//Color Tabs
-(void)onLoad;
-(void)cleanColorsMsg;
-(void)colorToInbox;
-(void)colorToSend;
-(void)colorToSearch;

@end
