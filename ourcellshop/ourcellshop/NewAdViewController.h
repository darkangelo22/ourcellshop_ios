//
//  NewAdViewController.h
//  
//
//  Created by René Abilio on 3/1/16.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DataPlan.h"
@class ViewController;

@interface NewAdViewController : UIViewController<UIImagePickerControllerDelegate>

@property(assign) ViewController* viewController;
@property int choiceimg;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conHeigthScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleNewAd;
@property (weak, nonatomic) IBOutlet UIButton *labCancel;
@property (weak, nonatomic) IBOutlet UILabel *labPhoto;
@property (weak, nonatomic) IBOutlet UILabel *labCategory;
@property (weak, nonatomic) IBOutlet UILabel *labElectronic;
@property (weak, nonatomic) IBOutlet UITextField *txtWathDoSell;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;
@property (weak, nonatomic) IBOutlet UILabel *labDollar;
@property (weak, nonatomic) IBOutlet UILabel *labDescription;
@property (weak, nonatomic) IBOutlet UILabel *labShareFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnPublish;
@property (weak, nonatomic) IBOutlet UIImageView *imgFacebook;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerAceptar;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancel;
@property (weak, nonatomic) IBOutlet UIButton *actPickerCancel;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property double lat;
@property double lon;
@property BOOL isComboShow;
@property DataPlan* catSel;
- (void)clean;
- (IBAction)actPickerDone:(id)sender;
- (IBAction)actPickerCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img3;

@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) NSMutableArray *categories;
//Vars
@property BOOL isPublishOnFacebook;
- (IBAction)actCategory:(id)sender;
- (IBAction)actCancel:(id)sender;
- (IBAction)actPublish:(id)sender;
- (IBAction)actPublishFacebook:(id)sender;
- (IBAction)takePic1:(id)sender;
- (IBAction)takePic2:(id)sender;
- (IBAction)takePic3:(id)sender;


@end
