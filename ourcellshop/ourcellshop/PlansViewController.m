//
//  PlansViewController.m
//  
//
//  Created by René Abilio on 9/1/16.
//
//

#import "PlansViewController.h"
#import "ViewController.h"
#import "AdminNSUserDefaults.h"

@interface PlansViewController ()

@end

@implementation PlansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.isSearchClientInfo = NO;
    
    
    
    //Header
    [self.titlePlans setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //Sections
    [self.labSectionPlans setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labSectionPaymentMethod setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labSectionAccountInfo setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    
    //Account Info
    [self.labName setFont:[UIFont fontWithName:@"Roboto-Light" size:13.0]];
    [self.labCurrentBalance setFont:[UIFont fontWithName:@"Roboto-Light" size:13.0]];
    [self.labLastTransaction setFont:[UIFont fontWithName:@"Roboto-Light" size:13.0]];
    [self.labPrice setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    [self.labDate setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    
    //Payment Method
    [self.labApplePay setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    [self.labAccountBalance setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    //Load Data
    self.imgProvider.image = [UIImage imageNamed:self.dataProvider.imageName];
    [self.btnPay setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    self.btnPay.layer.cornerRadius = 5;
    
    
    self.isApplePay = YES;
    [self refreshPaymentMethod];

//    if (IS_IPHONE_5)
//    {
//        self.conPlanHeight.constant = self.conPlanHeight.constant + 27;
//        self.conServiceHeight.constant = self.conServiceHeight.constant + 27;
//
//    }
//    if (IS_IPHONE_6)
//    {
//        self.conPlanHeight.constant = self.conPlanHeight.constant + 37;
//        self.conServiceHeight.constant = self.conServiceHeight.constant + 37;
//    }
//    if (IS_IPHONE_6_PLUS)
//    {
//        self.conPlanHeight.constant = self.conPlanHeight.constant + 42;
//        self.conServiceHeight.constant = self.conServiceHeight.constant + 42;
//    }
    
    //Load Logo
    if (self.dataProvider != nil) {
        UIImage *image = [[UIImage alloc] initWithData:self.dataProvider.image];
        [self.imgProvider setImage:image];
    }
    _plans = [[NSMutableArray alloc]init];
    
    _services = [[NSMutableArray alloc]init];
    //LLENANDO DETALLES DE LOS PLANES
    for(DataPlan* plan in self.dataProvider.Plans)
    {
        if (_category == plan.idc)
        {
        if (plan.type.intValue == 4)
          [_plans addObject:plan];
        if (plan.type.intValue == 5)
          [_services addObject:plan];
        }
    }
    
    [self.tablePlans setDataSource:self];
    [self.tablePlans setDelegate:self];
    
    [self.tableServices setDataSource:self];
    [self.tableServices setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    _plans = [[NSMutableArray alloc]init];
    
    _services = [[NSMutableArray alloc]init];
    //LLENANDO DETALLES DE LOS PLANES
    for(DataPlan* plan in self.dataProvider.Plans)
    {
        if (_category.integerValue == plan.idc.integerValue)
        {
        if (plan.type.intValue == 4)
            [_plans addObject:plan];
        if (plan.type.intValue == 5)
            [_services addObject:plan];
        }
    }
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tablePlans reloadData];
         [self.tableServices reloadData];
    });
    
}
- (IBAction)actPay:(id)sender {
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}

- (IBAction)actCancel:(id)sender {
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}

- (IBAction)actBack:(id)sender {
    [self.viewController goHome];
    [self dismissViewControllerAnimated:YES completion:^{ [self.viewController refreshTableHomeProviders]; }];
}

- (IBAction)actApplePay:(id)sender {
    self.isApplePay = YES;
    [self refreshPaymentMethod];
}

- (IBAction)actAccountBalance:(id)sender {
    self.isApplePay = NO;
    [self refreshPaymentMethod];
}


-(void)refreshPaymentMethod{
    
    UIColor *blueColor = [UIColor colorWithRed:44.0/255.0 green:79.0/255.0 blue:149.0/255.0 alpha:1.0];
    
    UIColor *darkColor = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0];
    
    if (self.isApplePay) {
        self.imgApplePay.image = [UIImage imageNamed:@"check.png"];
        self.imgAccountBalance.image = [UIImage imageNamed:@"check-white.png"];
        [self.labApplePay setTextColor:blueColor];
        [self.labAccountBalance setTextColor:darkColor];
        [self.labName setText:@"Anonymous"];
        [self.labPrice setText:@"$0 USD"];
        [self.labDate setText:@"N/A"];
        
    }else{
        if([AdminNSUserDefaults isUserLogued])
        {
       
        self.imgApplePay.image = [UIImage imageNamed:@"check-white.png"];
        self.imgAccountBalance.image = [UIImage imageNamed:@"check.png"];
        [self.labApplePay setTextColor:darkColor];
        [self.labAccountBalance setTextColor:blueColor];
        
        [self updateClientInfo];
        }
        else{
            
            PinViewController* pin = [self.viewController loadController:[PinViewController class]];
            pin.viewController = self.viewController;
            [self presentViewController:pin animated:YES completion:^{[self.viewController closeMenu];}];
            
        }
    }
   
}


#pragma mark Tabla

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case 0:
             return [_plans count];
            break;
        case 1:
             return [_services count];
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0)
    {
    static NSString *simpleTableIdentifier = @"PlansTableViewCell";
    
    PlansTableViewCell *cell = (PlansTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //    if (cell == nil)
    //    {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlansTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    //    }
    
    DataPlan* auxPlaN = [_plans objectAtIndex:indexPath.row];
    
    //Data Fill
    cell.plan = auxPlaN;
    [cell updateViewPlanData];
    cell.btnSelectPlan.tag = indexPath.row;
    [cell.btnSelectPlan addTarget:self action:@selector(selectPlanClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"PlansTableViewCell";
        
        PlansTableViewCell *cell = (PlansTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        //    if (cell == nil)
        //    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlansTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        //    }
        
        DataPlan* auxPlaN = [_services objectAtIndex:indexPath.row];
        
        //Data Fill
        cell.plan = auxPlaN;
        [cell updateViewPlanData];
        cell.btnSelectPlan.tag = indexPath.row;
        [cell.btnSelectPlan addTarget:self action:@selector(selectServiceClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    DataPlan *planEditAux = [self.plans objectAtIndex:index];
}

-(void)selectServiceClicked:(UIButton*)sender
{
    int tagBtnIndex = sender.tag;
    
    DataPlan *planAux = [self.services objectAtIndex:tagBtnIndex];
    
    
    //Clean isSelectPlan
    for (int i=0; i<[self.plans count]; i++) {
        DataPlan *planX = [self.plans objectAtIndex:i];
        planX.isSelectedPlan = NO;
    }

    //Clean isSelectPlan
    for (int i=0; i<[self.services count]; i++) {
        DataPlan *planX = [self.services objectAtIndex:i];
        planX.isSelectedPlan = NO;
    }
    
    //Assign YES to planClicked (planAux)
    planAux.isSelectedPlan = YES;
    
    //Reload Table to RePaint
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tablePlans reloadData];
        [self.tableServices reloadData];
    });
}

-(void)selectPlanClicked:(UIButton*)sender
{
    int tagBtnIndex = sender.tag;
    
    DataPlan *planAux = [self.plans objectAtIndex:tagBtnIndex];
    
    //Clean isSelectPlan
    for (int i=0; i<[self.plans count]; i++) {
        DataPlan *planX = [self.plans objectAtIndex:i];
        planX.isSelectedPlan = NO;
    }
    
    //Clean isSelectPlan
    for (int i=0; i<[self.services count]; i++) {
        DataPlan *planX = [self.services objectAtIndex:i];
        planX.isSelectedPlan = NO;
    }
    //Assign YES to planClicked (planAux)
    planAux.isSelectedPlan = YES;
    
    //Reload Table to RePaint
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tablePlans reloadData];
         [self.tableServices reloadData];
    });
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)updateClientInfo{
    
    if (!self.isSearchClientInfo) {
        
        self.isSearchClientInfo = YES;
        
        //OBTENIENDO TOKEN
        NSString *token = GET_TOKEN(@"SuperPowerUser", @"MySuperP@ssword1");
        
        //INICIALIZANDO SERVICIO DE USUARIOS
        NSString *urlUserService = GET_USER(@"0e4879f5-e286-4356-9eaf-cbccce70caf8");
        
        NSURL *urlUser= [NSURL URLWithString:urlUserService];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlUser
                                                 cachePolicy:NSURLRequestUseProtocolCachePolicy
                                             timeoutInterval:300.0];
     
        [request addValue:[NSString stringWithFormat:@"%@", token] forHTTPHeaderField:@"Authorization"];
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE USUARIOS
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:NULL];
                 NSString* balance =[greet objectForKey:@"balance"];
                 [self.labPrice setText:[[NSString alloc] initWithFormat:@"$%@ USD",balance]];
                 [self.labName setText:[greet objectForKey:@"username"]];
                 
                 
                 NSMutableArray *transactionArray = [greet objectForKey:@"transactions"];
                 if ([transactionArray count] >0)
                 {
                     NSString *dateLastTransaction = [[transactionArray objectAtIndex:0] objectForKey:@"date"];
                     NSArray *dateElements = [dateLastTransaction componentsSeparatedByString:@"-"];
                 
                     NSString *year = [dateElements objectAtIndex:0];
                     NSString *month = [dateElements objectAtIndex:1];
                     NSString *day = [dateElements objectAtIndex:2];
                 
                     NSString *dayJoin = [NSString stringWithFormat:@"%c%c",[day characterAtIndex:0], [day characterAtIndex:1]];
                     NSString *finalDateFormat = [NSString stringWithFormat:@"%@/%@/%@",dayJoin,month,year];
                     [self.labDate setText:finalDateFormat];
                 }
                 else
                     [self.labDate setText:@"N/A"];
                 self.isSearchClientInfo = NO;
             }
         }];
    }
    
}


@end
