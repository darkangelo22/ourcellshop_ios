//
//  LoginUpViewController.h
//  
//
//  Created by René Abilio on 23/12/15.
//
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface LoginUpViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conLogoTop;

@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

- (IBAction)actCancel:(id)sender;
- (IBAction)actSignUp:(id)sender;
-(void)moveToMain;

@end
