//
//  AppDelegate.m
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "AdminNSUserDefaults.h"
#import "Util.m"

@interface AppDelegate ()

@property (nonatomic, strong) UIImageView *splashImage;
@property (nonatomic, strong) UIActivityIndicatorView *gear;

@end

@implementation AppDelegate

- (void) setUpSplash {
    
    // splah image
    NSString *nameImageSplash = @"Default@2x";
    if(IS_IPHONE_5)
        nameImageSplash = @"Default-568h@2x";
    if(IS_IPHONE_6)
        nameImageSplash = @"Default-iPhone6";
    if(IS_IPHONE_6_PLUS)
        nameImageSplash = @"Default-iPhone6Plus";
    
    UIImage *image = [UIImage imageNamed:nameImageSplash];
    self.splashImage = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.splashImage.image = image;
    [self.window insertSubview:self.splashImage aboveSubview:self.window.rootViewController.view];
    
    // gear
    self.gear = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.gear.frame = CGRectMake(0, 0, 5,5);
    self.gear.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.splashImage addSubview:self.gear];
    [self.gear startAnimating];
}

- (void) removeSplash
{
    // gear
    [self.gear stopAnimating];
    
    // splash image
    [self.gear removeFromSuperview];
    [self.splashImage removeFromSuperview];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewVC = [sb instantiateViewControllerWithIdentifier:@"initialViewController"];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = viewVC;
    
    
    [self.window makeKeyAndVisible];
    
    
    //Setting UserNotLogued
    UserLogued *userLogued = [[UserLogued alloc]initEmptyUser];
    [AdminNSUserDefaults saveUserLogued:userLogued];
    
    
    // Show splash
    [self setUpSplash];
    
    // Remove splash
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(removeSplash) userInfo:nil repeats:NO];
    
    return YES;
    
}

- (id)loadController:(Class)classType {
    NSString *className = NSStringFromClass(classType);
    UIViewController *controller = [[classType alloc] initWithNibName:className bundle:nil];
    return controller;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
