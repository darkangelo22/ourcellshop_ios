//
//  DataService.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataService : NSObject

@property NSString* idService;
@property NSString* titleService;
@property NSString* descriptionService;
@property NSString* coordinates;
@property NSDate* datePublish;
@property NSString* owner;

-(id)initWithData:(NSString*)idS titleServiceP:(NSString*) titl descriptionServiceP:(NSString*) desc dateP:(NSDate*) datePubli coordinates:(NSString*)coordinatesp  ownerp:(NSString*)owner;

-(id)initWithService:(DataService*)dataService;

@end
