//
//  keyValue.m
//  
//
//  Created by René Abilio on 4/2/16.
//
//

#import "keyValue.h"

@implementation keyValue

-(keyValue*)initWithKey:(NSString*)keyp value:(NSMutableArray*)valuep{
    self.key = keyp;
    self.value = valuep;
    return self;
}
@end
