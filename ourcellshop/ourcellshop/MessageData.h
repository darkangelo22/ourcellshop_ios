//
//  MessageData.h
//  
//
//  Created by René Abilio on 14/1/16.
//
//

#import <Foundation/Foundation.h>

@interface MessageData : NSObject

@property NSString* from;
@property NSString* to;
@property NSString* subject;
@property NSString* body;
@property NSString* date;
@property NSString* imgUrl;

-(id)initWithData:(NSString*)fromp to:(NSString*)top subject:(NSString*)subjectp body: (NSString*)bodyp date:(NSString*) datep imgUrl:(NSString*) imgUrlp;

-(id)initWithMessage:(MessageData*)msg;

@end
