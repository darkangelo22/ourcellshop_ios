//
//  BuyViewController.m
//  
//
//  Created by René Abilio on 1/1/16.
//
//

#import "BuyViewController.h"
#import "ViewController.h"

@interface BuyViewController ()

@end

@implementation BuyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.numberAlert.layer.cornerRadius = 8;
    [self.labBack setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.titleAds setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.txtFilterSearch setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    self.txtFilterSearch.delegate = self;
    
    self.txtFilterSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
//        self.conSpaceRigth.constant = 25;
    }
    
    
    
    [self.tableBuy setDataSource:self];
    [self.tableBuy setDelegate:self];
    //-------------------------------------------------
    //-------------------------------------------------
    
}
-(void)onLoad{
    //Fill Data----------------------------------------
    //-------------------------------------------------
    [self.overlayView removeFromSuperview];
    self.view.userInteractionEnabled = YES;

    //INHABILITANDO CLICK EN PANTALLA CON BOTON GIGANTE
    self.view.userInteractionEnabled = NO;
    //  self.viewController.view.userInteractionEnabled = NO;
    
    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    //INICIALIZANDO SERVICIO DE PAISES
   
    self.buys = [[NSMutableArray alloc]init];
    
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@Product?fields=productimages,size,itemConditionId,makeId,categoryid,coordinates,id,title,description,contactByPhone,contactByText,price,emaildisplayid,username",urlBase];
    
    int ZipCode = self.viewController.FilterProductData.ZipCode;
    int Distance=self.viewController.FilterProductData.Distance;
    int Category=self.viewController.FilterProductData.Category;
    double MinPrice=self.viewController.FilterProductData.MinPrice;
    double MaxPrice=self.viewController.FilterProductData.MaxPrice;
    int Published=self.viewController.FilterProductData.Published;
    
    NSString *post;
    post= [NSString stringWithFormat:@"Distance=%d&Category=%d&Published=%d",Distance,Category,Published];
    
    post = ZipCode != -1 ? [NSString stringWithFormat:@"%@%@=%d",post,@"&ZipCode",ZipCode]:post;
    post = MinPrice != -1 ? [NSString stringWithFormat:@"%@%@=%f",post,@"&MinPrice",MinPrice]:post;
    post = MaxPrice != -1 ? [NSString stringWithFormat:@"%@%@=%f",post,@"&MaxPrice",MaxPrice]:post;
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //[request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;

    
    dispatch_queue_t myQueue = dispatch_queue_create("my queue1", NULL);
    dispatch_async(myQueue, ^{
        
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             [self.overlayView removeFromSuperview];
             self.view.userInteractionEnabled = YES;
             
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:NULL];
                 NSString *urlproviderphoto = GET_PROVIDERS_PHOTO;
                 //LLENANDO LISTA DE PAISES
                 for (id key in greet) {
                     //                                  [self.countries setObject: [key objectForKey:@"description"] forKey:[key objectForKey:@"id"]];
                     NSMutableArray *arrayImag = [[NSMutableArray alloc] init];
                     for (id img in [key objectForKey:@"productimages"]) {
                         NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat: @"%@%@",urlproviderphoto,[img objectForKey:@"imageUrl"]]];
                         NSData *dataimg = [NSData dataWithContentsOfURL:url];
                         UIImage *image = [UIImage imageWithData:dataimg];
                         if (image != nil)
                         [arrayImag addObject:image];
                     }
                     DataBuy *dataBuy1 = [[DataBuy alloc]initWithData:[key objectForKey:@"id"] titleBuyP:[key objectForKey:@"title"] descriptionBuyP:[key objectForKey:@"description"] imagesP:arrayImag coordinatesp:[key objectForKey:@"coordinates"] ownerp:[key objectForKey:@"username"] ];
                     
                     [self.buys addObject:dataBuy1];
                     
                     
                 }
                 //Refreshing table
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.tableBuy reloadData];
                 });
             }
         }];
    });

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actBack:(id)sender {
    [self.viewController backHome];
}

- (IBAction)actShowFilters:(id)sender {
    self.viewController.screenToFilter = 3;
    [self.viewController showFilters];
}

- (IBAction)goMap:(id)sender {
    self.viewController.screenToCancelLogin = 4;
    [self.viewController goMapWithData:_buys];
}

#pragma mark Metodos del Scroll y los TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //self.currSelected = textField;
    [textField setReturnKeyType:UIReturnKeyDone];
    //
    //    CGRect formRect = _scrollView.frame;
    //    float height = [UIScreen mainScreen].bounds.size.height - _scrollView.frame.origin.y - PORTRAIT_KEYBOARD_HEIGHT;
    //    formRect.size.height = height < SCROLL_HEIGHT ? height : SCROLL_HEIGHT;
    //    _scrollView.frame = formRect;
    
    //self.linePin.backgroundColor = [[UIColor blueColor]colorWithAlphaComponent:0.4f];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    //    if ([textField isFirstResponder])
    //    {
    //        CGRect formRect = _scrollView.frame;
    //        formRect.size.height = SCROLL_HEIGHT;
    //        _scrollView.frame = formRect;
    //
    //        [textField resignFirstResponder];
    //
    //    }
    
    
    [textField resignFirstResponder];
    return YES;
}



#pragma mark Tabla Proveedores
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_buys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"BuyTableViewCell";
    
    BuyTableViewCell *cell = (BuyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BuyTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DataBuy* auxPlate = [_buys objectAtIndex:indexPath.row];
    
    //Data Buys
    cell.titleBuy.text = auxPlate.titleBuy;
    cell.descriptionBuy.text =(![auxPlate.descriptionBuy isEqual: [NSNull null]]) ? auxPlate.descriptionBuy :@"";
    cell.coordinates = auxPlate.coordinates;
    cell.viewController = self.viewController;
    [cell initDataCarouselWithArrayImages:auxPlate.arrayImages];
    cell.owner =auxPlate.owner;
    return cell;
    
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    DataBuy *plateEditAux = [self.buys objectAtIndex:index];
    
}

@end
