//
//  CountryViewController.h
//  
//
//  Created by René Abilio on 4/2/16.
//
//

#import <UIKit/UIKit.h>
#import "CountryTableViewCell.h"
#import "DataCountry.h"
@class ViewController;
@class HomeViewController;
@interface CountryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property(assign) HomeViewController* HomeViewController;
@property(assign) ViewController* viewController;
- (IBAction)actBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableCountries;
@property NSMutableArray* countries;
@property NSMutableArray* arrayc;
@end
