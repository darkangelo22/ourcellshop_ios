//
//  PlansViewController.h
//  
//
//  Created by René Abilio on 9/1/16.
//
//

#import <UIKit/UIKit.h>
#import "DataProvider.h"
#import "DataPlan.h"
#import "PlansTableViewCell.h"

@class ViewController;

@interface PlansViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property DataProvider* dataProvider;

@property(assign) ViewController* viewController;

//Table
//Plans
@property BOOL isSearchClientInfo;

@property NSNumber* category;

@property (strong, nonatomic) NSMutableArray *plans;

@property (strong, nonatomic) NSMutableArray *services;

@property (weak, nonatomic) IBOutlet UITableView *tablePlans;

@property (weak, nonatomic) IBOutlet UITableView *tableServices;

@property (weak, nonatomic) IBOutlet UILabel *titlePlans;

@property (weak, nonatomic) IBOutlet UILabel *labSectionPlans;

@property (weak, nonatomic) IBOutlet UILabel *labSectionPaymentMethod;

@property (weak, nonatomic) IBOutlet UILabel *labSectionAccountInfo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conPlanHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conServiceHeight;

@property (weak, nonatomic) IBOutlet UILabel *labName;

@property (weak, nonatomic) IBOutlet UILabel *labCurrentBalance;

@property (weak, nonatomic) IBOutlet UILabel *labLastTransaction;

@property (weak, nonatomic) IBOutlet UILabel *labPrice;

@property (weak, nonatomic) IBOutlet UILabel *labDate;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak, nonatomic) IBOutlet UIImageView *imgProvider;

@property (weak, nonatomic) IBOutlet UIButton *btnPay;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property BOOL isApplePay;

@property (weak, nonatomic) IBOutlet UILabel *labApplePay;
@property (weak, nonatomic) IBOutlet UIImageView *imgApplePay;

@property (weak, nonatomic) IBOutlet UILabel *labAccountBalance;
@property (weak, nonatomic) IBOutlet UIImageView *imgAccountBalance;


- (IBAction)actPay:(id)sender;
- (IBAction)actCancel:(id)sender;
- (IBAction)actBack:(id)sender;
- (IBAction)actApplePay:(id)sender;
- (IBAction)actAccountBalance:(id)sender;
-(void)refreshPaymentMethod;
-(void)updateClientInfo;

@end
