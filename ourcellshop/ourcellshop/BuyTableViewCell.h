//
//  BuyTableViewCell.h
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"
@class ViewController;
@interface BuyTableViewCell : UITableViewCell<UIScrollViewDelegate>
@property ViewController* viewController;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *titleBuy;
@property (weak, nonatomic) IBOutlet UITextView *descriptionBuy;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property NSMutableArray *widthImagesScroll;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
- (IBAction)actContact:(id)sender;
- (IBAction)actMap:(id)sender;
@property NSString* coordinates;
@property NSString* owner;
//Data From Picture
@property (nonatomic, strong) NSMutableArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;

//- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;
-(void)initDataCarouselWithArrayImages:(NSMutableArray*)imagesCarousel;
- (void)setImageToCenter:(UIImageView *)imageView;
-(int)getInitXToPos:(int)pos;

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthBlue;


@end
