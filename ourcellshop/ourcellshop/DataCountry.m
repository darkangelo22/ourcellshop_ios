//
//  DataCountry.m
//  
//
//  Created by René Abilio on 21/1/16.
//
//

#import "DataCountry.h"

@implementation DataCountry

-(NSComparisonResult)compare:(DataCountry*)country{
    return [self.descriptionCountry compare:country.descriptionCountry];
}
-(id)initWithData:(NSString*)idp  name:(NSString*)namep description:(NSString*) descriptionp isO2:(NSString*)isO2{
    
    if (!(self = [super init]))
        return nil;
    
    self.cid = idp;
    self.name = namep;
    self.descriptionCountry = descriptionp;
    self.isO2 = isO2;

    
    return self;
    
}

-(id)initWithCountry:(DataCountry*)dataCountry{
    
    if (!(self = [super init]))
        return nil;
    
    self.cid = dataCountry.cid;
    self.name = dataCountry.name;
    self.descriptionCountry = dataCountry.description;
    self.isO2 = dataCountry.isO2;

    return self;
    
}

@end
