//
//  DataPlan.h
//  
//
//  Created by René Abilio on 10/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataPlan : NSObject

@property NSNumber* idc;
@property NSString* name;
@property NSString* descriptionPlan;
@property NSString* price;
@property NSString* labPlan;
@property BOOL isSelectedPlan;
@property NSNumber* type;
-(id)initWithData:(NSString*)name descriptionp:(NSString*)description pricep:(NSString*)price labplanp:(NSString*)labplan isSelPlan: (BOOL)isPlan  typep:(NSNumber*)type idcp:(NSNumber*)idc;

@end
