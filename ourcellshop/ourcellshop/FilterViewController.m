//
//  FilterViewController.m
//  
//
//  Created by René Abilio on 2/1/16.
//
//

#import "FilterViewController.h"
#import "ViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Header
    [self.titleFilters setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.labCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labResetAll setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //MiniHeader
    [self.labDistance setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labPrice setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labCategories setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labPublished setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labLocation setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    
    //Categories
    [self.labElectronic setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labSoftware setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labUnlock setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labRepair setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //Price
    [self.labMin setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labMax setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labZipCode setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    //Published
    [self.lab24Hours setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labWeek setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labAllProducts setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    self.btnSave.layer.cornerRadius = 5;
    
    
    self.isElectronic = YES;
    self.isSoftware = NO;
    self.isRepair = NO;
    self.isUnlock = NO;
    self.timeFromSearch = 1;

    [self refreshCategories];
    [self refreshTimePublished];
    
    
    //Scroll
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
        //IS iPhone 4 Heigth Scroll reduce
        self.conHeigthScroll.constant = 400;
    }
    
    self.scrollView.scrollEnabled=YES;
    //CGFloat height = DEVICE_HEIGTH;
    [self.scrollView setContentSize:CGSizeMake(DEVICE_WIDTH, 100)];
    
    self.txtMax.delegate = self;
    self.txtMin.delegate = self;
    
    
    
    //Input Number Pad--------------
    
//    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
////    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
//    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
//                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
//    [numberToolbar sizeToFit];
//    self.txtMax.inputAccessoryView = numberToolbar;
//    self.txtMin.inputAccessoryView = numberToolbar;
    
    //------------------------------
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actResetAll:(id)sender {
    //Clean All Filters
    self.timeFromSearch = 1;
    self.isElectronic = NO;
    self.isSoftware = NO;
    self.isUnlock = NO;
    self.isRepair = NO;
    [self refreshCategories];
    [self refreshTimePublished];
}

- (IBAction)actCancel:(id)sender {
    //Dismiss
    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)actSaveAll:(id)sender {
    //Salvar al NSUserDefault
    int selectedCategory = 1;
    double min = [_txtMin.text doubleValue];
    double max = [_txtMax.text doubleValue];
    int zipcode = -1;
    if (![_txtZipCode.text compare:@""]==NSOrderedSame)
    {
        zipcode = [_txtZipCode.text integerValue];
    }
    if (_isUnlock) selectedCategory=1;
    if (_isRepair) selectedCategory=2;
    if (_isSoftware) selectedCategory=4;
    if (_isElectronic) selectedCategory=5;
    
    FilterServices* filterdata = [[FilterServices alloc] initWithZipCode:zipcode Distance:_HorizontalSlider.value Category:selectedCategory MinPrice:min MaxPrice:max Published:self.timeFromSearch];
    self.viewController.FilterProductData = filterdata;
    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actElectronic:(id)sender {
    self.isElectronic = YES;
    self.isRepair = NO;
    self.isSoftware = NO;
    self.isUnlock = NO;
    [self refreshCategories];
}

- (IBAction)actUnlock:(id)sender {
    self.isElectronic = NO;
    self.isRepair = NO;
    self.isSoftware = NO;
    self.isUnlock = YES;
    [self refreshCategories];
}

- (IBAction)actSoftware:(id)sender {
    self.isElectronic = NO;
    self.isRepair = NO;
    self.isSoftware = YES;
    self.isUnlock = NO;
    [self refreshCategories];
}

- (IBAction)actRepair:(id)sender {
    self.isElectronic = NO;
    self.isRepair = YES;
    self.isSoftware = NO;
    self.isUnlock = NO;
    [self refreshCategories];
}

- (IBAction)act24Hours:(id)sender {
    self.timeFromSearch = 1;
    [self refreshTimePublished];
}

- (IBAction)actThisWeek:(id)sender {
    self.timeFromSearch = 2;
    [self refreshTimePublished];
}

- (IBAction)actAllProducts:(id)sender {
    self.timeFromSearch = 3;
    [self refreshTimePublished];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)refreshCategories{
    
    UIColor *blueColor = [UIColor colorWithRed:44.0/255.0 green:79.0/255.0 blue:149.0/255.0 alpha:1.0];
    
    UIColor *darkColor = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0];
    
    //Electronic
    if (self.isElectronic) {
        self.imgElectronic.image = [UIImage imageNamed:@"electronic-blue.png"];
        [self.labElectronic setTextColor:blueColor];
        
    }else{
        self.imgElectronic.image = [UIImage imageNamed:@"electronic.png"];
        [self.labElectronic setTextColor:darkColor];
    }
    
    //Software
    if (self.isSoftware) {
        self.imgSoftware.image = [UIImage imageNamed:@"software-blue.png"];
        [self.labSoftware setTextColor:blueColor];
        
    }else{
        self.imgSoftware.image = [UIImage imageNamed:@"software.png"];
        [self.labSoftware setTextColor:darkColor];
    }
    
    //Unlock
    if (self.isUnlock) {
        self.imgUnlock.image = [UIImage imageNamed:@"unlock-blue.png"];
        [self.labUnlock setTextColor:blueColor];
        
    }else{
        self.imgUnlock.image = [UIImage imageNamed:@"unlock.png"];
        [self.labUnlock setTextColor:darkColor];
    }
    
    //Repair
    if (self.isRepair) {
        self.imgRepair.image = [UIImage imageNamed:@"repair-blue.png"];
        [self.labRepair setTextColor:blueColor];
        
    }else{
        self.imgRepair.image = [UIImage imageNamed:@"repair.png"];
        [self.labRepair setTextColor:darkColor];
    }
}

-(void)refreshTimePublished{
    
    //Clean all Img with Check White
    self.img24Hours.image = [UIImage imageNamed:@"check-white.png"];
    self.imgThisWeek.image = [UIImage imageNamed:@"check-white.png"];
    self.imgAllProd.image = [UIImage imageNamed:@"check-white.png"];
    
    //Paint Blue Check
    switch (self.timeFromSearch) {
        case 1:
            self.img24Hours.image = [UIImage imageNamed:@"check.png"];
            break;
        case 2:
            self.imgThisWeek.image = [UIImage imageNamed:@"check.png"];
            break;
        case 3:
            self.imgAllProd.image = [UIImage imageNamed:@"check.png"];
            break;
    }
    
}


#pragma mark TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [self.scroll setContentOffset:CGPointMake(0, 45) animated:true];
    
    
    [textField setReturnKeyType:UIReturnKeyDone];
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelNumberPad{
    [self.txtMax resignFirstResponder];
    //numberTextField.text = @"";
}

-(void)doneWithNumberPad{
   // NSString *numberFromTheKeyboard = numberTextField.text;
    [self.txtMax resignFirstResponder];
}


@end
