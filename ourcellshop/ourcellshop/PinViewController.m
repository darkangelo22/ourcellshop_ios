//
//  PinViewController.m
//  t2parking
//
//  Created by René Abilio on 9/4/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "PinViewController.h"
#import "ViewController.h"
#define SCROLL_HEIGHT [self getScrollHeigth]
#define SCROLL_HEIGHT_ORIGINAL 262

@interface PinViewController ()

@end

@implementation PinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnValidar.layer.cornerRadius = 2;
    self.btnResend.layer.cornerRadius = 2;
    
    
    //Ajuste iPhone 6
    if(IS_IPHONE_6){
        self.conLogoTop.constant = 70;
        
        
    }
    //Ajuste iPhone 5
    if(IS_IPHONE_5){
        self.conLogoTop.constant = 60;
        
        
    }
    
    //Ajuste iPhone 6+
    if(IS_IPHONE_6_PLUS){
        self.conLogoTop.constant = 75;
    }
    
    [self.labNear setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    [self.lab2Me setFont:[UIFont fontWithName:@"Roboto-Regular" size:17.0]];
    [self.labDescription setFont:[UIFont fontWithName:@"Roboto-Regular" size:18.0]];
    
    
    [self.butLogin setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.butSearchAround setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    [self.butSign setFont:[UIFont fontWithName:@"Roboto-Bold" size:11.0]];
    self.butLogin.layer.cornerRadius = 5;
    self.butSearchAround.layer.cornerRadius = 5;
    self.butSign.layer.cornerRadius = 5;
    
    [self.labDonot setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    [self.labOrJust setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    [self.labAlready setFont:[UIFont fontWithName:@"Roboto-Regular" size:10.0]];
    [self.btnSignIn setFont:[UIFont fontWithName:@"Roboto-Regular" size:10.0]];
    [self.btnSignUp setFont:[UIFont fontWithName:@"Roboto-Regular" size:10.0]];
    [self.btnCancel setFont:[UIFont fontWithName:@"Roboto-Bold" size:10.0]];
    
}


- (IBAction)actCancel:(id)sender {
    
    [self.viewController goCancelLogin];
    
    [self dismissViewControllerAnimated:YES completion:^{}];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [_labError setHidden:YES];
    [_imgIndicator setHidden:YES];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actResendValidateNumber:(id)sender {
}

- (IBAction)actValidar:(id)sender {
}
- (IBAction)hideKeyScrollInside:(id)sender {
    
    
}

- (IBAction)hideKeyboard:(id)sender {
    
}

- (id)loadController:(Class)classType {
    NSString *className = NSStringFromClass(classType);
    UIViewController *controller = [[classType alloc] initWithNibName:className bundle:nil];
    return controller;
}

- (IBAction)actLoginUp:(id)sender {
    LoginUpViewController *LoginUpVC = [self loadController:[LoginUpViewController class]];
    
    [self presentViewController:LoginUpVC animated:YES completion:NULL];
}

-(int)getScrollHeigth{
    if(IS_IPHONE_5){
        return 230;
    }
    
    if(IS_IPHONE_6_PLUS || IS_IPHONE_6){
        return 320;
    }
    
    return 150;
}

- (IBAction)actLook:(id)sender {
    
    [self moveToMain];
}

- (IBAction)actSign:(id)sender {
    
}

- (IBAction)actButHide:(id)sender {
    
}

-(void)moveToMain{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    ViewController *viewControllerPrincipal = [sb instantiateViewControllerWithIdentifier:@"initialViewController"];
    
    
    [self presentViewController:viewControllerPrincipal animated:YES completion:NULL];
}

- (IBAction)actLogIn:(id)sender {
    LoginViewController *LoginVC = [self loadController:[LoginViewController class]];
    LoginVC.viewController = self.viewController;
    LoginVC.gate = _gate;
    [self presentViewController:LoginVC animated:YES completion:NULL];
}


@end
