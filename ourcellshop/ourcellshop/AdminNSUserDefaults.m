//
//  AdminNSUserDefaults.m
//  t2parking
//
//  Created by René Abilio on 9/7/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "AdminNSUserDefaults.h"

@implementation AdminNSUserDefaults

//+(void)saveMatriculas:(NSMutableArray*)matriculas{
//    
//    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:matriculas];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:encodedObject forKey:@"matriculas"];
//    [defaults synchronize];
//
//}
//
//+(NSMutableArray*)getMatriculas{
//    
//    NSMutableArray *matriAuxs;
//    
//    @try {
//        NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
//        NSData *encodedObject1 = [defaults1 objectForKey:@"matriculas"];
//        matriAuxs = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject1];
//    }
//    @catch (NSException *exception) {
//        matriAuxs = [[NSMutableArray alloc]init];
//    }
//    
//    return matriAuxs;
//}
//
//+(void)savePrincipalMatricula:(NSString*)matricula{
//    
//    [[NSUserDefaults standardUserDefaults] setObject:matricula forKey:@"matriculaPrincipal"];
//    
//}
//
//
//+(NSString*)getPrincipalMatricula{
//    
//    return [[NSUserDefaults standardUserDefaults] objectForKey:@"matriculaPrincipal"];
//
//}
//
//+(void)savePhoneNumber:(NSString*)phone{
//    
//    [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"phoneNumber"];
//
//}
//
//+(NSString*)getPhoneNumber{
//
//    return [[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
//
//}
//
//+(void)savePhoneNumberValidated{
//    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"isPhoneValidated"];
//}
//
//+(BOOL)getPhoneNumberValidated{
//    
//    NSString* isPhoneValid = [[NSUserDefaults standardUserDefaults] objectForKey:@"isPhoneValidated"];
//    BOOL idValid = [isPhoneValid isEqualToString:@"1"];
//    return idValid;
//}


+(BOOL)stringIsNilOrEmpty:(NSString*)aString {
    if (!aString)
        return YES;
    return [aString isEqualToString:@""];
}


+(void)saveUserLogued:(UserLogued*)user{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    [prefs setObject:myEncodedObject forKey:@"user"];
}

+(UserLogued*)getUserLogued{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [prefs objectForKey:@"user"];
    UserLogued *userLog = (UserLogued *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    return userLog;
}

+(BOOL)isUserLogued{
    UserLogued* userLog = [AdminNSUserDefaults getUserLogued];
    if(userLog == nil)
        return NO;
    
    return ![self stringIsNilOrEmpty:userLog.username];
}




@end
