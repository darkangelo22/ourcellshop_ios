//
//  Util.m
//  t2parking
//
//  Created by René Abilio on 9/1/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)

#define DEVICE_WIDTH self.view.bounds.size.width

#define DEVICE_HEIGTH self.view.bounds.size.height

#define DEVICE_WIDTH_TOUR [UIScreen mainScreen].bounds.size.width

#define PORTRAIT_KEYBOARD_HEIGHT 216


#define PORTRAIT_KEYBOARD_NUMBER_HEIGHT 150

