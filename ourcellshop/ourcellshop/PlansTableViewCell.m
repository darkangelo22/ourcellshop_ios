//
//  PlansTableViewCell.m
//  
//
//  Created by René Abilio on 10/1/16.
//
//

#import "PlansTableViewCell.h"

@implementation PlansTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self updateViewPlanData];
}

-(void)updateViewPlanData{
    
    if(self.plan != nil){
        
        if ([self.plan.price isKindOfClass:[NSString class]] && [self.plan.price compare:@"-1"]==NSOrderedSame) {
           self.labPlan.text = [[NSString alloc] initWithFormat:@"%@", self.plan.descriptionPlan,self.plan.price];
        }
        else
        self.labPlan.text = [[NSString alloc] initWithFormat:@"%@ / $%@", self.plan.descriptionPlan,self.plan.price];
        
        if(self.plan.isSelectedPlan){
            
            UIColor *blueColor = [UIColor colorWithRed:44.0/255.0 green:79.0/255.0 blue:149.0/255.0 alpha:1.0];
            
            self.imgCheck.image = [UIImage imageNamed:@"check.png"];
            
            [self.labPlan setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
            [self.labPlan setTextColor:blueColor];
            
        }else{
            UIColor *darkColor = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0];
            
            self.imgCheck.image = [UIImage imageNamed:@"check-white.png"];
            
            [self.labPlan setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
            [self.labPlan setTextColor:darkColor];
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
