//
//  MapViewController.h
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@class ViewController;

@interface MapViewController : UIViewController<UITextFieldDelegate>
@property bool showData;
@property NSMutableArray* data;
@property(assign) ViewController* viewController;
@property (weak, nonatomic) IBOutlet UIButton *numberAlert;
@property (weak, nonatomic) IBOutlet UILabel *labBack;
@property (weak, nonatomic) IBOutlet UILabel *titleAds;
@property (weak, nonatomic) IBOutlet UITextField *txtFilterSearch;
-(void)loadData;
- (IBAction)actBack:(id)sender;
- (IBAction)actShowFilters:(id)sender;
- (IBAction)goBuy:(id)sender;
- (void)CenterWithLat:(double)lat lon:(double)lon title:(NSString*)title;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property  CLLocationCoordinate2D Center;
//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conSpaceRigth;

@end
