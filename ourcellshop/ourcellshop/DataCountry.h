//
//  DataCountry.h
//  
//
//  Created by René Abilio on 21/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataCountry : NSObject
@property NSString* cid;
@property NSString* name;
@property NSString* descriptionCountry;
@property NSString* isO2;
-(NSComparisonResult)compare:(DataCountry*)country;

-(id)initWithData:(NSString*)idp name:(NSString*)namep description:(NSString*) descriptionp isO2:(NSString*)isO2;

-(id)initWithCountry:(DataCountry*)dataCountry;

@end
