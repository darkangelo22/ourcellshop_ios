//
//  SellTableViewCell.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <UIKit/UIKit.h>
#import "Util.m"

@interface SellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleSell;
@property (weak, nonatomic) IBOutlet UILabel *dateSell;
@property (weak, nonatomic) IBOutlet UILabel *descriptionSell;
@property (weak, nonatomic) IBOutlet UIImageView *imageSell;
@property (weak, nonatomic) IBOutlet UILabel *priceSell;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

- (void)setImageToCenter:(UIImageView *)imageView;


//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conRigthBlue;


@end
