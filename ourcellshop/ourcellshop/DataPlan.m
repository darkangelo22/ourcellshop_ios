//
//  DataPlan.m
//  
//
//  Created by René Abilio on 10/1/16.
//
//

#import "DataPlan.h"

@implementation DataPlan

-(id)initWithData:(NSString*)name descriptionp:(NSString*)desc pricep:(NSString*)price labplanp:(NSString*)labplan isSelPlan: (BOOL)isPlan   typep:(NSNumber*)type  idcp:(NSNumber*)idc{
    if (!(self = [super init]))
        return nil;
    self.name = name;
    self.descriptionPlan=desc;
    self.price=price;
    self.labPlan = labplan;
    self.isSelectedPlan = isPlan;
    self.type = type;
    self.idc = idc;
    return self;

}

@end
