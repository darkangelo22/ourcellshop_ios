//
//  SellViewController.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <UIKit/UIKit.h>
#import "DataSell.h"
#import "SellTableViewCell.h"

@class ViewController;

@interface SellViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *overlayView;

@property (weak, nonatomic) IBOutlet UITableView *tableSell;
@property (strong, nonatomic) NSMutableArray *sells;
@property (weak, nonatomic) IBOutlet UILabel *labNoObj;
@property (weak, nonatomic) IBOutlet UIView *viewNoObj;

@property(assign) ViewController* viewController;

@property (weak, nonatomic) IBOutlet UITextField *txtFilterSearch;
-(void)onLoad;
- (IBAction)actShowFilter:(id)sender;
- (IBAction)actNewAd:(id)sender;


@end
