//
//  MessageSendViewController.m
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import "MessageSendViewController.h"
#import "ViewController.h"
#import "MessageData.h" 
#import "MessageSendTableViewCell.h"

@interface MessageSendViewController ()

@end

@implementation MessageSendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.labInbox setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    [self.labSend setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    [self.labSearch setFont:[UIFont fontWithName:@"Roboto-Bold" size:12.0]];
    

        [self.tableSendMessage setDataSource:self];
    [self.tableSendMessage setDelegate:self];
    

}
-(void)onLoad{
    [self.overlayView removeFromSuperview];
    self.view.userInteractionEnabled = YES;

    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    //INICIALIZANDO SERVICIO DE PAISES
    NSString *urlMsgService = GET_MESSAGES;
    NSURL *urlMsg = [NSURL URLWithString:urlMsgService];
    
    self.messages = [[NSMutableArray alloc]init];
    
    NSURLRequest *requestBuy = [NSURLRequest requestWithURL:urlMsg
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                            timeoutInterval:300.0];
    dispatch_queue_t myQueue = dispatch_queue_create("my queue1", NULL);
    dispatch_async(myQueue, ^{
        
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
        [NSURLConnection sendAsynchronousRequest:requestBuy
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             [self.overlayView removeFromSuperview];
             self.view.userInteractionEnabled = YES;
             
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:NULL];
                 NSString *urlproviderphoto = GET_PROVIDERS_PHOTO;
                 //LLENANDO LISTA DE PAISES
                 for (id key in greet) {
                     //                 [self.countries setObject: [key objectForKey:@"description"] forKey:[key objectForKey:@"id"]];
                     
                     NSString* user = [key objectForKey:@"from"];
                     UserLogued* userLogged = [AdminNSUserDefaults getUserLogued];

                     if ([user compare:userLogged.username]==NSOrderedSame)
                     {
                         NSString *ide =[key objectForKey:@"id"];
                         NSString *from = [key objectForKey:@"from"];
                           NSString *to = [key objectForKey:@"tousername"];
                         NSString *subject =[key objectForKey:@"subject"];
                         NSNumber *body=[key objectForKey:@"body"];
                         
                         NSString *date = [key objectForKey:@"date"];
                         NSArray *dateElements = [date componentsSeparatedByString:@"-"];
                         NSString *year = [dateElements objectAtIndex:0];
                         NSString *month = [dateElements objectAtIndex:1];
                         NSString *day = [dateElements objectAtIndex:2];
                         
                         NSString *dayJoin = [NSString stringWithFormat:@"%c%c",[day characterAtIndex:0], [day characterAtIndex:1]];
                         month = [self getMonth:month];
                         NSString *finalDateFormat = [NSString stringWithFormat:@"%@ %@",month,dayJoin];
                         
                         MessageData *msg1 =[[MessageData alloc]initWithData:from to:to subject:subject body:body date:finalDateFormat imgUrl:@"http://localhost/"];
                         
                         
                         [self.messages addObject:msg1];
                     }
                     
                     
                     
                 }
                 //Refreshing table
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.tableSendMessage reloadData];
                 });
             }
         }];
    });
    
}
-(NSString*) getMonth:(NSString *) mon
{
    NSString* result;

    if ([mon compare:@"01" ]== NSOrderedSame){
       result= @"Jan";
    }
    if ([mon compare:@"02" ]== NSOrderedSame){
        result= @"Feb";
    }
    if ([mon compare:@"03" ]== NSOrderedSame){
        result= @"Mar";
    }
    if ([mon compare:@"04" ]== NSOrderedSame){
        result= @"Apr";
    }
    if ([mon compare:@"05" ]== NSOrderedSame){
        result= @"May";
    }
    if ([mon compare:@"06" ]== NSOrderedSame){
        result= @"Jun";
    }
    if ([mon compare:@"07" ]== NSOrderedSame){
        result= @"Jul";
    }
    if ([mon compare:@"08" ]== NSOrderedSame){
        result= @"Aug";
    }
    if ([mon compare:@"09" ]== NSOrderedSame){
        result= @"Sep";
    }
    if ([mon compare:@"10" ]== NSOrderedSame){
        result= @"Oct";
    }
    if ([mon compare:@"11" ]== NSOrderedSame){
        result= @"Nov";
    }
    if ([mon compare:@"12" ]== NSOrderedSame){
        result= @"Dec";
    }
    return  result;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actInbox:(id)sender {
    [self.viewController goMsg];
}

- (IBAction)actSend:(id)sender {
}

- (IBAction)actSearch:(id)sender {
}

-(void)cleanColorsMsg{
    
    self.imgInbox.image = [UIImage imageNamed:@"inbox.png"];
    self.imgSend.image = [UIImage imageNamed:@"send.png"];
    self.imgSearch.image = [UIImage imageNamed:@"searchmsg.png"];
    
    UIColor *myColor = [UIColor colorWithRed:154.0/255.0 green:154.0/255.0 blue:154.0/255.0 alpha:1.0];
    
    //Set Gray Color
    [self.labInbox setTextColor:myColor];
    [self.labSend setTextColor:myColor];
    [self.labSearch setTextColor:myColor];
    
}

-(void)colorToInbox{
    self.imgInbox.image = [UIImage imageNamed:@"inbox-white.png"];
    
    [self.labInbox setTextColor:[UIColor whiteColor]];
}

-(void)colorToSend{
    self.imgSend.image = [UIImage imageNamed:@"send-white.png"];
    
    [self.labSend setTextColor:[UIColor whiteColor]];
}

-(void)colorToSearch{
    self.imgSearch.image = [UIImage imageNamed:@"searchmsg-white.png"];
    
    [self.labSearch setTextColor:[UIColor whiteColor]];
}
#pragma mark Tabla Messages
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"MessageTableViewCell";
    
    MessageSendTableViewCell *cell = (MessageSendTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    //    if (cell == nil)
    //    {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MessageSendTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    //    }
    
    MessageData* auxPlate = [_messages objectAtIndex:indexPath.row];
    
    //Data Fill
    
    cell.To.text = auxPlate.from;
  
    cell.Subject.text = auxPlate.subject == (NSString *)[NSNull null] ?@"":auxPlate.subject ;
    cell.body.text = auxPlate.body == (NSString *)[NSNull null] ?@"":auxPlate.body ;
    cell.date.text = auxPlate.date;
    
    cell.btnSelectMsg.tag = indexPath.row;
    [cell.btnSelectMsg addTarget:self action:@selector(selectMsgClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}

-(void)selectMsgClicked:(UIButton*)sender
{
    MessageData* msg = [_messages objectAtIndex:sender.tag];
    [self.viewController goMsgRead:msg];
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    MessageData *plateEditAux = [self.messages objectAtIndex:index];
    
}

@end
