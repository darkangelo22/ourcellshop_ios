//
//  NewAddViewController.m
//
//
//  Created by René Abilio on 6/1/16.
//
//

#import "NewAddViewController.h"
#import "ViewController.h"

@interface NewAddViewController ()

@end

@implementation NewAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.conHeigthScroll.constant = 410;
    self.conHeigthViewInner.constant = 600;
    
    self.scrollView.scrollEnabled=YES;
    [self.scrollView setContentSize:CGSizeMake(DEVICE_WIDTH, 600)];

    [self.txtDescription setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    _txtDescription.text = @"Description";
    _txtDescription.textColor = [UIColor lightGrayColor];
    _txtDescription.delegate = self;
    //Scroll
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
        //IS iPhone 4 Heigth Scroll reduce
        self.conMapHeight.constant =  self.conMapHeight.constant - 15;
    }
    else{
        self.conMapHeight.constant =  self.conMapHeight.constant +30;
        
    }
    
    _isComboShow = NO;
    [self.picker setDelegate:self];
    [self.picker setDataSource:self];
    self.picker.showsSelectionIndicator=YES;
    [self.btnPickerGuardar setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.btnPickerCancelar setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    self.categories=[[NSMutableArray alloc] init];
    [self.txtDescription setDelegate:self];
    [self.txtTitle setDelegate:self];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    
    tapRecognizer.numberOfTapsRequired = 1;
    
    tapRecognizer.numberOfTouchesRequired = 1;


    [self.mapView addGestureRecognizer:tapRecognizer];
    
    
}
-(IBAction)foundTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint point = [recognizer locationInView:self.mapView];
    
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
       MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    
    point1.coordinate = tapPoint;
    [self.mapView removeAnnotations:[self.mapView annotations]];
    _lat = tapPoint.latitude;
    _lon = tapPoint.longitude;
    point1.title=@"Service Position";
    [self.mapView addAnnotation:point1];
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([_txtDescription.text compare: @"Description"]==NSOrderedSame)
    {
        _txtDescription.text = @"";
        _txtDescription.textColor = [UIColor blackColor];
    }
    return YES;
}
- (BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    if ([_txtDescription.text compare: @""]==NSOrderedSame)
    {
        _txtDescription.text = @"Description";
        _txtDescription.textColor = [UIColor lightGrayColor];
    }
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(_txtDescription.text.length == 0){
        _txtDescription.textColor = [UIColor lightGrayColor];
        _txtDescription.text = @"Description";
        [_txtDescription resignFirstResponder];
    }
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textViewDidEndEditing:(UITextView *)textField{
    [textField resignFirstResponder];
}
-(bool)textViewShouldReturn:(UITextView *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actPublich:(id)sender {
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@service/register",urlBase];
    NSString* title = _txtTitle.text;
    NSString* description=_txtDescription.text;
    NSString* coordinates = [[NSString alloc] initWithFormat:@"%f;%f",_lat,_lon];
     NSString* serviceTypeId= @"1";
    if ([_txtCategory.text compare:@"Unlock"]==NSOrderedSame) {
        serviceTypeId = @"1";
    }
    if ([_txtCategory.text compare:@"Repair"]==NSOrderedSame) {
        serviceTypeId = @"2";
    }
    if ([_txtCategory.text compare:@"Activation"]==NSOrderedSame) {
        serviceTypeId = @"3";
    }
       NSString *post = [NSString stringWithFormat:@"title=%@&description=%@&coordinates=%@&serviceTypeId=%@&ContactByPhone=%@&Price=%@&EmailDisplayId=%@&applicationUserId=%@&PublishedDate=%@",title,description,coordinates,serviceTypeId,@"false",_txtPrice.text,@"1",@"0e4879f5-e286-4356-9eaf-cbccce70caf8",@"02-06-2016"];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&err];
    if (data.length > 0 && err == nil)
    {
        NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                              options:0
                                                                error:NULL];
        
    }
    
    
    
    
    [self.viewController goService];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)actCancel:(id)sender {
    
    [self.viewController goService];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)actDone:(id)sender {
    [self hideInmediately];
    [self.txtCategory setText:_catSel.descriptionPlan];
  
}

- (IBAction)actCancelBox:(id)sender {
       [self hideInmediately];
}





-(void) hideShowPicker{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if(!_isComboShow){
            
            int y = DEVICE_HEIGTH - _pickerView.frame.size.height;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
            
        }else{
            
            int y = DEVICE_HEIGTH + 10;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
        }
        
        _isComboShow = !_isComboShow;
        
    }];
}

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_categories count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50.0f;
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
//
//    Plate *platAux = [_matriculas objectAtIndex:row];
//
//    NSString *formatMatric = [[NSString alloc]initWithFormat:@"%@ - %@",platAux.descriptionCar, platAux.number];
//    return formatMatric;
//
//}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:@"Roboto-Regular" size:25]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
    }
    
    // Fill the label text here
    DataPlan *platAux = [_categories objectAtIndex:row];
    NSString *formatMatric = [[NSString alloc]initWithFormat:@"%@",platAux.descriptionPlan];
    
    
    tView.text=formatMatric;
    return tView;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    _catSel = [_categories objectAtIndex:row];
    
}

- (void) hideInmediately{
    
    int y = DEVICE_HEIGTH + 10;
    [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
    
    _isComboShow = NO;
}
- (IBAction)actCategory:(id)sender {
    self.categories = [[NSMutableArray alloc]init];
    DataPlan* pl = [[DataPlan alloc] initWithData:@"Unlock" descriptionp:@"Unlock"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"1"];
    [self.categories addObject:pl];
    DataPlan* pl1 = [[DataPlan alloc] initWithData:@"Repair" descriptionp:@"Repair"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"2"];
    [self.categories addObject:pl1];

    DataPlan* pl2 = [[DataPlan alloc] initWithData:@"Activation" descriptionp:@"Activation"  pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:@"3"];
    [self.categories addObject:pl2];
    [self hideShowPicker];
    [self.picker reloadAllComponents];
}
@end
