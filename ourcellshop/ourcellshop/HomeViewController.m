//
//  HomeViewController.m
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import "HomeViewController.h"
#import "ViewController.h"
#import "AdminNSUserDefaults.h"

@interface HomeViewController ()

@property NSTimer *timerRemaining;

@property BOOL isSearchingTickets;

@property (nonatomic, strong) NSArray *cellSizes;
@property (nonatomic, strong) NSArray *cats;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _categories = [[NSMutableArray alloc] init];
    self.widthCollectionView.constant = 360;
    [self.titleHome setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    self.numberAlert.layer.cornerRadius = 8;
    
    [self.labFindUs setFont:[UIFont fontWithName:@"Roboto-Bold" size:13.0]];
    [self.labWelcome setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    
    [self.labCountry setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.txtSearchProvider setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.labDataCountry setFont:[UIFont fontWithName:@"Roboto-Bold" size:14.0]];
    [self.labProviders setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    
    //INHABILITANDO CLICK EN PANTALLA CON BOTON GIGANTE
    self.view.userInteractionEnabled = NO;
  //  self.viewController.view.userInteractionEnabled = NO;
    
    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);

    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
        [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    if (IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_6_PLUS) {
        self.conHeigthDescription.constant = 190;
    }
    else{
        _conHeightCombo.constant =  _conHeightCombo.constant + 50;
    }
    //INICIALIZANDO SERVICIO DE PROVEDORES
    NSString *urlProviderService = GET_PROVIDERS;
    NSURL *urlprovider = [NSURL URLWithString:urlProviderService];
    self.providers = [[NSMutableArray alloc]init];
    
    self.allproviders = [[NSMutableArray alloc]init];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlprovider
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:300.0];
    //INICIALIZANDO SERVICIO DE PAISES
     NSString *urlCountryService = GET_COUNTRIES;
    NSURL *urlcountry = [NSURL URLWithString:urlCountryService];
    self.countries = [[NSMutableDictionary alloc]init];
    self.allcountries = [[NSMutableArray alloc]init];
    NSURLRequest *requestCountry = [NSURLRequest requestWithURL:urlcountry
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                timeoutInterval:300.0];
    dispatch_queue_t myQueue = dispatch_queue_create("my queue3", NULL);
    dispatch_async(myQueue, ^{
        

    //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
    [NSURLConnection sendAsynchronousRequest:requestCountry
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:NULL];
             //LLENANDO LISTA DE PAISES
               for (id key in greet) {
                   DataCountry* ctr = [[DataCountry alloc] initWithData:[key objectForKey:@"id"] name:[key objectForKey:@"isO3"] description:[key objectForKey:@"countryName"] isO2:[key objectForKey:@"isO2"] ];
                   [_allcountries addObject:ctr];
                   
               }
             
             //Ordenando el Array de Paises
             _allcountries =[NSMutableArray arrayWithArray: [_allcountries sortedArrayUsingSelector:@selector(compare:)]];
             
             //Armando el Diccionario con el Array Ordenado
             _countries = [[NSMutableArray alloc] init];
             for (DataCountry* key in _allcountries) {
                 NSMutableArray * theMutableArray = [self GetCountryValuesByKey:[key.descriptionCountry substringToIndex:1]];
                 if ( theMutableArray == nil ) {
                     theMutableArray = [NSMutableArray array];
                     [theMutableArray addObject:key];
                     keyValue* tmp = [[keyValue alloc] initWithKey:[key.descriptionCountry substringToIndex:1] value:theMutableArray];
                     [self addCountry:tmp];
                 }
                 else
                 {
                     [theMutableArray addObject:key];
                     keyValue* tmp = [[keyValue alloc] initWithKey:[key.descriptionCountry substringToIndex:1] value:theMutableArray];
                     [self addCountry:tmp];
                 }
             }
             
             
         }
     
     
         //OBTENIENDO RESPUESTA DEL SERVICIO DE PROVEDORES
         [NSURLConnection sendAsynchronousRequest:request
                                            queue:[NSOperationQueue mainQueue]
                                completionHandler:^(NSURLResponse *response,
                                                    NSData *dataP, NSError *connectionError)
          {
              [self.overlayView removeFromSuperview];
              self.view.userInteractionEnabled = YES;
            //  self.viewController.view.userInteractionEnabled = YES;
              if (dataP.length > 0 && connectionError == nil)
              {
                  NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:dataP
                                                                           options:0
                                                                             error:NULL];
                  NSString *urlproviderphoto = GET_PROVIDERS_PHOTO;
                  //LLENANDO LISTA DE PROVEEDORES
                  for (id key in greeting) {
                      NSString *countryid = [key objectForKey:@"countryid"];
                     
                      NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat: @"%@%@",urlproviderphoto,[key objectForKey:@"imageurl"]]];
                      NSData *dataimg = [NSData dataWithContentsOfURL:url];
                      NSMutableArray* plans = [[NSMutableArray alloc] init];
                      if (plans.count > 0)
                      [[plans objectAtIndex:0] setIsSelectedPlan:YES];
                      DataProvider *dataProv1 = [[DataProvider alloc]initWithData:[key objectForKey:@"id"]  provName: [key objectForKey:@"name"] imageDataP:dataimg imageNameP:[[NSString alloc] initWithFormat: @"%@%@",urlproviderphoto,[key objectForKey:@"imageurl"]] descriptionProviderP:[key objectForKey:@"description"]  countryProvider:[self GetCountryById:countryid]  plansp:plans];
                      if ([dataProv1.countryProvider compare:@"USA"] == NSOrderedSame){
                          [self.providers addObject:dataProv1];
                      }
                      [self.allproviders addObject:dataProv1];
                      
                  }
                  
                  //Refreshing table
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [self.tableProviders reloadData];
                  });
                  
              }else if (connectionError != nil){
                  //Poner PIN INCORRECTO con cartel Error de conexion
                  //                 _labError.text = @"Error de conexión";
                  //                 [_labError setHidden:NO];
              }
          }];
         
     }];
    });
    
   
    [self.tableProviders setDataSource:self];
    [self.tableProviders setDelegate:self];
    
    [self.txtSearchProvider setDelegate:self];
    [self.txtCountry setDelegate:self];
    [self.tableCountries setDelegate:self];
    [self.tableCountries setDataSource:self];
    _isComboShow = NO;
    [self.picker setDelegate:self];
    [self.picker setDataSource:self];
    self.picker.showsSelectionIndicator=YES;
    [self.btnGuardarPicker setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.btnCancelarPicker setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];

}


-(void)addCountry:(keyValue*)cgroup
{
    int pos = 0;
    bool es = false;
    int c = 0;
    for(keyValue* val in _countries)
    {
        if ([val.key  compare:cgroup.key]==NSOrderedSame)
        {
            pos = c;
            es = YES;
            break;
        }
        c++;
    }
    if (es)
    {
        keyValue* a=[_countries objectAtIndex:pos];
        a.value = cgroup.value;
    }
    else
    {
        [_countries addObject:cgroup];
    }
    
}
-(NSString*)GetCountryById:(NSString*)cid{
    for(DataCountry* ctr in _allcountries)
    {
        if (ctr.cid.longLongValue  == cid.longLongValue)
            return ctr.name;
    }
    return @"";
}
-(NSMutableArray*)GetCountryValuesByKey:(NSString*)key{
    for(keyValue* val in _countries)
    {
       if ([val.key compare:key] == NSOrderedSame)
           return val.value;
    }
    return [[NSMutableArray alloc] init];
}
-(void)viewWillAppear:(BOOL)animated{
    
}


-(void)filterProviders:(NSString*)filtro{
    //CALL SERVICE WEB WITH FILTER
    NSString *filter = [filtro lowercaseString];
    self.providers = [[NSMutableArray alloc]init];
    NSArray *dataElements = [filter componentsSeparatedByString:@"_"];
    
    for (int i=0; i<[self.allproviders count]; i++) {
        DataProvider *prov = [self.allproviders objectAtIndex:i];
        NSString *nameProviderAux = [prov.providerName lowercaseString];
        NSString *countryProviderAux = [prov.countryProvider lowercaseString];
        
        bool es = true;
        if ([dataElements count]>1)
        {
            
            if ([nameProviderAux rangeOfString:dataElements[1]].location == NSNotFound ){
                es = false;
            }
            
            if ([countryProviderAux rangeOfString:dataElements[0]].location == NSNotFound) {
                es = false;
            }

        }
        else
        {
            if ([countryProviderAux rangeOfString:dataElements[0]].location == NSNotFound) {
                es = false;
            }
        }
        if (es)
        {
             [self.providers addObject:prov];
        }
        
    }
   
    
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableProviders reloadData];
        
    });
}


#pragma mark Tabla Proveedores
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_providers count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
            static NSString *simpleTableIdentifier = @"ProviderTableViewCell";
            
            ProviderTableViewCell *cell = (ProviderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            //    if (cell == nil)
            //    {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProviderTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            //    }
            
            DataProvider* auxPlate = [_providers objectAtIndex:indexPath.row];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIImage *image = [UIImage imageWithData:auxPlate.image];
                //Data Fill
                UIImageView *imageView = [[UIImageView alloc]initWithFrame:cell.imgageProvider.frame];
                imageView.backgroundColor = [UIColor clearColor];
                [imageView.layer setMasksToBounds:YES];
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                //        [imageView setContentMode:UIViewContentModeScaleAspectFill];
                //        [imageView setContentMode:UIViewContentModeScaleToFill];
                [imageView setImage:image];
                [cell.contentView addSubview:imageView];
                
            });
            
            cell.descriptionProvider.text = auxPlate.descriptionProvider;
            cell.descriptionProvider.numberOfLines = 2;
            cell.labCountry.text = auxPlate.countryProvider;
            
            
            cell.btnSelectPlan.tag = indexPath.row;
            [cell.btnSelectPlan addTarget:self action:@selector(selectPlanClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        
    
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
            NSInteger index = indexPath.row;
            
            [tableView deselectRowAtIndexPath:indexPath animated:false];
            
            _provSel = [self.providers objectAtIndex:index];
}


-(void)selectPlanClicked:(UIButton*)sender
{
    
    int tagBtnIndex = sender.tag;
    
    _provSel = [self.providers objectAtIndex:tagBtnIndex];
    
    //LLenando datos de categoria de proveedores
    //INICIALIZANDO SERVICIO DE PROVEDORES
    NSString *urlProviderCat = GET_CATEGORIES;
    NSURL *urlCat = [NSURL URLWithString:urlProviderCat];
    self.categories = [[NSMutableArray alloc]init];
    NSURLRequest *req = [NSURLRequest requestWithURL:urlCat
                                         cachePolicy:NSURLRequestUseProtocolCachePolicy
                                     timeoutInterval:300.0];
    
    dispatch_queue_t myQueue1 = dispatch_queue_create("my queue", NULL);
    dispatch_async(myQueue1, ^{
        
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
        [NSURLConnection sendAsynchronousRequest:req
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             
             
             [self.overlayView removeFromSuperview];
             self.view.userInteractionEnabled = YES;
             //  self.viewController.view.userInteractionEnabled = YES;
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:0
                                                                            error:NULL];
                 
                 //LLENANDO LISTA DE CATEGORIAS
                 _provSel.Plans = [[NSMutableArray alloc] init];
                 for (id key in greeting) {
                     NSString* providerId = [key objectForKey:@"providerid"];
                     if ([providerId compare: _provSel.idProvider]==NSOrderedSame)
                     {
                         NSString* desc =[key objectForKey:@"description"] ;
                         DataPlan* pl = [[DataPlan alloc] initWithData:desc descriptionp:desc pricep:@"-1" labplanp:@"" isSelPlan:NO typep:0 idcp:[key objectForKey:@"id"]];
                         [self.categories addObject:pl];
                         for (id plan in [key objectForKey:@"plans"] ) {
                             NSString* name =[plan objectForKey:@"name"] ;
                             NSString* desc =[plan objectForKey:@"description"] ;
                             NSString* price =[plan objectForKey:@"price"] ;
                             NSNumber* serviceType =[[NSNumber alloc] initWithInt:[[plan objectForKey:@"serviceTypeId"]integerValue ]];
                             DataPlan* pl = [[DataPlan alloc] initWithData:name descriptionp:desc pricep:price labplanp:@"" isSelPlan:NO typep:serviceType idcp:[plan objectForKey:@"providerServicesId"]];
                             [_provSel.Plans addObject:pl];
                         }
                         if (self.categories.count > 0)
                             [[self.categories objectAtIndex:0] setIsSelectedPlan:YES];
                     }
                     
                     
                     
                 }
                 
                 //Refreshing table
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.picker reloadAllComponents];
                 });
                 if ([_categories count]== 1)
                 {
                          PlansViewController *servicesDetails = [self loadController:[PlansViewController class]];
                     servicesDetails.viewController = self.viewController;
                          servicesDetails.dataProvider =_provSel;
                          servicesDetails.category = [[NSNumber alloc] initWithInt:self.GetSelectedCategory.intValue];
                          [self presentViewController:servicesDetails animated:YES completion:Nil];
                     
                 }
                 
             }else if (connectionError != nil){
                 //Poner PIN INCORRECTO con cartel Error de conexion
                 //                 _labError.text = @"Error de conexión";
                 //                 [_labError setHidden:NO];
             }
         }];
        
    });
   
   
    [self hideShowPicker];
//    ServicesProviderViewController *servicesDetails = [self loadController:[ServicesProvide rViewController class]];
//    servicesDetails.dataProvider = providerX;
//    
//    [self presentViewController:servicesDetails animated:YES completion:Nil];
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actCancelar:(id)sender {
  [self hideInmediately];

}

- (IBAction)actDone:(id)sender {

[self hideInmediately];
        
    
        PlansViewController *servicesDetails = [self loadController:[PlansViewController class]];
        servicesDetails.dataProvider =_provSel;
        servicesDetails.viewController = self.viewController;

        servicesDetails.category = [[NSNumber alloc] initWithInt:self.GetSelectedCategory.intValue];
        [self presentViewController:servicesDetails animated:YES completion:Nil];
   
//
//    _txtCountry.text = _countrySel.descriptionCountry;
//    [self filterProviders:_txtCountry.text];
}
-(NSNumber*) GetSelectedCategory{
    if (_catSel == nil)
    {
        _catSel = [_categories objectAtIndex:0];
    }
    return _catSel.idc;

}
- (id)loadController:(Class)classType {
    NSString *className = NSStringFromClass(classType);

    UIViewController *controller = [[classType alloc] initWithNibName:className bundle:nil];
    
    return controller;
}

- (IBAction)actOpenMenu:(id)sender {
    [self.viewController openLeftMenu];
}

- (IBAction)actLogin:(id)sender {
    [self.viewController loadLogin];
}


#pragma mark Metodos del Scroll y los TextEdit

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //NSLog(@"value: %@", value);
    
    if(value != nil && [value compare:@""]!=NSOrderedSame )
       [self filterProviders:[[NSString alloc] initWithFormat:@"%@_%@",_txtCountry.text,value]];
    else
        [self filterProviders:[[NSString alloc] initWithFormat:@"%@",_txtCountry.text]];

    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)updateTableRepairDismiss{
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableProviders reloadData];
    });
}

- (IBAction)actCountry:(id)sender {
    [self ShowCountryView];
  
}
-(void)ShowCountryView{
    [self.viewController loadCountry:_countries arrayc:_allcountries];
}
-(void) hideShowPicker{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if(!_isComboShow){
            
            int y = DEVICE_HEIGTH - _pickerView.frame.size.height;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
            
        }else{
            
            int y = DEVICE_HEIGTH + 10;
            [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
            
        }
        
        _isComboShow = !_isComboShow;
        
    }];
}

- (NSInteger)numberOfComponentsInPickerView: (UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_categories count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50.0f;
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
//
//    Plate *platAux = [_matriculas objectAtIndex:row];
//
//    NSString *formatMatric = [[NSString alloc]initWithFormat:@"%@ - %@",platAux.descriptionCar, platAux.number];
//    return formatMatric;
//
//}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont fontWithName:@"Roboto-Regular" size:25]];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
    }
    
    // Fill the label text here
    DataPlan *platAux = [_categories objectAtIndex:row];
    NSString *formatMatric = [[NSString alloc]initWithFormat:@"%@",platAux.descriptionPlan];
    
    
    tView.text=formatMatric;
    return tView;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    _catSel = [_categories objectAtIndex:row];
    
}

- (void) hideInmediately{
    
    int y = DEVICE_HEIGTH + 10;
    [_pickerView setFrame:CGRectMake(0,y,DEVICE_WIDTH,_pickerView.frame.size.height)];
    
    _isComboShow = NO;
}
@end
