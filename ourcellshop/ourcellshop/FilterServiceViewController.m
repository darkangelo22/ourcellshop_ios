//
//  FilterServiceViewController.m
//
//
//  Created by René Abilio on 11/2/16.
//
//

#import "FilterServiceViewController.h"
#import "ViewController.h"

@interface FilterServiceViewController ()

@end

@implementation FilterServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Header
    [self.titleFilters setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.labCancel setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labResetAll setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //MiniHeader
    [self.labDistance setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labPrice setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labCategories setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    [self.labPublished setFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0]];
    
    //Categories
    [self.labActivation setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labUnlock setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labRepair setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //Price
    [self.labMin setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labMax setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    //Published
    [self.lab24hours setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labWeek setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.labAllProducts setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    self.labBtnSave.layer.cornerRadius = 5;
    
    //Location
    [self.labZipCode setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    
    
    self.isActivation = NO;
    self.isRepair = NO;
    self.isUnlock = YES;
    self.timeFromSearch = 1;
    
    [self refreshCategories];
    [self refreshTimePublished];
    
    
    //Scroll
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
        //IS iPhone 4 Heigth Scroll reduce
        self.conHeigthScroll.constant = 400;
    }
    
    self.scrollView.scrollEnabled=YES;
    //CGFloat height = DEVICE_HEIGTH;
    [self.scrollView setContentSize:CGSizeMake(DEVICE_WIDTH, 100)];
    
    self.maxPrice.delegate = self;
    self.minPrice.delegate = self;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)actCancel:(id)sender {
    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:^{}];
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)actResetAll:(id)sender {
    //Clean All Filters
    self.timeFromSearch = 1;
    self.isActivation = NO;
    self.isUnlock = YES;
    self.isRepair = NO;
    self.minPrice.text = @"";
    self.maxPrice.text = @"";
    self.txtZipCode.text = @"";
    [self refreshCategories];
    [self refreshTimePublished];
    
}
- (IBAction)actUnlock:(id)sender {
    self.isUnlock = YES;
    self.isRepair = NO;
    self.isActivation = NO;
    
    [self refreshCategories];
    
}

- (IBAction)actRepair:(id)sender {
    self.isUnlock = NO;
    self.isRepair = YES;
    self.isActivation = NO;
    [self refreshCategories];
}

- (IBAction)actActivation:(id)sender {
    self.isUnlock = NO;
    self.isRepair = NO;
    self.isActivation = YES;
    [self refreshCategories];
    
}

- (IBAction)act24Horas:(id)sender {
    self.timeFromSearch = 1;
    [self refreshTimePublished];
}
- (IBAction)actThisWeek:(id)sender {
    self.timeFromSearch = 2;
    [self refreshTimePublished];
}
- (IBAction)actAllProducts:(id)sender {
    self.timeFromSearch = 3;
    [self refreshTimePublished];
}

- (IBAction)actSaveFilter:(id)sender {
    int selectedCategory = 1;
    double min = [_minPrice.text doubleValue];
    double max = [_maxPrice.text doubleValue];
    int zipcode = -1;
    if (![_txtZipCode.text compare:@""]==NSOrderedSame)
    {
        zipcode = [_txtZipCode.text integerValue];
    }
    if (_isUnlock) selectedCategory=1;
    if (_isRepair) selectedCategory=2;
    if (_isActivation) selectedCategory=3;
    
    FilterServices* filterdata = [[FilterServices alloc] initWithZipCode:zipcode Distance:_HorizontalSlider.value Category:selectedCategory MinPrice:min MaxPrice:max Published:self.timeFromSearch];
    self.viewController.FilterServiceData = filterdata;
    [self.viewController goScreenFilterFrom];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)cleanCategories{
    UIColor *darkColor = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0];
    
    self.imgActivation.image = [UIImage imageNamed:@"software.png"];
    [self.labActivation setTextColor:darkColor];
    self.imgUnlock.image = [UIImage imageNamed:@"unlock.png"];
    [self.labUnlock setTextColor:darkColor];
    self.imgRepair.image = [UIImage imageNamed:@"repair.png"];
    [self.labRepair setTextColor:darkColor];
}
-(void)refreshCategories{
    [self cleanCategories];
    UIColor *blueColor = [UIColor colorWithRed:44.0/255.0 green:79.0/255.0 blue:149.0/255.0 alpha:1.0];
    
    UIColor *darkColor = [UIColor colorWithRed:55.0/255.0 green:55.0/255.0 blue:55.0/255.0 alpha:1.0];
    
    
    //Software
    if (self.isActivation) {
        self.imgActivation.image = [UIImage imageNamed:@"software-blue.png"];
        [self.labActivation setTextColor:blueColor];
        
    }else{
        self.imgActivation.image = [UIImage imageNamed:@"software.png"];
        [self.labActivation setTextColor:darkColor];
    }
    
    //Unlock
    if (self.isUnlock) {
        self.imgUnlock.image = [UIImage imageNamed:@"unlock-blue.png"];
        [self.labUnlock setTextColor:blueColor];
        
    }else{
        self.imgUnlock.image = [UIImage imageNamed:@"unlock.png"];
        [self.labUnlock setTextColor:darkColor];
    }
    
    //Repair
    if (self.isRepair) {
        self.imgRepair.image = [UIImage imageNamed:@"repair-blue.png"];
        [self.labRepair setTextColor:blueColor];
        
    }else{
        self.imgRepair.image = [UIImage imageNamed:@"repair.png"];
        [self.labRepair setTextColor:darkColor];
    }
}

-(void)refreshTimePublished{
    
    //Clean all Img with Check White
    self.img24Hours.image = [UIImage imageNamed:@"check-white.png"];
    self.imgThisWeek.image = [UIImage imageNamed:@"check-white.png"];
    self.imgAllProducts.image = [UIImage imageNamed:@"check-white.png"];
    
    //Paint Blue Check
    switch (self.timeFromSearch) {
        case 1:
            self.img24Hours.image = [UIImage imageNamed:@"check.png"];
            break;
        case 2:
            self.imgThisWeek.image = [UIImage imageNamed:@"check.png"];
            break;
        case 3:
            self.imgAllProducts.image = [UIImage imageNamed:@"check.png"];
            break;
    }
    
}

@end
