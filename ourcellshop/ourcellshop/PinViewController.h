//
//  PinViewController.h
//  t2parking
//
//  Created by René Abilio on 9/4/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "Util.m"
#import "RouteService.h"
#import "LoginViewController.h"
#import "LoginUpViewController.h"

@class ViewController;

@class TourViewController;

@interface PinViewController : UIViewController



@property(assign) ViewController* viewController;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conLogoTop;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak, nonatomic) IBOutlet UILabel *labDescription;
@property (weak, nonatomic) IBOutlet UILabel *labNear;
@property (weak, nonatomic) IBOutlet UILabel *lab2Me;
@property (weak, nonatomic) IBOutlet UIButton *butLogin;
@property (weak, nonatomic) IBOutlet UIButton *butSign;
@property (weak, nonatomic) IBOutlet UIButton *butSearchAround;
@property (weak, nonatomic) IBOutlet UILabel *labDonot;
@property (weak, nonatomic) IBOutlet UILabel *labOrJust;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UILabel *labAlready;
- (IBAction)actLook:(id)sender;
- (IBAction)actSign:(id)sender;
- (IBAction)actButHide:(id)sender;
-(void)moveToMain;
- (IBAction)actLogIn:(id)sender;
- (id)loadController:(Class)classType;
- (IBAction)actLoginUp:(id)sender;
- (IBAction)actCancel:(id)sender;




//Old Code
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnValidar;
@property (weak, nonatomic) IBOutlet UIButton *btnResend;
@property TourViewController* tourVC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthTexto;
@property (weak, nonatomic) IBOutlet UIView *linePin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterIngresaCod;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterCod;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterLineCod;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterValidar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterReenviar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterResend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterError;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conCenterIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labError;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labTitlePin;
@property (weak, nonatomic) IBOutlet UILabel *labDescriptionPin;
@property (weak, nonatomic) IBOutlet UILabel *labTiempoValidez;
@property (weak, nonatomic) IBOutlet UILabel *labIngresaPin;
@property (weak, nonatomic) IBOutlet UILabel *labTitleResend;
- (IBAction)actResendValidateNumber:(id)sender;
- (IBAction)actValidar:(id)sender;
- (IBAction)hideKeyScrollInside:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
- (int)getScrollHeigth;
@property int gate;

@end
