//
//  ViewController.h
//  t2parking
//
//  Created by René Abilio on 8/25/15.
//  Copyright (c) 2015 René Abilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "MapViewController.h"
#import "MenuViewController.h"
#import "Util.m"
#import "RouteService.h"
#import "ServiceViewController.h"
#import "BuyViewController.h"
#import "FilterViewController.h"
#import "FilterServiceViewController.h"
#import "NewAdViewController.h"
#import "SellViewController.h"
#import "MessageSendViewController.h"
#import "MessageViewController.h"
#import "NewAddViewController.h"
#import "NewAdViewController.h"
#import "LoginViewController.h"
#import "PinViewController.h"
#import "CountryViewController.h"
#import "FilterServices.h"
#import "ReadMsgViewController.h"
#import "NewMsgViewController.h"
#import "MessageData.h"


@interface ViewController : UIViewController <UIScrollViewDelegate, UITextFieldDelegate>


//Tabs
@property (weak, nonatomic) IBOutlet UIImageView *imgHome;
@property (weak, nonatomic) IBOutlet UIImageView *imgService;
@property (weak, nonatomic) IBOutlet UIImageView *imgSell;
@property (weak, nonatomic) IBOutlet UIImageView *imgBuy;
@property (weak, nonatomic) IBOutlet UIImageView *imgPay;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnService;
@property (weak, nonatomic) IBOutlet UIButton *btnSell;
@property (weak, nonatomic) IBOutlet UIButton *btnBuy;
@property (weak, nonatomic) IBOutlet UIButton *btnPzy;
@property (weak, nonatomic) IBOutlet UILabel *labHome;
@property (weak, nonatomic) IBOutlet UILabel *labServices;
@property (weak, nonatomic) IBOutlet UILabel *labSell;
@property (weak, nonatomic) IBOutlet UILabel *labBuy;
@property (weak, nonatomic) IBOutlet UILabel *labPay;
-(void)CenterWithLat:(double)lat lon:(double)lon title:(NSString*)title;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heigthScroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conAlignTickets;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conAlignInicio;

@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnManual;
@property (weak, nonatomic) IBOutlet UIButton *btnTickets;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UIView *mark;
@property (weak, nonatomic) IBOutlet UILabel *titleApp;
@property (weak, nonatomic) IBOutlet UILabel *titleApp2;
@property NSNumber *priceOf30;
@property int posMark;
@property BOOL isShowMap;
@property BOOL isShowMsgRead;
@property BOOL isShowMsgSend;
@property BOOL isShowMsg;
@property NSNumber* ZipCode;

@property FilterServices* FilterProductData;
@property FilterServices* FilterServiceData;
@property (weak, nonatomic) IBOutlet UIView *viewSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consWith;
@property (weak, nonatomic) IBOutlet UITextField *txtBuscar;
@property (weak, nonatomic) IBOutlet UIButton *butDelete;
@property int screenToFilter;//1 From Service, 2 From Sell, 3 From Buy Gallery, 4 From Map
@property int screenToCancelLogin;//1 Home, 2 Service, 3 Buy Gallery, 4 Buy Map (Sell y Messages no pk esos DEBES estar logueado)
- (IBAction)actDelete:(id)sender;
- (IBAction)actHome:(id)sender;
- (IBAction)actManual:(id)sender;
- (IBAction)actService:(id)sender;
- (IBAction)actMenu:(id)sender;
- (IBAction)actConfig:(id)sender;
- (IBAction)actSell:(id)sender;
- (IBAction)actBuy:(id)sender;
- (IBAction)actPzy:(id)sender;
-(void)openLeftMenu;
-(void)goHome;
-(void)goService;
-(void)goBuy;
-(void)goMap;
-(void)goMapWithData:(NSMutableArray*)data;
-(void)goGallery;
-(void)goCancelLogin;
-(void)goNewAd;
-(void)goNewAdd;
-(void)goScreenFilterFrom;
-(void)goMsg;
-(void)goMsgRead:(MessageData*)msg;
-(void)goMsgSend;
-(void)goMsgNew:(NSString*)owner;
-(void)loadLogin;
-(void)loadLogin:(int)value;
-(void)loadCountry:(NSMutableDictionary*)countries arrayc:(NSMutableArray*)arrayc;

//Color Tabs
-(void)cleanColors;
-(void)colorToHome;
-(void)colorToService;
-(void)colorToSell;
-(void)colorToBuy;
-(void)colorToPzy;


#pragma mark - Metodos de navegacion menu
-(void)showTerminoCondiciones;
-(void)showConfiguration;
-(void)showHelp;
- (void)showPagoServicio;
-(void)backHome;
-(void)showFilters;
-(void)showServiceFilters;


#pragma mark - Metodos Auxiliares
-(void) fillScroll;
-(id)loadController:(Class)classType;
-(void)showMenu;
-(void)closeMenu;

-(void)btnHide;
- (IBAction)hideMenuHeader:(id)sender;
- (void)actSucive;
- (IBAction)actCleanTxtSearch:(id)sender;
-(void)refreshTableHomeProviders;



@end

