//
//  DataBuy.h
//  
//
//  Created by René Abilio on 6/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataBuy : NSObject

@property NSString* idBuy;
@property NSString* titleBuy;
@property NSString* descriptionBuy;
@property NSMutableArray* arrayImages;
@property NSString* coordinates;
@property NSString* owner;
-(id)initWithData:(NSString*)idB titleBuyP:(NSString*) titl descriptionBuyP:(NSString*) desc imagesP:(NSMutableArray*) arrayImag coordinatesp:(NSString*) coordinates ownerp:(NSString*) owner ;

-(id)initWithBuy:(DataBuy*)dataBuy;


@end
