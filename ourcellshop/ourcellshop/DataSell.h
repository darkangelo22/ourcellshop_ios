//
//  DataSell.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataSell : NSObject

@property NSString* idSell;
@property NSData* image;
@property NSString* titleSell;
@property NSString* descriptionSell;
@property NSNumber* price;
@property NSDate* datePublish;

-(id)initWithData:(NSString*)idS imageP: (NSData*)img titleSellP:(NSString*) titl descriptionSellP:(NSString*) desc priceP:(NSNumber*) pric dateP:(NSDate*) datePubli;

-(id)initWithSell:(DataSell*)sell;

@end
