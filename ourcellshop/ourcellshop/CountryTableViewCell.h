//
//  CountryTableViewCell.h
//  
//
//  Created by René Abilio on 4/2/16.
//
//

#import <UIKit/UIKit.h>

@interface CountryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labCountry;
@property (weak, nonatomic) IBOutlet UIImageView *imgCountry;
@property (weak, nonatomic) IBOutlet UIImageView *imgCountryFlag;

@end
