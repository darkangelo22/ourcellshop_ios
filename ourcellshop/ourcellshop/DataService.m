//
//  DataService.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "DataService.h"

@implementation DataService

-(id)initWithData:(NSString*)idS titleServiceP:(NSString*) titl descriptionServiceP:(NSString*) desc dateP:(NSDate*) datePubli  coordinates:(NSString*)coordinatesp  ownerp:(NSString*)owner{
    
    if (!(self = [super init]))
        return nil;
    
    self.idService = idS;
    self.titleService = titl;
    self.descriptionService = desc;
    self.datePublish = datePubli;
    self.coordinates = coordinatesp;
     self.owner = owner;
    return self;
    
}

-(id)initWithService:(DataService*)dataService{
    
    if (!(self = [super init]))
        return nil;
    
    self.idService = dataService.idService;
    self.titleService = dataService.titleService;
    self.descriptionService = dataService.descriptionService;
    self.datePublish = dataService.datePublish;
    self.coordinates = dataService.coordinates;
    self.owner = dataService.owner;
    return self;
    
}

@end
