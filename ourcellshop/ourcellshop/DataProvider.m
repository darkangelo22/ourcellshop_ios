//
//  DataProvider.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "DataProvider.h"

@implementation DataProvider

-(id)initWithData:(NSString*)idP provName:(NSString*)providerNameP imageDataP: (NSData*)imgdata imageNameP: (NSString*)img descriptionProviderP:(NSString*) desc countryProvider:(NSString*) countr  plansp:(NSMutableArray*)plans{
    
    if (!(self = [super init]))
        return nil;
    
    self.idProvider = idP;
    self.providerName = providerNameP;
    self.imageName = img;
    self.image = imgdata;
    self.descriptionProvider = desc;
    self.countryProvider = countr;
    self.Plans = plans;
 
    return self;
}

-(id)initWithProvider:(DataProvider*)prov{
    
    if (!(self = [super init]))
        return nil;
    
    self.idProvider = prov.idProvider;
    self.imageName = prov.imageName;
    self.image = prov.image;
    self.providerName = prov.providerName;
    self.descriptionProvider = prov.descriptionProvider;
    self.countryProvider = prov.countryProvider;
    self.Plans = prov.Plans;
       return self;
    
}

//To save In NSUserDefault
- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.idProvider forKey:@"idProvider"];
    [encoder encodeObject:self.imageName forKey:@"imageName"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.descriptionProvider forKey:@"descriptionProvider"];
    [encoder encodeObject:self.descriptionProvider forKey:@"countryProvider"];
    [encoder encodeObject:self.descriptionProvider forKey:@"Plans"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.idProvider = [decoder decodeObjectForKey:@"idProvider"];
        self.imageName = [decoder decodeObjectForKey:@"imageName"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.descriptionProvider = [decoder decodeObjectForKey:@"descriptionProvider"];
        self.countryProvider = [decoder decodeObjectForKey:@"countryProvider"];
        self.countryProvider = [decoder decodeObjectForKey:@"Plans"];
    }
    return self;
}


@end
