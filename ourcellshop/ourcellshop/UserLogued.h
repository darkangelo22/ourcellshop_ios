//
//  UserLogued.h
//  
//
//  Created by René Abilio on 7/1/16.
//
//

#import <Foundation/Foundation.h>

@interface UserLogued : NSObject

@property NSString* username;
@property NSString* typeauthentication;//"1": Service, "2": Twitter, "3": Facebook
@property NSString* token;
@property NSString* userid;

-(id)initWithData:(NSString*)user typeauthentication: (NSString*)auth  token: (NSString*)token  userid: (NSString*)userid;

-(id)initEmptyUser;

@end
