//
//  SellViewController.m
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import "SellViewController.h"
#import "ViewController.h"
#import "AdminNSUserDefaults.h"
@interface SellViewController ()

@end

@implementation SellViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.txtFilterSearch setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    self.txtFilterSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    
    [self.tableSell setDataSource:self];
    [self.tableSell setDelegate:self];
    self.txtFilterSearch.delegate = self;
}
-(void)onLoad{
    //INHABILITANDO CLICK EN PANTALLA CON BOTON GIGANTE
    self.view.userInteractionEnabled = NO;
    //  self.viewController.view.userInteractionEnabled = NO;
    [self.overlayView removeFromSuperview];
    self.view.userInteractionEnabled = YES;

    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    NSDate *dateG = [[NSDate alloc]init];
 
    //INICIALIZANDO SERVICIO DE PAISES
    NSString *urlBuyService = GET_PRODUCTS;
    NSURL *urlBuy= [NSURL URLWithString:urlBuyService];
    self.sells = [[NSMutableArray alloc]init];
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@Product?fields=productimages,size,itemConditionId,makeId,categoryid,coordinates,id,title,description,contactByPhone,contactByText,price,emaildisplayid,username",urlBase];
    
       NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //[request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;

    dispatch_queue_t myQueue = dispatch_queue_create("my queue5", NULL);
    dispatch_async(myQueue, ^{
        
        
        //OBTENIENDO RESPUESTA DEL SERVICIO DE PAISES
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             [self.overlayView removeFromSuperview];
             self.view.userInteractionEnabled = YES;
             
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                       options:0
                                                                         error:NULL];
                 NSString *urlproviderphoto = GET_PROVIDERS_PHOTO;
                 //LLENANDO LISTA DE PAISES
                 for (id key in greet) {
                     //                 [self.countries setObject: [key objectForKey:@"description"] forKey:[key objectForKey:@"id"]];
                      NSString* user = [key objectForKey:@"userName"];
                     UserLogued* userLogged = [AdminNSUserDefaults getUserLogued];
                     if ([user compare:userLogged.username]==NSOrderedSame)
                     {
                         NSData *dataimg=[[NSData alloc] init];
                           for (id img in [key objectForKey:@"productimages"]) {
                               NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat: @"%@%@",urlproviderphoto,[img objectForKey:@"imageurl"]]];
                               NSData *dataimg = [NSData dataWithContentsOfURL:url];
                           }
                         
                         
                         
                         
                         NSString *ide =[key objectForKey:@"id"];
                         NSString *title = [key objectForKey:@"title"];
                         NSString *desc =[key objectForKey:@"description"];
                         NSNumber *price=[[NSNumber alloc] initWithDouble:[[key objectForKey:@"price"] doubleValue]];
                         
                         DataSell *dataProv1 = [[DataSell alloc] initWithData:ide imageP:dataimg titleSellP:title descriptionSellP:desc priceP:price dateP:dateG];
                         
                         [self.sells addObject:dataProv1];
                     }
                     
                     
                     
                 }
                 if ([self.sells count] > 0)
                 {
                     [self.viewNoObj setHidden:YES];
                 }
                 else
                 {
                      [self.viewNoObj setHidden:NO];
                 }
                 //Refreshing table
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.tableSell reloadData];
                 });
             }
         }];
    });
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actNewAd:(id)sender {
    [self.viewController goNewAd];
}

- (IBAction)actShowFilter:(id)sender {
    [self.viewController showFilters];
}


#pragma mark Tabla Proveedores
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [_sells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SellTableViewCell";
    
    SellTableViewCell *cell = (SellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SellTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DataSell* auxPlate = [_sells objectAtIndex:indexPath.row];
    
    //Data Fill
    UIImage *image = [UIImage imageWithData:auxPlate.image];
    cell.imageSell.image = image;
    
    
    cell.titleSell.text = auxPlate.titleSell;
    cell.descriptionSell.text = auxPlate.descriptionSell;
    cell.priceSell.text = [[NSString alloc]initWithFormat:@"%@", auxPlate.price];
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    DataSell *plateEditAux = [self.sells objectAtIndex:index];
    
}



#pragma mark Metodos del Scroll y los TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
