//
//  DataProvider.h
//  
//
//  Created by René Abilio on 5/1/16.
//
//

#import <Foundation/Foundation.h>

@interface DataProvider : NSObject

@property NSString* idProvider;
@property NSString* imageName;
@property NSData* image;
@property NSString* providerName;
@property NSString* descriptionProvider;
@property NSString* countryProvider;
@property NSMutableArray* Plans;


-(id)initWithData:(NSString*)idP provName:(NSString*)providerNameP imageDataP: (NSData*)imgdata imageNameP: (NSString*)img descriptionProviderP:(NSString*) desc countryProvider:(NSString*) countr plansp:(NSMutableArray*)plans;

-(id)initWithProvider:(DataProvider*)prov;

@end
