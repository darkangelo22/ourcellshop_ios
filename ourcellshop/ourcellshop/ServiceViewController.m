//
//  ServiceViewController.m
//
//
//  Created by René Abilio on 1/1/16.
//
//

#import "ServiceViewController.h"
#import "ViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface ServiceViewController ()

@end

@implementation ServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.numberAlert.layer.cornerRadius = 8;
    [self.titleService setFont:[UIFont fontWithName:@"Roboto-Bold" size:15.0]];
    [self.labBack setFont:[UIFont fontWithName:@"Roboto-Regular" size:13.0]];
    [self.txtFilterSearch setFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0]];
    
    self.numberSectionSearch = 1;
    [self setColorSearchType];
    
    self.txtGeneralSearch.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.txtFilterSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    //self.txtGeneralSearch.layer.borderWidth = 1.0;
    
    self.txtFilterSearch.delegate = self;
    
    
    //iPhone 4
    if (!IS_IPHONE_5 && !IS_IPHONE_6 && !IS_IPHONE_6_PLUS) {
        //        self.conSpaceRigth.constant = 25;
    }
    
    
 
    self.servicesArrayUnlock=[[NSMutableArray alloc] init];
    self.servicesArrayRepair=[[NSMutableArray alloc] init];
    self.servicesArrayActivation=[[NSMutableArray alloc] init];
    [self.tableServices setDataSource:self];
    [self.tableServices setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actBack:(id)sender {
    [self.viewController backHome];
}

-(void)setColorSearchType{
    [self cleanColorsTools];
    
    switch (self.numberSectionSearch) {
        case 1:
        {
            [self colorToUnlock];
        }break;
        case 2:
        {
            [self colorToTool];
        }break;
        case 3:
        {
            [self colorToWiFi];
        }break;
    }
    
}
-(void)onLoad{
    [self.overlayView removeFromSuperview];
    self.view.userInteractionEnabled = YES;

    self.servicesArrayUnlock=[[NSMutableArray alloc] init];
    self.servicesArrayRepair=[[NSMutableArray alloc] init];
    self.servicesArrayActivation=[[NSMutableArray alloc] init];
    //INHABILITANDO CLICK EN PANTALLA CON BOTON GIGANTE
    self.view.userInteractionEnabled = NO;
    //  self.viewController.view.userInteractionEnabled = NO;
    
    //PANTALLA NEGRA DE ESPERA
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    self.overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.overlayView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    
    // INDICADOR DE ESPERA
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0, 0, 5, 5);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [self.overlayView addSubview:self.indicator];
    [self.indicator startAnimating];
    [self.view addSubview:self.overlayView];
    
    
    
    NSString *urlBase = URL_BASE;
    
    NSString *requestUrl = [[NSString alloc]initWithFormat:@"%@Service",urlBase];
    
    int ZipCode = self.viewController.FilterServiceData.ZipCode;
    int Distance=self.viewController.FilterServiceData.Distance;
    int Category=self.viewController.FilterServiceData.Category;
    double MinPrice=self.viewController.FilterServiceData.MinPrice;
    double MaxPrice=self.viewController.FilterServiceData.MaxPrice;
    int Published=self.viewController.FilterServiceData.Published;
    
    NSString *post=@"";
    if (ZipCode != -1)
    {
        post= [NSString stringWithFormat:@"Distance=%d&Category=%d&Published=%d",Distance,Category,Published];
    }
    
     post = ZipCode != -1 ? [NSString stringWithFormat:@"%@%@=%d",post,@"&ZipCode",ZipCode]:post;
     post = MinPrice != -1 ? [NSString stringWithFormat:@"%@%@=%f",post,@"&MinPrice",MinPrice]:post;
     post = MaxPrice != -1 ? [NSString stringWithFormat:@"%@%@=%f",post,@"&MaxPrice",MaxPrice]:post;
    
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:requestUrl]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *res = nil;
    NSError *err = nil;
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     
     {
         [self.overlayView removeFromSuperview];
         self.view.userInteractionEnabled = YES;
         
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *greet = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:NULL];
             //LLENANDO LISTA DE PAISES
             for (id key in greet) {
                 //                 [self.countries setObject: [key objectForKey:@"description"] forKey:[key objectForKey:@"id"]];
                 
                 DataService *dataServ1 = [[DataService alloc]initWithData:[key objectForKey:@"id"] titleServiceP:[key objectForKey:@"title"] descriptionServiceP:[key objectForKey:@"description"] dateP:[[NSDate alloc]init] coordinates:[key objectForKey:@"coordinates"] ownerp:[key objectForKey:@"username"] ];
                 long tipo =[[key objectForKey:@"serviceTypeId"] longValue];
                 if (tipo == 1)
                 {
                     [self.servicesArrayUnlock addObject:dataServ1];
                 }
                 if (tipo == 2)
                 {
                     [self.servicesArrayRepair addObject:dataServ1];
                 }
                 if (tipo == 3)
                 {
                     [self.servicesArrayActivation addObject:dataServ1];
                 }
                 
             }
             //Refreshing table
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self.tableServices reloadData];
             });
         }
         
     }];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
  
    
}
- (IBAction)actNewAd:(id)sender {
    
    BOOL isUserLog = [AdminNSUserDefaults isUserLogued];
    if(isUserLog){
        [self.viewController goNewAdd];
        //    [self.viewController goNewAdd];
    }
    else
    {
        [self.viewController loadLogin];
    }
    
}

-(void)cleanColorsTools{
    
    self.imgUnlock.image = [UIImage imageNamed:@"ios7-unlocked.png"];
    self.imgTool.image = [UIImage imageNamed:@"wrench.png"];
    self.imgWiFi.image = [UIImage imageNamed:@"connection-bars.png"];
    
}
-(void)colorToUnlock{
    self.imgUnlock.image = [UIImage imageNamed:@"ios7-unlocked-white.png"];
}
-(void)colorToTool{
    self.imgTool.image = [UIImage imageNamed:@"wrench-white.png"];
}
-(void)colorToWiFi{
    self.imgWiFi.image = [UIImage imageNamed:@"connection-bars-white.png"];
}

- (IBAction)actShowFilter:(id)sender {
    self.viewController.screenToFilter = 1;
    [self.viewController showServiceFilters];
}

- (IBAction)actTool:(id)sender {
    self.numberSectionSearch = 2;
    [self setColorSearchType];
    
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableServices reloadData];
    });
}

- (IBAction)actWifi:(id)sender {
    self.numberSectionSearch = 3;
    [self setColorSearchType];
    
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableServices reloadData];
    });
}

- (IBAction)actUnlock:(id)sender {
    self.numberSectionSearch = 1;
    [self setColorSearchType];
    
    //Refreshing table
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableServices reloadData];
    });
}

#pragma mark Metodos del Scroll y los TextEdit

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    
    [textField setReturnKeyType:UIReturnKeyDone];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

//-(void)colorToHome{
//    self.imgHome.image = [UIImage imageNamed:@"home-white.png"];
//
//    [self.btnHome setTintColor:[UIColor whiteColor]];
//    self.btnHome.titleLabel.tintColor = [UIColor whiteColor];
//}


#pragma mark Tabla Proveedores
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    switch (self.numberSectionSearch) {
        case 1:
        {
            return [self.servicesArrayUnlock count];
        }break;
        case 2:
        {
            return [self.servicesArrayRepair count];
        }break;
        case 3:
        {
            return [self.servicesArrayActivation count];
        }break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"ServiceTableViewCell";
    
    ServiceTableViewCell *cell = (ServiceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ServiceTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    DataService* auxPlate = nil;
    
    switch (self.numberSectionSearch) {
        case 1:
        {
            auxPlate = [self.servicesArrayUnlock objectAtIndex:indexPath.row];
        }break;
        case 2:
        {
            auxPlate = [self.servicesArrayRepair objectAtIndex:indexPath.row];
        }break;
        case 3:
        {
            auxPlate = [self.servicesArrayActivation objectAtIndex:indexPath.row];
        }break;
    }
    
    //Data Fill
    cell.titleService.text = auxPlate.titleService;
    cell.descriptionService.text = (![auxPlate.descriptionService isEqual: [NSNull null]]) ? auxPlate.descriptionService :@"";
    cell.coordinates = auxPlate.coordinates;
    cell.owner = auxPlate.owner;
    cell.viewController = self.viewController;
    return cell;
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    DataService* auxPlate = nil;
    
    switch (self.numberSectionSearch) {
        case 1:
        {
            auxPlate = [self.servicesArrayUnlock objectAtIndex:index];
        }break;
        case 2:
        {
            auxPlate = [self.servicesArrayRepair objectAtIndex:index];
        }break;
        case 3:
        {
            auxPlate = [self.servicesArrayActivation objectAtIndex:index];
        }break;
    }
    
}

@end
